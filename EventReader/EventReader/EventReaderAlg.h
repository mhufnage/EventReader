#ifndef EVENTREADER_EVENTREADERALG_H
#define EVENTREADER_EVENTREADERALG_H
#include "GaudiKernel/ToolHandle.h"
#include "GaudiKernel/ServiceHandle.h"
#include "GaudiKernel/ITHistSvc.h"
#include "GaudiKernel/ToolHandle.h"
#include "StoreGate/ReadHandleKey.h"
#include "StoreGate/WriteHandleKey.h"
#include "StoreGate/ReadCondHandleKey.h"
#include "AthenaBaseComps/AthAlgorithm.h"

#include "xAODEventInfo/EventInfo.h"
#include "xAODCaloEvent/CaloCluster.h"
#include "xAODCaloEvent/CaloClusterContainer.h"
#include "xAODJet/JetContainer.h"
#include "xAODEgamma/PhotonContainer.h" //xAODEgamma = online and xAODegamma = offline
#include "xAODEgamma/Photon.h"
#include "xAODEgamma/ElectronContainer.h"
#include "xAODEgamma/Electron.h"
#include "xAODTruth/TruthParticle.h"
#include "xAODTruth/TruthEvent.h"
#include "xAODTruth/TruthVertex.h"
#include "xAODTruth/TruthParticleContainer.h"
#include "xAODTruth/TruthEventContainer.h"
#include "xAODTruth/TruthVertexContainer.h"
#include "xAODTruth/xAODTruthHelpers.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "xAODTracking/VertexContainer.h"
#include "xAODTracking/Vertex.h"


#include "LArRawEvent/LArDigitContainer.h"
#include "LArRawEvent/LArDigit.h"
#include "LArRawEvent/LArRawChannelContainer.h"
#include "LArRawEvent/LArRawChannel.h"
#include "TileEvent/TileDigitsContainer.h"
#include "TileEvent/TileDigits.h"
#include "TileEvent/TileDigitsCollection.h"
#include "TileEvent/TileRawChannelContainer.h"
#include "TileEvent/TileRawChannelCollection.h"
#include "TileEvent/TileRawChannel.h"
#include "TileEvent/TileCell.h"

#include "Identifier/Identifier.h"
#include "Identifier/HWIdentifier.h"
#include "LArIdentifier/LArOnlineID.h"
#include "LArCabling/LArOnOffIdMapping.h"
// #include "TileConditions/TileCablingService.h"
#include "TileConditions/TileCablingSvc.h"
#include "CaloIdentifier/CaloCell_ID.h"
// #include "CaloIdentifier/TileHWID.h"
// #include "CaloIdentifier/TileID.h"

#include <TH1.h>
#include <TTree.h>

#include <bitset>

// #include "CaloEvent/CaloCell.h"
// #include "xAODTrigRinger/versions/TrigRingerRings_v2.h"
// #include "xAODTrigRinger/TrigRingerRingsContainer.h"
// #include <xAODCaloRings/versions/RingSet_v1.h>
// #include <xAODCaloRings/RingSetContainer.h>
// #include <xAODEgamma/versions/Electron_v1.h>
// #include <xAODEgamma/ElectronContainer.h>
// #include "xAODTruth/TruthParticle.h"
// #include "xAODTruth/TruthParticleContainer.h"
// #include "xAODTruth/xAODTruthHelpers.h"
// #include <xAODCaloRings/versions/CaloRings_v1.h>
// #include <xAODCaloRings/CaloRingsContainer.h>


// using namespace std;
// class LArDigitContainer;
class LArOnOffIdMapping;
class TileCablingSvc;
// class TileOnOffIdMapping;

class EventReaderAlg: public ::AthAlgorithm 
{ 
// The structure here is built to separate the implementation from de header file. 
// So, the implementation goes in another file, a *.cxx one. 
 public: 
    EventReaderAlg( const std::string& name, ISvcLocator* pSvcLocator );
    virtual ~EventReaderAlg(); 
    virtual StatusCode  initialize() override;     //once, before any input is loaded
    virtual StatusCode  execute() override;        //per event
    virtual StatusCode  finalize() override;       //once, after all events processed
    void                clear();
    void                bookBranches(TTree *tree);
    int                 getCaloRegionIndex(const CaloCell* cell); //customized representative indexes of calo region
    double              fixPhi(double phi);
    double              deltaPhi(double phi1 , double phi2);
    double              deltaR( double eta, double phi);
    virtual StatusCode  dumpEventInfo(const xAOD::EventInfo *ei);
    virtual StatusCode  dumpClusterInfo(const xAOD::CaloClusterContainer *cls, const std::string& clsName, const LArOnOffIdMapping* larCabling);
    virtual StatusCode  dumpJetsInfo(const xAOD::JetContainer *jets, const std::string& jetAlg, const LArOnOffIdMapping* larCabling );
    virtual StatusCode  dumpTruthParticle(const xAOD::TruthParticleContainer* truthParticleCnt);
    virtual StatusCode  dumpTruthEvent(const xAOD::TruthEventContainer* truthEventCnt);
    virtual StatusCode  dumpPhotons(const xAOD::PhotonContainer* photonCnt);
    virtual StatusCode  dumpZeeCut();

    // virtual StatusCode  dumpJetRoiInfo();
    // virtual StatusCode  dumpLArDigits(const LArDigitContainer *digitsContainer, const std::string& digitsContName, std::bitset<200000>& clustBits, int& cellIndex, int& readOutIndex, int& roiIndex);
    // virtual StatusCode  dumpTileDigits(const TileDigitsContainer *digitsContainer, const std::string& digitsContName, std::bitset<65536>& clustBits, std::vector<size_t>& tileHashMap, int& cellIndex, int& readOutIndex, int& roiIndex);
    // virtual StatusCode  dumpTileRawCh(const TileRawChannelContainer *rawChannelContainer, const std::string& rawChannelName, std::bitset<65536>& clustBits); 
    // virtual StatusCode  getCaloCellInfo(const CaloCell* cell, std::bitset<65536>& clustBitsTile, std::vector<size_t>& tileHashMap, std::bitset<200000>& clustBitsLAr, int cellNum, const LArOnOffIdMapping* larCabling);
    virtual StatusCode  testCode();

  private: 
    Gaudi::Property<std::string> pr_clusterName {this, "clusterName" , "CaloCalTopoClusters" ,  "Name of the cluster container that will have its cells dumped."};
    Gaudi::Property<std::string> pr_jetName {this, "jetName" , "AntiKt4EMPFlowJets" ,  "Name of the jet container that will have its cells dumped."};
    Gaudi::Property<std::string> pr_tileDigName {this, "tileDigName" , "TileDigitsCnt" ,  "Name of the Tile digits container that will have its values dumped."};
    Gaudi::Property<std::string> pr_larDigName {this, "larDigName" , "LArDigitContainer_MC" ,  "Name of the LAr digits container that will have its values dumped."};    
    Gaudi::Property<std::string> pr_tileRawName {this, "tileRawName" , "TileRawChannelCnt" ,  "Name of the Tile raw channel container that will have its values dumped."};
    Gaudi::Property<std::string> pr_larRawName {this, "larRawName" , "LArRawChannels" ,  "Name of the LAr raw channel container that will have its values dumped."};

    Gaudi::Property<bool> pr_doTile {this, "doTile", true, "Dump the tile cells inside any ROI."}; //pending
    Gaudi::Property<bool> pr_noBadCells {this, "noBadCells", true, "If True, skip the cells tagged as badCells/channels."}; //pending
    Gaudi::Property<bool> pr_doLAr {this, "doLAr", true, "Dump the LAr cells inside any ROI."}; //pending
    Gaudi::Property<bool> pr_printCellsClus {this, "printCellsClus", true, "Print out the cluster cells basic info during the dump."}; //pending
    Gaudi::Property<bool> pr_printCellsJet {this, "printCellsJet", false, "Print out the jet ROI cells basic info during the dump of Jet ROI."}; //pending
    Gaudi::Property<bool> pr_testCode {this, "testCode", false, "Execute testing code function."};
    Gaudi::Property<float> pr_roiRadius {this, "roiRadius" , 0.5 ,  "Radius of the jets region of interest."};
    Gaudi::Property<bool> pr_isMC {this, "isMC", false, "Switch the dumper to MC sample mode."};

    ServiceHandle<ITHistSvc> m_ntsvc;
    ServiceHandle<TileCablingSvc> m_tileCablingSvc { this, "TileCablingSvc", "TileCablingSvc", "Tile cabling service"}; //Name of Tile cabling service
    SG::ReadCondHandleKey<LArOnOffIdMapping> m_larCablingKey;
    // SG::ReadCondHandleKey<LArOnOffIdMapping> m_cablingKey{this,"CablingKey","LArOnOffIdMap","SG Key of LArOnOffIdMapping object"};

    // const LArOnOffIdMapping* larCabling=*larCablingHdl;
    const LArOnlineID* m_onlineLArID; //from detector store LAr
    const TileHWID* m_tileHWID;//Atlas detector Identifier for Tile Calorimeter online (hardware) identifiers
    const TileID* m_tileID; //Atlas detector Identifier class for Tile Calorimeter offline identifiers
    const TileCablingService* m_tileCabling;
    const CaloCell_ID* m_calocell_id;

    TTree *m_Tree;

    // ## EventInfo ##
    unsigned int        e_runNumber     = 9999; //< Run number
    unsigned int        e_bcid          = 9999; //< BCID number
    unsigned long long  e_eventNumber   = 9999; //< Event number
    float               e_pileup        = -999;  //< avg_mu
    //##################
    // ## Cluster ##
    std::vector < int > *c_clusterIndex = nullptr; // cluster index, for each cluster in the same event. (they have the same number of inputs)
    std::vector < double > *c_clusterEnergy = nullptr; //energy of the cluster
    std::vector < double > *c_clusterEta = nullptr; // clus. baricenter eta
    std::vector < double > *c_clusterPhi = nullptr; // clus. baricenter phi
    std::vector < double > *c_clusterEta_calc = nullptr; // clus. baricenter eta calculated
    std::vector < double > *c_clusterPhi_calc = nullptr; // clus. baricenter phi calculated
    std::vector < double > *c_clusterPt = nullptr; // clus. baricenter eta
    // Cell
    std::vector < int > *c_clusterIndex_cellLvl = nullptr; // cluster index, for each cell index. (they have the same number of inputs)
    std::vector < int > *c_clusterCellIndex = nullptr; // cell index inside a cluster
    std::vector < int > *c_cellGain = nullptr; // gain of cell signal, from 'CaloGain' standard.
    std::vector < int > *c_cellLayer = nullptr; // layer index of cell signal, from caloDDE.
    std::vector < int > *c_cellRegion = nullptr; // region of calorimeter system (custom index from 'getCaloRegionIndex')
    std::vector < double > *c_cellEnergy = nullptr; // cell inside cluster energy
    std::vector < double > *c_cellEta = nullptr; // cell inside cluster baricenter eta
    std::vector < double > *c_cellPhi = nullptr; // cell inside cluster baricenter phi
    std::vector < double > *c_cellDEta = nullptr; // cell inside cluster delta_eta (granularity)
    std::vector < double > *c_cellDPhi = nullptr; // cell inside cluster delta_phi (granularity)
    std::vector < double > *c_cellToClusterDPhi = nullptr; // cell inside cluster delta_phi distance to cluster baricenter.
    std::vector < double > *c_cellToClusterDEta = nullptr; // cell inside cluster delta_eta distance to cluster baricenter.
    // Channel
    std::vector < int > *c_clusterIndex_chLvl = nullptr; // cluster index, for each channel index. (they have the same number of inputs)
    std::vector < float > *c_clusterChannelIndex = nullptr; // index for a sequence of channels. It is a float for identify PMTs: +0.0 LAr, +0.1 Tile PMT1, +0.2 Tile PMT2
    std::vector < std::vector < int > > *c_channelChInfo = nullptr; // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
    std::vector < std::vector < float > > *c_channelDigits = nullptr;  // samples from LAr and Tile cells/channels
    std::vector < std::vector < short > > *c_larSamples = nullptr; //xxxxxxxxxxx excluir
    std::vector < std::vector < float > > *c_tileSamples = nullptr; //xxxxxxxxxxx excluir
    std::vector < double > *c_channelEnergy = nullptr; // energy of cell or readout channel inside cluster (MeV)
    std::vector < double > *c_channelTime = nullptr; // time of channel inside cluster (VERIFICAR A UNIDADE !!!!!!!!!!!!)
    std::vector < bool > *c_channelBad = nullptr; // channel linked to a cluster, whitch is tagged as bad. (1 - bad, 0 - not bad)
    std::vector <  unsigned int  > *c_channelHashMap = nullptr; //cell map of ALL cells inside a cluster. id 0x2d214a140000000. is a 64-bit number that represent the cell.
    // Raw channel
    std::vector < std::vector < int > > *c_rawChannelChInfo = nullptr; // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel, provenance) and Tile (ros, drawer, channel, adc, pedestal)
    std::vector < float > *c_rawChannelAmplitude = nullptr; // raw channel energy (adc)
    std::vector < float > *c_rawChannelTime = nullptr; // raw channel time (VERIFICAR A UNIDADE !!!!!!!!!!!!!)
    std::vector < float > *c_rawChannelPedProv = nullptr; // raw channel estimated pedestal (Tile) or LAr provenance (??)
    std::vector < float > *c_rawChannelQuality = nullptr; // raw channel quality
    std::vector < float > *c_clusterRawChannelIndex = nullptr; // raw channel index
    std::vector < int > *c_clusterIndex_rawChLvl = nullptr; // cluster index at raw channel level
    
    // VERIFICAR !!!!
    // std::vector < std::vector < float > > *c_bothCaloSamples = nullptr; //samples of tile and LAr cells inside a cluster
    // std::vector < unsigned long long > *c_cellIdentifier = nullptr; //id 0x2d214a140000000. is a 64-bit number that represent the cell.
    // ################
    // ##  Jets  ##
    std::vector < int >   *j_jetIndex = nullptr; // jet index, for each cluster in the same event.
    std::vector < float > *j_jetPt = nullptr; // transverse momentum of jets 
    std::vector < float > *j_jetEta = nullptr; // vector to store jets eta 
    std::vector < float > *j_jetPhi = nullptr; // vector to store jets phi
    float j_roi_r;
    // Cell
    std::vector < int > *j_jetRoiIndex_cellLvl = nullptr; // jet index, for each cell index. (they have the same number of inputs)
    std::vector < int > *j_jetRoiCellIndex = nullptr; // cell index inside a jet ROI
    std::vector < int > *j_cellGain = nullptr; // gain of cell signal, from 'CaloGain' standard.
    std::vector < int > *j_cellLayer = nullptr; // layer index of cell signal, from caloDDE (sampling).
    std::vector < int > *j_cellRegion = nullptr; // region of calorimeter system (custom index from 'getCaloRegionIndex')
    std::vector < double > *j_cellEnergy = nullptr; // energy of a cell inside jet ROI
    std::vector < double > *j_cellEta = nullptr; // cell inside jet ROI baricenter eta
    std::vector < double > *j_cellPhi = nullptr; // cell inside jet ROI baricenter phi
    std::vector < double > *j_cellDEta = nullptr; // cell inside jet ROI delta_eta (granularity)
    std::vector < double > *j_cellDPhi = nullptr; // cell inside jet ROI delta_phi (granularity)
    std::vector < double > *j_cellToJetDPhi = nullptr; // cell inside jet ROI delta_phi distance to jet eta/phi.
    std::vector < double > *j_cellToJetDEta = nullptr; // cell inside jet ROI delta_eta distance to jet eta/phi.
    // Channel
    std::vector < int > *j_jetRoiIndex_chLvl = nullptr; // jet ROI index, for each channel index. (they have the same number of inputs)
    std::vector < float > *j_jetRoiChannelIndex = nullptr; // index for a sequence of channels. It is a float for identify PMTs: +0.0 LAr, +0.1 Tile PMT1, +0.2 Tile PMT2
    std::vector < std::vector < int > > *j_channelChInfo = nullptr; // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
    std::vector < std::vector < float > > *j_channelDigits = nullptr;  // samples from LAr and Tile cells/channels
    std::vector < std::vector < short > > *j_larSamples = nullptr; //xxxxxxxxxxx excluir
    std::vector < std::vector < float > > *j_tileSamples = nullptr; //xxxxxxxxxxx excluir
    std::vector < double > *j_channelEnergy = nullptr; // energy of cell or readout channel inside jet ROI (MeV)
    std::vector < double > *j_channelTime = nullptr; // time of channel inside cluster (VERIFICAR A UNIDADE !!!!!!!!!!!!)
    std::vector < bool > *j_channelBad = nullptr; // channel linked to a cluster, whitch is tagged as bad. (1 - bad, 0 - not bad)
    std::vector <  unsigned int  > *j_channelHashMap = nullptr; //cell map of ALL cells inside a jet ROI.
    // Raw channel
    std::vector < std::vector < int > > *j_rawChannelChInfo = nullptr; // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel, provenance) and Tile (ros, drawer, channel, adc, pedestal)
    std::vector < float > *j_rawChannelAmplitude = nullptr; // raw channel energy (adc)
    std::vector < float > *j_rawChannelTime = nullptr; // raw channel time (VERIFICAR A UNIDADE !!!!!!!!!!!!!)
    std::vector < float > *j_rawChannelPedProv = nullptr; // raw channel estimated pedestal (Tile) or LAr provenance (??)
    std::vector < float > *j_rawChannelQuality = nullptr; // raw channel quality
    std::vector < float > *j_jetRoiRawChannelIndex = nullptr; // raw channel index
    std::vector < int > *j_jetRoiIndex_rawChLvl = nullptr; // jet index at raw channel level
    
    // ## Particle Truth ##
    std::vector < float > *mc_part_energy   = nullptr;
    std::vector < float > *mc_part_pt       = nullptr;
    std::vector < float > *mc_part_m        = nullptr;
    std::vector < float > *mc_part_eta      = nullptr;
    std::vector < float > *mc_part_phi      = nullptr;    
    std::vector < int >   *mc_part_pdgId    = nullptr;
    std::vector < int >   *mc_part_status   = nullptr;
    std::vector < int >   *mc_part_barcode  = nullptr;

    // ## Vertex Truth ##
    std::vector < float > *mc_vert_x     = nullptr;
    std::vector < float > *mc_vert_y     = nullptr;
    std::vector < float > *mc_vert_z     = nullptr;
    std::vector < float > *mc_vert_time  = nullptr;
    std::vector < float > *mc_vert_perp  = nullptr;
    std::vector < float > *mc_vert_eta   = nullptr;
    std::vector < float > *mc_vert_phi   = nullptr;
    std::vector < int > *mc_vert_barcode = nullptr;
    std::vector < int > *mc_vert_id      = nullptr;
  
     // ## Particle Reco ##
    std::vector < float > *ph_eta     = nullptr;
    std::vector < float > *ph_phi     = nullptr;
    std::vector < float > *ph_pt      = nullptr;
    std::vector < float > *ph_energy  = nullptr;
    std::vector < float > *ph_m       = nullptr;
    // std::vector < int > *ph_origin  = nullptr;
    // std::vector < int > *ph_type    = nullptr;


    //##################
    

}; 


#endif 
