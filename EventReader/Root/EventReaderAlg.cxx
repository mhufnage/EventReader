// PhotonTruthMatchingTool includes
#include "EventReader/EventReaderAlg.h"
#include <xAODEventInfo/EventInfo.h>

// CaloCell
#include "CaloEvent/CaloCell.h"
#include "CaloEvent/CaloCellContainer.h"
#include "CaloEvent/CaloClusterCellLink.h"
#include "CaloEvent/CaloClusterCellLinkContainer.h"
// #include "LArRawChannelContainer/"
#include "CaloEvent/CaloCompactCell.h"
#include "CaloEvent/CaloCompactCellContainer.h"

#include "CLHEP/Units/SystemOfUnits.h"
#include "CLHEP/Units/PhysicalConstants.h"

using CLHEP::GeV;
using CLHEP::pi;
using CLHEP::twopi;

// Readout lib's
// #include "LArCabling/LArOnOffIdMapping.h"
// #include "TileConditions/TileCablingService.h"

// #include <xAODJet/JetContainer.h>

EventReaderAlg::EventReaderAlg( const std::string& name, ISvcLocator* pSvcLocator ) : 
    AthAlgorithm( name, pSvcLocator )
    , m_ntsvc("THistSvc/THistSvc", name)
    // , m_tileCabling("TileCablingSvc",name) //initialize Tile cabling service
    , m_larCablingKey("LArOnOffIdMap")
    , m_onlineLArID(0)
    , m_tileHWID(nullptr)
    , m_tileID(nullptr)
    , m_tileCabling(nullptr)
    , m_calocell_id(nullptr)
    // , m_ntsvc("THistSvc/THistSvc", name)
    {

  //declareProperty( "Property", m_nProperty = 0, "My Example Integer Property" ); //example property declaration
  declareProperty("LArOnOffMap",m_larCablingKey," LAr cabling map for online and offline.");

}

EventReaderAlg::~EventReaderAlg() {
  // delete m_runNumber;
  // delete m_bcid;
  // delete m_eventNumber;
}

StatusCode EventReaderAlg::initialize() {
  ATH_MSG_INFO ("Initializing " << name() << "...");
  ATH_CHECK( m_tileCablingSvc.retrieve() );  
  m_tileCabling = m_tileCablingSvc->cablingService(); // initialize the cabling service to a pointer.
  ATH_CHECK( detStore()->retrieve(m_onlineLArID, "LArOnlineID") ); // get online ID info from LAr (online)  
  ATH_CHECK( detStore()->retrieve(m_tileHWID) ); // get hardware ID info from TileCal (online)
  ATH_CHECK( detStore()->retrieve(m_tileID) ); // get cell ID info from TileCal (offline)
  ATH_CHECK( detStore()->retrieve (m_calocell_id, "CaloCell_ID") );
  ATH_CHECK( m_larCablingKey.initialize());
  
  // Book the variables to save in the *.root 
  m_Tree = new TTree("data", "data");
  ATH_MSG_INFO("Booking branches...");
  bookBranches(m_Tree); // book TTree with branches
  if (!m_ntsvc->regTree("/rec/", m_Tree).isSuccess()) {
      ATH_MSG_ERROR("could not register tree [data]");
      return StatusCode::FAILURE;
    }

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::execute() {  
  ATH_MSG_DEBUG ("Executing " << name() << "...");
  ATH_MSG_INFO("Cleanning of event variables...");
  clear();

  SG::ReadCondHandle<LArOnOffIdMapping> larCablingHdl(m_larCablingKey);
  const LArOnOffIdMapping* larCabling=*larCablingHdl;

  const xAOD::EventInfo *eventInfo = nullptr;
  const xAOD::CaloClusterContainer* cls = nullptr;
  const xAOD::JetContainer* jets = nullptr;
  const xAOD::PhotonContainer* photonCnt = nullptr;
  const xAOD::TruthParticleContainer* truthPartContainer = nullptr;
  const xAOD::TruthEventContainer* truthEventCnt = nullptr;
  

  // EventInfo
  if(!dumpEventInfo(eventInfo)) ATH_MSG_DEBUG("Event information cannot be collected!");
  // Jets  
  if(!dumpJetsInfo(jets,pr_jetName, larCabling)) ATH_MSG_DEBUG("Jets information cannot be collected!");
  // Clusters  
  if(!dumpClusterInfo(cls,pr_clusterName, larCabling)) ATH_MSG_DEBUG("Cluster information cannot be collected!");
  // Events Truth
  if (pr_isMC){
    if(!dumpTruthEvent(truthEventCnt)) ATH_MSG_DEBUG("Truth Event information cannot be collected!");
  }
  // Particle Truth
  // if(!dumpTruthParticle(truthPartContainer)) ATH_MSG_DEBUG("Truth Particle information cannot be collected!");
  // Photons
  if(!dumpPhotons(photonCnt)) ATH_MSG_DEBUG("Photon container information cannot be collected!");

  // Apply the Zee cut
  if(!dumpZeeCut()) ATH_MSG_DEBUG("Zee cut algorithm cannot be performed!");

  // Test code section
  if (pr_testCode){
    if(!testCode()) ATH_MSG_DEBUG("Test code cannot run!");
  }
  
  m_Tree->Fill();

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::testCode(){

    // TILEDIGITS AND TILERAWCHANNEL
    const TileRawChannelContainer* TileRawCnt = nullptr;
    const TileDigitsContainer* TileDigitsCnt = nullptr;

    ATH_CHECK (evtStore()->retrieve (TileDigitsCnt, "TileDigitsCnt"));

    // test max of all kinds of id's
    unsigned int maxAdcHash = m_tileID->adc_hash_max();
    std::cout << "SW MAX CELLS: " << m_tileID->cell_hash_max() << std::endl;
    std::cout << "SW MAX PMT: " << m_tileID->pmt_hash_max() << std::endl;
    std::cout << "SW MAX ADC: " << maxAdcHash << std::endl;

    maxAdcHash = m_tileHWID->adc_hash_max();
    std::cout << "HW MAX CHANNELS: " << m_tileHWID->channel_hash_max() << std::endl;
    std::cout << "HW MAX ADC: " << maxAdcHash << std::endl;

    int maxChannels = m_tileCabling->getMaxChannels();
    int maxGains = m_tileCabling->getMaxGains();
    std::cout << "MAX CHANNELS (in a drawer): " << maxChannels << std::endl;
    std::cout << "MAX GAINS: " << maxGains << std::endl;
    

    for (const TileDigitsCollection* TileDigColl : *TileDigitsCnt){
      if (TileDigColl->empty() ) continue;

      HWIdentifier adc_id_collection = TileDigColl->front()->adc_HWID();
      // int ros = m_tileHWID->ros(adc_id_collection);
      // int drawer = m_tileHWID->drawer(adc_id_collection);
      // int partition = ros - 1;
      // uint32_t rodBCID = TileDigColl->getRODBCID();

      for (const TileDigits* TileDig : *TileDigColl){
        // ATH_MSG_INFO( "  TileDig->samples(): " << TileDig->samples());
        HWIdentifier adc_id = TileDig->adc_HWID(); // adc id, related to hardware. is different from channel id.

        IdentifierHash adc_ch_hash = m_tileHWID->get_channel_hash(adc_id);
        IdentifierHash adc_hash = m_tileHWID->get_hash(adc_id);

        Identifier swcell_id = m_tileCabling->h2s_cell_id(adc_id); // cell id 
        Identifier swpmt_id = m_tileCabling->h2s_pmt_id(adc_id); // cell id 
        Identifier swadc_id = m_tileCabling->h2s_adc_id(adc_id); // cell id 

        // int tileCh = m_tileHWID->channel(adc_id); // channel from hw perspective (0-48)
        // int tileAdc = m_tileHWID->adc(adc_id); // gain from hw perspective (0 - low, 1 - high)

        // size_t index = (size_t) (adc_hash);
        // ATH_MSG_INFO ("   adc_id: " << adc_id << ". index: " << index <<". ros: " << ros << ". drawer: " << drawer << ". hw_ch_hash_max (hw_adc_hash_max): " << hw_ch_hash_max << "("<< hw_adc_hash_max << ")" ". rodBCID: " << rodBCID);
        // ATH_MSG_INFO ("   adc_id (ros/drawer/ch/adc): " << adc_id << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" << "index " << index << " adc_ch_hash " << adc_ch_hash << " adc_hash " << adc_hash << " swids (cell/pmt/adc): " << swcell_id << " / " << swpmt_id << " / " << swadc_id <<  ") rodBCID: " << rodBCID);

      } // end loop TileDigitsContainer    
    } //end loop TileDigitsCollection


    ATH_CHECK (evtStore()->retrieve( TileRawCnt , "TileRawChannelCnt"));
    for (const TileRawChannelCollection* TileChannelColl : *TileRawCnt){
      for (const TileRawChannel* TileChannel : *TileChannelColl){
        // https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/TileCalorimeter/TileEvent/TileEvent/TileRawData.h
        int tileIndex, tilePmt = 0;
        Identifier rawCellId = TileChannel->cell_ID();
        Identifier rawPmtId = TileChannel->pmt_ID();
        Identifier rawAdcId = TileChannel->adc_ID();

        HWIdentifier rawAdcHwid = TileChannel->adc_HWID();

        int tileAdc       = m_tileHWID->adc(rawAdcHwid);
        int tileCh   = m_tileHWID->channel(rawAdcHwid);
        int drawer    = m_tileHWID->drawer(rawAdcHwid);
        int ros       = m_tileHWID->ros(rawAdcHwid);

        TileChannel->cell_ID_index(tileIndex,tilePmt);
 
        ATH_MSG_INFO ("TileRawID/rawPmtId/rawAdcId/rawAdcHwid (ros/drawer/ch/adc): " << rawCellId <<"/" << rawPmtId << "/" << rawAdcId << "/" << rawAdcHwid << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" ". Index/Pmt: " << tileIndex << "/" << tilePmt << ". Amplitude: " << TileChannel->amplitude() << ". Time: " << TileChannel->time()); 

      } //end loop TileRawChannelContainer
    } //end loop TileRawChannelCollection
      
  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpEventInfo(const xAOD::EventInfo *ei){
  // const xAOD::EventInfo *eventInfo = nullptr;
  ATH_CHECK (evtStore()->retrieve (ei, "EventInfo"));

  e_runNumber   = ei->runNumber();
  e_eventNumber = ei->eventNumber();
  e_bcid        = ei->bcid();
  e_pileup      = ei->actualInteractionsPerCrossing();

  ATH_MSG_INFO ("in execute, runNumber = " << e_runNumber << ei->runNumber() << ", eventNumber = " << e_eventNumber << ei->eventNumber() << ", bcid: " << e_bcid );
  
  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpZeeCut(){
  ATH_MSG_INFO("Dumping Zee cut region...");
  const xAOD::ElectronContainer* electronsCnt  = nullptr;
  const xAOD::EventInfo* ei                    = nullptr;

  ATH_CHECK (evtStore()->retrieve(electronsCnt,"Electrons"));
  ATH_CHECK (evtStore()->retrieve (ei, "EventInfo"));

  int elecIdx = 0;
  int nElec   = electronsCnt->size();

  


  for (auto elec : *electronsCnt){
    

    // https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/TrackingCPEOYE2015
    double d0sig = xAOD::TrackingHelpers::d0significance( elec->trackParticle(), ei->beamPosSigmaX(), ei->beamPosSigmaY(), ei->beamPosSigmaXY() ); 
    ATH_MSG_INFO ("   Electron #" << elecIdx << "/"<< nElec << ": pT= " << elec->pt() << ", charge= "<< elec->charge() <<", eta= "<< elec->eta() << ", me= " << elec->m() << ", d0/d0_sig= "<<elec->trackParticle()->d0() <<"/" << d0sig << ", z0= "<< elec->trackParticle()->z0() );
    elecIdx++;
  }

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpTruthEvent(const xAOD::TruthEventContainer* truthEventCnt){
  ATH_MSG_INFO("Dumping Truth Event Info...");

  ATH_CHECK (evtStore()->retrieve(truthEventCnt, "TruthEvents"));
  xAOD::TruthEventContainer::const_iterator itr; //iterator for truth event
  // for (itr =  truthEventCnt->begin(); itr != truthEventCnt->end(); ++itr ){
  for (const xAOD::TruthEvent* evt : *truthEventCnt){
    ATH_MSG_INFO ("Looping over evt");
    int nVert = evt->nTruthVertices(); // number of truth vertices
    int nPart = evt->nTruthParticles(); // number of truth particles

    ATH_MSG_INFO (" nVert: " << nVert <<", nPart: "<< nPart);

    // int nVert = (*itr)->nTruthVertices(); // number of truth vertices
    // int nPart = (*itr)->nTruthParticles(); // number of truth particles

    for (int iVtx = 0; iVtx < nVert; ++iVtx){
      
      const xAOD::TruthVertex* vertex = evt->truthVertex(iVtx); // get truth vertex
      // const xAOD::TruthVertex* vertex = (*itr)->truthVertex(iVtx); // get truth vertex

      ATH_MSG_INFO ("Loop in vertex #"<< iVtx << ". ");
      
      float vert_x      = vertex->x(); // vertex displacement
      float vert_y      = vertex->y(); // vertex displacement
      float vert_z      = vertex->z(); // vertex displacement in beam direction
      float vert_time   = vertex->t(); // vertex time
      float vert_perp   = vertex->perp(); // Vertex transverse distance from the beam line
      float vert_eta    = vertex->eta(); // Vertex pseudorapidity
      float vert_phi    = vertex->phi(); // Vertex azimuthal angle
      int vert_barcode  = vertex->barcode(); //barcode
      int vert_id       = vertex->id();

      mc_vert_x->push_back(vert_x);
      mc_vert_y->push_back(vert_y);
      mc_vert_z->push_back(vert_z);
      mc_vert_time->push_back(vert_time);
      mc_vert_perp->push_back(vert_perp);
      mc_vert_eta->push_back(vert_eta);
      mc_vert_phi->push_back(vert_phi);
      mc_vert_barcode->push_back(vert_barcode);
      mc_vert_id->push_back(vert_id);

    } //
    for (int iPart = 0; iPart < nPart; ++iPart){
      ATH_MSG_INFO ("Loop in particle #"<< iPart);
      const xAOD::TruthParticle* particle = evt->truthParticle(iPart); //get truth particle
      // const xAOD::TruthParticle* particle = (*itr)->truthParticle(iPart); //get truth particle

      mc_part_energy->push_back(particle->e());
      mc_part_pt->push_back(particle->pt());
      mc_part_pdgId->push_back(particle->pdgId());
      mc_part_m->push_back(particle->m());
      mc_part_phi->push_back(particle->phi());
      mc_part_eta->push_back(particle->eta());
      mc_part_status->push_back(particle->status());
      mc_part_barcode->push_back(particle->barcode());
    } // end loop particles in event
    
  } // end loop truth events container

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpTruthParticle(const xAOD::TruthParticleContainer* truthParticleCnt){
  ATH_MSG_INFO("Dumping Truth Particle...");

  ATH_CHECK (evtStore()->retrieve (truthParticleCnt, "TruthParticles")); // retrieve container

  if (truthParticleCnt->size() == 0) ATH_MSG_INFO("truthParticleCnt is empty!");
  else{
    for (auto* truthParticle : *truthParticleCnt){
      mc_part_energy->push_back(truthParticle->e());
      mc_part_pt->push_back(truthParticle->pt());
      mc_part_pdgId->push_back(truthParticle->pdgId());
      mc_part_m->push_back(truthParticle->m());
      mc_part_phi->push_back(truthParticle->phi());
      mc_part_eta->push_back(truthParticle->eta());
    }
  }
  return StatusCode::SUCCESS;
  
}

StatusCode EventReaderAlg::dumpPhotons(const xAOD::PhotonContainer* photonCnt){
  ATH_MSG_INFO("Dumping Photons...");
  ATH_CHECK (evtStore()->retrieve (photonCnt, "Photons")); // retrieve photons container

  if (photonCnt->size() == 0) ATH_MSG_INFO ("photonCnt is empty!");
  else{
    for (auto* photon : *photonCnt){
      ph_energy->push_back(photon->e());
      ph_pt->push_back(photon->pt());
      ph_phi->push_back(photon->phi());
      ph_eta->push_back(photon->eta());
      ph_m->push_back(photon->m());
    }
  }

  return StatusCode::SUCCESS;
}

int EventReaderAlg::getCaloRegionIndex(const CaloCell* cell){
  if (cell->caloDDE()->is_tile()) return 0; //belongs to Tile
  // else if (cell->caloDDE()->is_lar_em()) return 1; //belongs to EM calorimeter !!
  else if (cell->caloDDE()->is_lar_em_barrel()) return 1; //belongs to EM barrel
  // else if (cell->caloDDE()->is_lar_em_endcap()) return 3; //belongs to EM end cap MESMA COISA DA LAR_EM !!
  else if (cell->caloDDE()->is_lar_em_endcap_inner()) return 2; //belongs to the inner wheel of EM end cap
  else if (cell->caloDDE()->is_lar_em_endcap_outer()) return 3; //belongs to the outer wheel of EM end cap
  else if (cell->caloDDE()->is_lar_hec()) return 4; //belongs to HEC
  else if (cell->caloDDE()->is_lar_fcal()) return 5; //belongs to FCAL
    
  ATH_MSG_ERROR (" #### Region not found for cell offline ID "<< cell->ID() <<" ! Returning -999.");
  return -999; //region not found
}

double EventReaderAlg::fixPhi(double phi){
  // Verify the phi value, if its in -pi,pi interval, then shifts 2pi in the correct direction.
  if (phi < -1*pi) return (phi + 2*pi);
  if (phi >  1*pi) return (phi - 2*pi);
  return phi;
}

double EventReaderAlg::deltaPhi(double phi1, double phi2){
  // Fix phi value for delta_phi calculation, to -pi,+pi interval.
  double deltaPhi = fixPhi(phi1) - fixPhi(phi2);
  return fixPhi(deltaPhi);
}

double EventReaderAlg::deltaR( double deta, double dphi){
  return sqrt( deta*deta + fixPhi(dphi)*fixPhi(dphi) );
}

StatusCode EventReaderAlg::dumpClusterInfo(const xAOD::CaloClusterContainer *cls, const std::string& clsName, const LArOnOffIdMapping* larCabling ){
  //Get cable map via read conditions handle
  // SG::ReadCondHandle<LArOnOffIdMapping> larCablingHdl(m_larCablingKey);
  // const LArOnOffIdMapping* larCabling=*larCablingHdl;
  const LArDigitContainer* LarDigCnt = nullptr;
  const TileDigitsContainer* TileDigCnt = nullptr;
  const TileRawChannelContainer* TileRawCnt = nullptr;
  const LArRawChannelContainer* LArRawCnt = nullptr;

  int clusterIndex = 0; //index of cluster in the same event.

  // calorimeter level
  std::bitset<200000>       larClusteredDigits;
  std::bitset<65536>        tileClusteredDigits;
  std::vector<size_t>       caloHashMap;
  std::vector<HWIdentifier> channelHwidInClusterMap; //unique for any readout in the ATLAS
  std::vector<int>          cellIndexMap;
  std::vector<float>        channelIndexMap;
  // cell level
  std::vector<double>       cellClusDeltaEta;//unused
  std::vector<double>       cellClusDeltaPhi; //unused
  std::vector<double>       cellGranularityEtaInClusterMap; //unused
  std::vector<double>       cellGranularityPhiInClusterMap; //unused
  // channel level
  std::vector<double>       channelEnergyInClusterMap;
  std::vector<double>       channelTimeInClusterMap;
  std::vector<double>       channelEtaInClusterMap;
  std::vector<double>       channelPhiInClusterMap;

  std::vector<int>          channelCaloRegionMap;
  std::vector<bool>         badChannelMap;
  
  //*****************************************
  
  ATH_CHECK (evtStore()->retrieve ( cls, clsName )); //reading from an ESD file.
  for (const xAOD::CaloCluster* cl : *cls) {

    double  clusEta           = cl->eta();
    double  clusPhi           = cl->phi();
    double  clusEt            = cl->et();
    double  clusPt            = cl->pt();
    double  clusCellSumE_Eta  = 0.0;
    double  clusCellSumE_Phi  = 0.0;
    double  clusCellSumE      = 0.0;
    double  clusEtaBaryc      = 0.0;
    double  clusPhiBaryc      = 0.0;
    // double cellClusDistPhi = 0.0;    
    
    ATH_MSG_INFO ("Cluster: "<< clusterIndex <<", numberCells: " << cl->numberCells() << ", e = " << cl->et() << " , pt = " << cl->pt() << " , eta = " << cl->eta() << " , phi = " << cl->phi());

    // loop over cells in cluster
    auto itrCells = cl->cell_begin();       
    auto itrCellsEnd = cl->cell_end(); 
    int cellNum = 0; //cell number just for printing out
    int cellIndex = 0; //cell index inside cluster (ordered)
    // int readOutIndex = 0; // channel index inside cluster

      for ( auto itCells=itrCells; itCells != itrCellsEnd; ++itCells){ 
      
        //loop over the cells in a cluster and do stuff...
        const CaloCell* cell = (*itCells);
 
        if (cell) { // check for empty clusters
          Identifier cellId = cell->ID(); //id 0x2d214a140000000. is a 64-bit number that represent the cell.

          int caloRegionIndex = getCaloRegionIndex(cell); // custom caloRegionIndex based on caloDDE

          double  eneCell   = cell->energy();
          double  timeCell  = cell->time();
          double  etaCell   = cell->eta();
          double  phiCell   = cell->phi();          
          int     gainCell  = cell->gain(); // CaloGain type
          // int     layerCell = cell->caloDDE()->getLayer(); //
          int     layerCell = m_calocell_id->calo_sample(cell->ID());
          bool    badCell   = cell->badcell(); // applied to LAR channels. For Tile, we use TileCell* tCell->badch1() or 2.
          bool    isTile    = cell->caloDDE()->is_tile(); // cell detector descriptors: athena/Calorimeter/CaloDetDescr/
          bool    isLAr     = cell->caloDDE()->is_lar_em();
          bool    isLArFwd  = cell->caloDDE()->is_lar_fcal();
          bool    isLArHEC  = cell->caloDDE()->is_lar_hec();
          double  detaCell  = cell->caloDDE()->deta(); //delta eta - granularity
          double  dphiCell  = cell->caloDDE()->dphi(); //delta phi - granularity

          // get cluster barycenter:
          clusCellSumE      = clusCellSumE + fabs(eneCell);
          clusCellSumE_Eta  = clusCellSumE_Eta + ( fabs(eneCell) * etaCell);
          clusCellSumE_Phi  = clusCellSumE_Phi + ( fabs(eneCell) * phiCell );
          
          // double cellClusterDEta = etaCell

          // cell->caloDDE()->print(); //print out eta, phi and r.
              
          IdentifierHash subCaloHash = cell->caloDDE()->subcalo_hash(); // sub-calo hash. Value specific for sub-calo region.
          IdentifierHash caloHash = cell->caloDDE()->calo_hash(); // calo-hash. Value relative to entire calo region.
          IdentifierHash chidHash, adcidHash;
          HWIdentifier chhwid, adchwid;
          size_t index, indexPmt1, indexPmt2;

          // ========= TileCal ==========
          if (isTile){ //Tile
            const TileCell* tCell = (const TileCell*) cell; //convert to TileCell class to access its specific methods.
            // ATH_MSG_INFO ("tCell->quality() " << tCell->quality());

            Identifier tileCellId = tCell->ID(); //swid for given tile sub-calo hash.

            // main TileCell readout properties:
            int gain1   = tCell->gain1(); //gain type: 0 LG, 1 HG
            int gain2   = tCell->gain2();
            // int qual1   = tCell->qual1(); // quality factor (chi2)
            // int qual2   = tCell->qual2();
            bool badch1 = tCell->badch1(); //is a bad ch? (it is in the bad channel's list?)
            bool badch2 = tCell->badch2();
            bool noch1  = (gain1<0 || gain1>1); // there is a ch?
            bool noch2  = (gain2<0 || gain2>1);
            float time1 = tCell->time1(); // reco time in both chs (OF2)
            float time2 = tCell->time2();
            float ene1  = tCell->ene1(); //reco energy in MeV (OF2) in both chs
            float ene2  = tCell->ene2();

            // verify if it's a valid ch pmt 1
            if ( !noch1 && (!badch1 || !pr_noBadCells ) ){
              // ...->adc_id(CellID, pmt 0 or 1, gain1() or gain2())
              HWIdentifier adchwid_pmt1 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,0,gain1));
              IdentifierHash hashpmt1 = m_tileHWID->get_channel_hash(adchwid_pmt1); //get pmt ch hash
              indexPmt1 = (size_t) (hashpmt1);

              int adc1     = m_tileHWID->adc(adchwid_pmt1);
              int channel1 = m_tileHWID->channel(adchwid_pmt1);
              int drawer1  = m_tileHWID->drawer(adchwid_pmt1);
              int ros1     = m_tileHWID->ros(adchwid_pmt1);

              if (pr_printCellsClus){ //optional to help debugging
                ATH_MSG_INFO (" in IsTile: Cell (layer) "<< cellNum <<" ("<< layerCell <<") ID (ros/draw/ch/adc) " << tileCellId << " ( "<< ros1 << "/" << drawer1 << "/" << channel1 << "/" << adc1 << "). HWID/indexPmt " << adchwid_pmt1 << "/" << indexPmt1 << ". ene1 (total)" << ene1 << "(" << eneCell << "). time(final) " << time1 << "(" << timeCell <<"). gain " << gain1 );
              }
              // test if there is conflict in the map (only for debugging reason)
              if (tileClusteredDigits.test(indexPmt1)) {
                ATH_MSG_ERROR (" ##### Error Tile: Conflict index of PMT1 position in PMT map. Position was already filled in this event. Skipping the cell of index: " << indexPmt1 << " and hwid: " << adchwid_pmt1);
                continue;
                }
              else { //If there wasn't an error, 
                tileClusteredDigits.set(indexPmt1); //fill the map.
                caloHashMap.push_back(indexPmt1);
                cellIndexMap.push_back(cellIndex);
                channelIndexMap.push_back(cellIndex + 0.1); // adds 0.1 to the cell index, to indicate this is the PMT 1
                channelHwidInClusterMap.push_back(adchwid_pmt1);
                channelTimeInClusterMap.push_back(time1);
                channelEnergyInClusterMap.push_back(ene1);
                channelCaloRegionMap.push_back(caloRegionIndex);
                if (!pr_noBadCells) badChannelMap.push_back(badch1);

                // readOutIndex++;
              }
              
            } //end-if its a valid pmt1
            // else if (noch1)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster has not a valid pmt1 channel!");
            // else if (badch1)  ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt1 channel!");
            // else              ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster, had its pmt1 channel skipped for no known reason.");
            
            // verify if it's a valid ch pmt 2
            if ( !noch2 && (!badch2 || !pr_noBadCells) ){
              HWIdentifier adchwid_pmt2 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,1,gain2));            
              IdentifierHash hashpmt2 = m_tileHWID->get_channel_hash(adchwid_pmt2);            
              indexPmt2 = (size_t) (hashpmt2);

              int adc2     = m_tileHWID->adc(adchwid_pmt2);
              int channel2 = m_tileHWID->channel(adchwid_pmt2);
              int drawer2  = m_tileHWID->drawer(adchwid_pmt2);
              int ros2     = m_tileHWID->ros(adchwid_pmt2);

              if (pr_printCellsClus){ //optional to help debugging
                ATH_MSG_INFO (" in IsTile: Cell (layer) "<< cellNum <<" ("<< layerCell <<") ID (ros/draw/ch/adc) " << tileCellId << " ("<< ros2 << "/" << drawer2 << "/" << channel2 << "/" << adc2 <<"). HWID/indexPmt " << adchwid_pmt2 << "/" << indexPmt2 <<  ". ene2(total)" << ene2 << "(" << eneCell << "). time(final) "  << time2 << "(" << timeCell << "). gain " << gain2 );
              }              
              // test if there is conflict in the map (only for debugging reason)
              if (tileClusteredDigits.test(indexPmt2)) {
                ATH_MSG_ERROR (" ##### Error Tile: Conflict index of PMT2 position in PMT map. Position was already filled in this event. Skipping the cell of index: " << indexPmt2 << " and hwid: " << adchwid_pmt2);
                continue;
                }
              else {//If there wasn't an error,
                tileClusteredDigits.set(indexPmt2); //fill the map.
                caloHashMap.push_back(indexPmt2);
                cellIndexMap.push_back(cellIndex);
                channelIndexMap.push_back(cellIndex + 0.2); // adds 0.2 to the cell index, to indicate this is the PMT2
                channelHwidInClusterMap.push_back(adchwid_pmt2);
                channelTimeInClusterMap.push_back(time2);
                channelEnergyInClusterMap.push_back(ene2);
                channelCaloRegionMap.push_back(caloRegionIndex);
                if (!pr_noBadCells) badChannelMap.push_back(badch2);

                // readOutIndex++;
              }              
            }
            if (noch1)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster has not a valid pmt1 channel!");
            if (noch2)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster has not a valid pmt2 channel!");
            if ( (badch1 && badch2) && pr_noBadCells )  {
              ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt1 and pmt2 channel! Skipping cell.");
              continue;
            }
            if ( (badch1 && noch2) && pr_noBadCells ) {
              ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt1 and has no valid pmt2 channel! Skipping cell.");
              continue;
            }
            // else if (noch2)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster has not a valid pmt2 channel!");
            // else if (badch1 && badch2)  {
            //   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster is listed as a bad pmt2 channel!");
            //   continue;
            // }
            // else              ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in cluster, had its pmt2 channel skipped for no known reason.");
            // } //end if id
          } // end if isTile

          // ========= LAr ==========
          else if ((isLAr) || (isLArFwd) || (isLArHEC)) { //LAr
            chhwid = larCabling->createSignalChannelID(cellId);
            // std::string hwid_lar = chid.getString();
            // ATH_MSG_INFO (typeid(chhwid).name());
            chidHash =  m_onlineLArID->channel_Hash(chhwid);
            index = (size_t) (chidHash);

            if (larClusteredDigits.test(index)) {
              ATH_MSG_ERROR (" ##### Error LAr: Conflict index position in Channel map. Position was already filled in this event. Skipping the cell of index: " << index << " and hwid: " << chhwid);
              continue;
              }
            else if (badCell && pr_noBadCells){
              ATH_MSG_INFO (" (LAr) Cell "<< cellNum <<" in cluster is a bad LAr channel! Skipping the cell.");
            continue;
            }
            else{
              larClusteredDigits.set(index);
              caloHashMap.push_back(index);
              cellIndexMap.push_back(cellIndex);
              channelIndexMap.push_back(cellIndex + 0.0);
              channelHwidInClusterMap.push_back(chhwid);
              channelTimeInClusterMap.push_back(timeCell);
              channelEnergyInClusterMap.push_back(eneCell);
              channelCaloRegionMap.push_back(caloRegionIndex);
              if (!pr_noBadCells) badChannelMap.push_back(badCell);
            }
            // readOutIndex++;
            if (pr_printCellsClus){ //optional to help debugging
              ATH_MSG_INFO (" in IsLAr: Cell (layer) "<< cellNum << " (" << layerCell <<") ID: " << cellId << "). HwId (B_EC/P_N/FeedTr/slot/ch) " <<  chhwid << " (" << m_onlineLArID->barrel_ec(chhwid) << "/" << m_onlineLArID->pos_neg(chhwid) << "/"<< m_onlineLArID->feedthrough(chhwid) << "/" << m_onlineLArID->slot(chhwid) << "/" << m_onlineLArID->channel(chhwid) << ") . Index: " << index << ". ene " << eneCell << ". time " << timeCell << ". D_eta: " << detaCell << ". D_phi: " << dphiCell << " ):");
            }

            // if 
          } //end else LAr

          else {
            ATH_MSG_ERROR (" ####### ERROR ! No CaloCell region was found!");
            continue;
            }

          // ##############################
          //  CELL INFO DUMP (CLUSTER)
          // ##############################
          // double cluster_dphi = fabs(clusPhi-phiCell)  < pi ? (clusPhi-phiCell) : twopi - fabs(clusPhi-phiCell); //wrap correction
          double cluster_dphi = (clusPhi-phiCell)  < -pi ? twopi + (clusPhi-phiCell) : ( clusPhi-phiCell  > pi ? (clusPhi-phiCell) - twopi : clusPhi-phiCell );

          // if (fabs(clusPhi-phiCell)  < pi) ATH_MSG_INFO ("abs deltaPhi = " << fabs(clusPhi-phiCell));
          // else ATH_MSG_INFO ("(cluster) deltaPhi = " << (clusPhi-phiCell) <<". Corrected value is: " << cluster_dphi );

          c_clusterIndex_cellLvl->push_back(clusterIndex);
          c_clusterCellIndex->push_back(cellIndex);
          c_cellGain->push_back(gainCell);
          c_cellLayer->push_back(layerCell);
          c_cellEta->push_back(etaCell);
          c_cellPhi->push_back(phiCell);
          c_cellDEta->push_back(detaCell);
          c_cellDPhi->push_back(dphiCell);
          c_cellToClusterDEta->push_back(clusEta - etaCell);
          c_cellToClusterDPhi->push_back(cluster_dphi);
          c_cellRegion->push_back(caloRegionIndex);
          

          // ##############################
          cellIndex++;
          cellNum++;
        } //end if-cell is empty verification
      } // end loop at cells inside cluster

    // ##############################
    //  CLUSTER INFO DUMP
    // ##############################
    clusEtaBaryc      = clusCellSumE_Eta / clusCellSumE;
    clusPhiBaryc      = fixPhi( clusCellSumE_Phi / clusCellSumE ); // verify if phi value is inside -pi,pi
    // ATH_MSG_INFO ("clusEtaBaryc " << clusEtaBaryc << " clusPhiBaryc " << clusPhiBaryc);

    c_clusterIndex->push_back(clusterIndex);
    c_clusterEnergy->push_back(clusEt);
    c_clusterEta->push_back(clusEta);
    c_clusterPhi->push_back(clusPhi);
    c_clusterPt->push_back(clusPt);
    c_clusterEta_calc->push_back(clusEtaBaryc);
    c_clusterPhi_calc->push_back(clusPhiBaryc);

    // ##############################
    //  DIGITS CHANNEL INFO DUMP (CLUSTER)
    // ##############################
    // Loop over ALL channels in LAr Digit Container.
    // Dump only the channels inside the previous clusters

    // ========= LAr ==========
    if (larClusteredDigits.any()){
      ATH_MSG_INFO (" (Cluster) Dumping LAr Digits...");
      ATH_CHECK (evtStore()->retrieve (LarDigCnt, pr_larDigName));

      for (const LArDigit* dig : *LarDigCnt) {
        HWIdentifier channelID = dig->channelID();
        IdentifierHash idHash = m_onlineLArID->channel_Hash(channelID);
        size_t index = (size_t) (idHash);

        if (larClusteredDigits.test(index)) {
          for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){
            if (channelHwidInClusterMap[k]==channelID){
              // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == adc_id " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << channelID );
              // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );
              
              // digits
              std::vector<short> larDigitShort = dig->samples();
              std::vector<float> larDigit( larDigitShort.begin(), larDigitShort.end() ); // get vector conversion from 'short int' to 'float'
              // if (larDigitShort != larDigit) ATH_MSG_ERROR (" ###### LAr DIGITS: Digits conversion from short to float has changed the actual digits value !!!");
              // c_larSamples->push_back(dig->samples());
              c_channelDigits->push_back(larDigit);

              // Cell / readout data
              c_channelEnergy->push_back(channelEnergyInClusterMap[k]);
              c_channelTime->push_back(channelTimeInClusterMap[k]);
              if (!pr_noBadCells) c_channelBad->push_back(badChannelMap[k]);

              // DataDescriptorElements

              // Channel info
              int barrelEc = m_onlineLArID->barrel_ec(channelID);
              int posNeg = m_onlineLArID->pos_neg(channelID);
              int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
              int slot = m_onlineLArID->slot(channelID);
              int chn = m_onlineLArID->channel(channelID);      
              std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, slot, chn } ; //unite info
              c_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

              // important indexes
              c_clusterChannelIndex->push_back(channelIndexMap[k]);//each LAr cell is an individual read out.    
              c_clusterIndex_chLvl->push_back(clusterIndex);  // what roi this cell belongs at the actual event: 0, 1, 2 ... 
              c_channelHashMap->push_back(index); // online id hash for channel
              
              if (pr_printCellsClus){
                ATH_MSG_INFO ("(Cluster) In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId (B_EC/P_N/FeedTr/slot/ch) " <<  channelID << " (" << barrelEc << "/" << posNeg << "/"<< feedThr << "/" << slot << "/" << chn << ". Dig (float): " << larDigit);
              }
            } //end-if caloHashMap matches
            // else{
            //   if (caloHashMap[k] > m_onlineLArID->channelHashMax() ) ATH_MSG_ERROR ("###### HASH INDEX OUT OF BONDS FOR LAR !...");
            // }
          } //end for loop over caloHashMaps
        } // end-if clustBits Test
      } //end loop over LAr digits container
    } //end-if larClustBits is not empty


      // ========= TileCal ==========
    if (tileClusteredDigits.any()) {
      ATH_MSG_INFO ("(Cluster) Dumping tile PMT's Digits...");
      // if(!dumpTileDigits(TileDigCnt,"TileDigitsCnt",tileClusteredDigits, tileHashMap, cellIndex, readOutIndex, clusterIndex)) ATH_MSG_DEBUG("Tile Digits information cannot be collected!");
      ATH_CHECK (evtStore()->retrieve (TileDigCnt, pr_tileDigName));
  
      for (const TileDigitsCollection* TileDigColl : *TileDigCnt){
        if (TileDigColl->empty() ) continue;

        HWIdentifier adc_id_collection = TileDigColl->front()->adc_HWID();
        // uint32_t rodBCID = TileDigColl->getRODBCID();

        for (const TileDigits* dig : *TileDigColl){
          HWIdentifier adc_id = dig->adc_HWID();

          IdentifierHash adcIdHash = m_tileHWID->get_channel_hash(adc_id);
          // IdentifierHash adcIdHash = m_tileHWID->get_hash(adc_id);
          
          size_t index = (size_t) (adcIdHash);

          if (index <= m_tileHWID->adc_hash_max()){ //safety verification
            if (tileClusteredDigits.test(index)){ //if this channel index is inside cluster map of cells
              // Im aware this may not be the most optimized way to dump the containers info, but the solution below
              // was done to ensure that the data order will be the same for different Branches. It's different from working just with the athena, 
              // because the order here is very important to keep the data links.
              for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInClusterMap[k] == adc_id){ //find the index from pmt index to proceed.
                  // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == adc_id " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << adc_id );
                  // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );

                  Identifier cellId = m_tileCabling->h2s_cell_id(adc_id); //gets cell ID from adc
                  
                  // Digits     
                  // c_tileSamples->push_back(dig->samples()); //temporarily maintained
                  c_channelDigits->push_back(dig->samples());

                  // Channel / readout data
                  c_channelEnergy->push_back(channelEnergyInClusterMap[k]);
                  c_channelTime->push_back(channelTimeInClusterMap[k]);
                  if (!pr_noBadCells) c_channelBad->push_back(badChannelMap[k]);

                  //channelInfo
                  int ros = m_tileHWID->ros(adc_id_collection); // index of ros (1-4)
                  int drawer = m_tileHWID->drawer(adc_id_collection); //drawer for each module (0-63)
                  int partition = ros - 1; //partition (0-3)
                  int tileChNumber = m_tileHWID->channel(adc_id); // channel from hw perspective (0-48)
                  int tileAdcGain = m_tileHWID->adc(adc_id); // gain from hw perspective (0 - low, 1 - high)
                  std::vector<int> chInfo{partition, drawer, tileChNumber, tileAdcGain}; //unite info

                  c_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  // important indexes
                  c_clusterChannelIndex->push_back(channelIndexMap[k]);
                  c_clusterIndex_chLvl->push_back(clusterIndex); // what roi this ch belongs
                  c_channelHashMap->push_back(index); // online id hash for channel
                  // c_clusterChannelIndex->push_back(readOutIndex); // each LAr cell is an individual read out.   
                  // c_clusterCellIndex->push_back(cellIndex); // MUST CORRECT THIS, NOT WORKING
                  
                  if (pr_printCellsClus){ //option for debugging
                    ATH_MSG_INFO ("In DumpTile Digits:" << " ReadOutIndex: " << channelIndexMap[k] << ". ID: "<< cellId << ". adc_id (ros/draw/ch/adc) " << adc_id << " (" << ros << "/" << drawer << "/" << tileChNumber << "/" << tileAdcGain << ")" <<  ". Dig: " << dig->samples());   
                  }
                  // readOutIndex++;
                  // cellIndex++;
                } // end if-pmt index comparison
                // if (caloHashMap[k] > m_tileHWID->adc_hash_max()){
                //   ATH_MSG_ERROR (" ###### HASH INDEX OUT OF BONDS FOR TILE !...");
                // } //end else
              }// end loop over pmt-indexes
            } // end if digit index exist on clusteredDigits
          } //end if hash exists
        } // end loop TileDigitsCollection
      } //end loop TileDigitsContainer
    } // end DumpTileDigits

    // ##############################
    //  RAW CHANNEL INFO DUMP (CLUSTER)
    // ##############################

    // ========= LAr ==========
    if (larClusteredDigits.any()) {
      ATH_MSG_INFO ("(Cluster) Dumping LAr Raw Channel...");
      ATH_CHECK (evtStore()->retrieve( LArRawCnt , pr_larRawName));

      for (const LArRawChannel& LArChannel : *LArRawCnt){
        // for (const TileRawChannel* TileChannel : *TileChannelColl){

          HWIdentifier channelID = LArChannel.channelID();
          IdentifierHash chHwidHash = m_onlineLArID->channel_Hash(channelID);
          size_t index = (size_t) (chHwidHash);

          if (larClusteredDigits.test(index)){ //if this channel index is inside 
            for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInClusterMap[k] == channelID){
                  // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == channelID " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << channelID );
                  
                  //RawCh info
                  int rawEnergy        = LArChannel.energy(); // energy in MeV (rounded to integer)
                  int rawTime          = LArChannel.time();    // time in ps (rounded to integer)            
                  uint16_t rawQuality  = LArChannel.quality(); // quality from pulse reconstruction
                  int provenance       = (int) LArChannel.provenance(); // its uint16_t
                  float rawEnergyConv  = (float) (rawEnergy); 
                  float rawTimeConv    = (float) (rawTime);
                  float rawQualityConv = (float) (rawQuality);

                  // Channel info
                  int barrelEc = m_onlineLArID->barrel_ec(channelID);
                  int posNeg = m_onlineLArID->pos_neg(channelID);
                  int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
                  int slot = m_onlineLArID->slot(channelID);
                  int chn = m_onlineLArID->channel(channelID);      
                  std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, slot, chn } ; //unite info
                  c_rawChannelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  

                  if ( (rawEnergy != rawEnergyConv) || (rawTime != rawTimeConv) || (rawQuality != rawQualityConv) ){
                    ATH_MSG_ERROR (" ###### LAR RAW CHANNEL: Value conversion from int to float of amplitude, time or quality (uint16_t) had changed its actual value !!!");
                  }
                  c_rawChannelAmplitude->push_back(rawEnergyConv);
                  c_rawChannelTime->push_back(rawTimeConv);
                  c_rawChannelQuality->push_back(rawQualityConv);
                  c_rawChannelPedProv->push_back(provenance);

                  // important indexes
                  c_clusterRawChannelIndex->push_back(channelIndexMap[k]);
                  c_clusterIndex_rawChLvl->push_back(clusterIndex); // what roi this ch belongs

                  if (pr_printCellsClus){ //optional to help debugging

                    HWIdentifier hardwareID = LArChannel.hardwareID();
                    HWIdentifier identifyID = LArChannel.identify();
                    // uint16_t     provenance = LArChannel.provenance();

                    ATH_MSG_INFO ("In DumpLAr Raw "<< channelIndexMap[k] <<": hardwareID (B_EC/P_N/feedThr/slot/chn): " << hardwareID << "(" << barrelEc << "/" << posNeg << "/" << feedThr << "/" << slot << "/" << chn << ")" << ". Energy: " << rawEnergyConv << ". Time: " << rawTimeConv << ". Provenance: " << provenance << ". Quality: " << rawQualityConv);
                  } // end-if optional cell print
                } // end-if hwid match rawChID
              } // end loop over HWIDClusterMap
          } // end-if clust.test
        // } //end loop LArRawChannelCollection
      } //end loop LArRawChannelContainer
    } // end-if clustBits have Cells


    // ========= TileCal ==========
    if (tileClusteredDigits.any()) {
      ATH_MSG_INFO ("Dumping tile PMT's Raw Channel...");
      ATH_CHECK (evtStore()->retrieve( TileRawCnt , pr_tileRawName));

      for (const TileRawChannelCollection* TileChannelColl : *TileRawCnt){
        for (const TileRawChannel* TileChannel : *TileChannelColl){

          HWIdentifier rawAdcHwid = TileChannel->adc_HWID();
          IdentifierHash chHwidHash = m_tileHWID->get_channel_hash(rawAdcHwid);
          size_t index = (size_t) (chHwidHash);


          if (tileClusteredDigits.test(index)){ //if this channel index is inside 
            for (unsigned int k=0 ; k < channelHwidInClusterMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInClusterMap[k] == rawAdcHwid){
                  // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == rawAdcHwid " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << rawAdcHwid );
                  
                  // Channel info
                  int tileAdc         = m_tileHWID->adc(rawAdcHwid);
                  int tileCh          = m_tileHWID->channel(rawAdcHwid);
                  int drawer          = m_tileHWID->drawer(rawAdcHwid);
                  int ros             = m_tileHWID->ros(rawAdcHwid);
                  int partition       = ros - 1;
                  std::vector<int> chInfo{partition, drawer, tileCh, tileAdc}; //unite info
                  c_rawChannelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  //RawCh info
                  float rawAmplitude  = TileChannel->amplitude(); // amplitude in ADC counts (max=1023)
                  float rawTime       = TileChannel->time();    // time relative to triggering bunch
                  // quality is a number in [0,1] obtained by integrating the parent
                  // distribution for the "goodness" from the DSP.  In hypothesis testing
                  // terms, it is the significance of the hypothisis that the input
                  // to the DSP matches the signal-model used to tune the DSP.                  
                  float rawQuality    = TileChannel->quality();// quality of the sampling distribution
                  float rawPedestal   = TileChannel->pedestal();// reconstructed pedestal value
                  c_rawChannelAmplitude->push_back(rawAmplitude);
                  c_rawChannelTime->push_back(rawTime);
                  c_rawChannelPedProv->push_back(rawPedestal);
                  c_rawChannelQuality->push_back(rawQuality);

                  // important indexes
                  c_clusterRawChannelIndex->push_back(channelIndexMap[k]);
                  c_clusterIndex_rawChLvl->push_back(clusterIndex); // what roi this ch belongs

                  if (pr_printCellsClus){ //optional to help debugging
                    int tileIndex, tilePmt;

                    Identifier rawCellId = TileChannel->cell_ID();
                    Identifier rawPmtId = TileChannel->pmt_ID();
                    Identifier rawAdcId = TileChannel->adc_ID();

                    TileChannel->cell_ID_index(tileIndex,tilePmt);

                    ATH_MSG_INFO ("In DumpTile Raw "<< channelIndexMap[k] <<": rawAdcHwid (ros/drawer/ch/adc): " << rawAdcHwid << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" ". Index/Pmt: " << tileIndex << "/" << tilePmt << ". Amplitude: " << rawAmplitude << ". Time: " << rawTime << ". Ped: " << rawPedestal << ". Quality: " << rawQuality);
                  } // end-if optional cell print
                } // end-if hwid match rawChID
              } // end loop over HWIDClusterMap
          } // end-if clust.test
        } //end loop TileRawChannelCollection
      } //end loop TileRawChannelContainer
    } // end-if clustBits have Cells
    

    
    clusterIndex++;

    larClusteredDigits.reset();
    tileClusteredDigits.reset();

    caloHashMap.clear();
  
    channelHwidInClusterMap.clear();
    cellIndexMap.clear();
    channelIndexMap.clear();
    channelEnergyInClusterMap.clear();
    channelTimeInClusterMap.clear();
    channelEtaInClusterMap.clear();
    channelPhiInClusterMap.clear();
    cellGranularityEtaInClusterMap.clear();
    cellGranularityPhiInClusterMap.clear();
    channelCaloRegionMap.clear();
    if (!pr_noBadCells) badChannelMap.clear();
    cellClusDeltaEta.clear();
    cellClusDeltaPhi.clear();
  } // end loop at clusters inside container

  ATH_MSG_INFO ("   RawChannel: "<< c_rawChannelAmplitude->size() <<" channels dumped, from " << c_cellEta->size() <<" cluster cells. ");
  ATH_MSG_INFO ("   Digits: "<< c_channelDigits->size() <<" channels dumped, out of " << c_rawChannelAmplitude->size() <<" raw channel cluster channels. ");
  if (c_channelDigits->size() == c_rawChannelAmplitude->size()){
    ATH_MSG_INFO ("   ALL Digits from the cluster were dumped successfully!");}
  else{
    ATH_MSG_INFO ("   The digits from "<< (c_rawChannelAmplitude->size() - c_channelDigits->size()) <<" channels are missing!");}

  return StatusCode::SUCCESS;
}

StatusCode EventReaderAlg::dumpJetsInfo(const xAOD::JetContainer *jets, const std::string& jetAlg, const LArOnOffIdMapping* larCabling ){

  const CaloCellContainer*        CaloCnt      = nullptr; // for loop inside 'AllCalo'
  const LArDigitContainer*        LarDigCnt    = nullptr;
  const TileDigitsContainer*      TileDigCnt   = nullptr;
  const TileRawChannelContainer*  TileRawCnt   = nullptr;
  const LArRawChannelContainer*   LArRawCnt    = nullptr;

  float r = pr_roiRadius; //jet ROI radius
  int jetRoiIndex = 0;  //index of jet in the same event.

  // calorimeter level
  std::bitset<200000>       larJetRoiDigits;
  std::bitset<65536>        tileJetRoiDigits;
  std::vector<size_t>       caloHashMap;
  std::vector<HWIdentifier> channelHwidInJetRoiMap; //unique for any readout in the ATLAS
  std::vector<int>          cellIndexMap;
  std::vector<float>        channelIndexMap;
  // cell level
  // std::vector<double>       cellClusDeltaEta;
  // std::vector<double>       cellClusDeltaPhi;
  std::vector<double>       cellGranularityEtaInJetRoiMap;
  std::vector<double>       cellGranularityPhiInJetRoiMap;
  // channel level
  std::vector<double>       channelEnergyInJetRoiMap;
  std::vector<double>       channelTimeInJetRoiMap;
  std::vector<double>       channelEtaInJetRoiMap;
  std::vector<double>       channelPhiInJetRoiMap;

  std::vector<int>          channelCaloRegionMap;
  std::vector<bool>         badChannelMap;

  //*****************************************
  
  ATH_CHECK (evtStore()->retrieve (jets, jetAlg )); //get the jet container from file in evt store
  ATH_MSG_INFO("Number of jets: " <<  jets->size());
  
  for (const xAOD::Jet* jet : *jets){ //loop over jets in container

    double jetPt   = jet->pt();
    double jetEta  = jet->eta();
    double jetPhi  = jet->phi();

    ATH_MSG_INFO ("Jet:   pt: " << jet->pt() << " MeV" << ". Eta; Phi: " << jet->eta() << "; " << jet->phi());

    int cellNum = 0; //cell number just for printing out
    int cellIndex = 0; //cell index inside cluster (ordered)

    ATH_CHECK (evtStore()->retrieve (CaloCnt, "AllCalo")); // point to all calo cell container
    
    for (const CaloCell* cell : *CaloCnt ){ //loop over cells and verify if its inside jet ROI

      double  etaCell   = cell->eta();
      double  phiCell   = cell->phi();

      //checking if cell is in jet's ROI
      double jetRoi_deta = (jetEta-etaCell);
      double jetRoi_dphi = deltaPhi( jetPhi , phiCell );
      // double jetRoi_dR   = deltaR( jetRoi_deta, jetRoi_dphi);
      // bool  insideRoi = jetRoi_dR < r; // circle of radius r around the jet
      bool  insideRoi = (fabs(jetRoi_deta) < r) && (fabs(jetRoi_dphi) < r); // square window of 2*r

      if (insideRoi){
       
        Identifier cellId = cell->ID(); //id 0x2d214a140000000. is a 64-bit number that represent the cell.

        int caloRegionIndex = getCaloRegionIndex(cell); // custom caloRegionIndex based on caloDDE

        double  eneCell   = cell->energy();
        double  timeCell  = cell->time();
                  
        int     gainCell  = cell->gain(); // CaloGain type
        // int     layerCell = cell->caloDDE()->getLayer(); //
        int     layerCell = m_calocell_id->calo_sample(cell->ID());
        bool    badCell   = cell->badcell(); // applied to LAR channels. For Tile, we use TileCell* tCell->badch1() or 2.
        bool    isTile    = cell->caloDDE()->is_tile(); // cell detector descriptors: athena/Calorimeter/CaloDetDescr/
        bool    isLAr     = cell->caloDDE()->is_lar_em();
        bool    isLArFwd  = cell->caloDDE()->is_lar_fcal();
        bool    isLArHEC  = cell->caloDDE()->is_lar_hec();
        double  detaCell  = cell->caloDDE()->deta(); //delta eta - granularity
        double  dphiCell  = cell->caloDDE()->dphi(); //delta phi - granularity

        // ATH_MSG_INFO (" Cell ID " << cellId << "and Eta/Phi " << etaCell << "/" << phiCell << " from region " << caloRegionIndex << " is inside Jet radius r=" << r << " and Eta/Phi " << jetEta << "/" << jetPhi );

        IdentifierHash subCaloHash = cell->caloDDE()->subcalo_hash(); // sub-calo hash. Value specific for sub-calo region.
        IdentifierHash caloHash = cell->caloDDE()->calo_hash(); // calo-hash. Value relative to entire calo region.
        IdentifierHash chidHash, adcidHash;
        HWIdentifier chhwid, adchwid;
        size_t index, indexPmt1, indexPmt2;

        // ========= TileCal ==========
        if (isTile){ //Tile
          const TileCell* tCell = (const TileCell*) cell; //convert to TileCell class to access its specific methods.
          // ATH_MSG_INFO ("tCell->quality() " << tCell->quality());

          Identifier tileCellId = tCell->ID(); //swid for given tile sub-calo hash.

          // main TileCell readout properties:
          int gain1   = tCell->gain1(); //gain type: 0 LG, 1 HG
          int gain2   = tCell->gain2();
          // int qual1   = tCell->qual1(); // quality factor (chi2)
          // int qual2   = tCell->qual2();
          bool badch1 = tCell->badch1(); //is a bad ch? (it is in the bad channel's list?)
          bool badch2 = tCell->badch2();
          bool noch1  = (gain1<0 || gain1>1); // there is a ch?
          bool noch2  = (gain2<0 || gain2>1);
          float time1 = tCell->time1(); // reco time in both chs (OF2)
          float time2 = tCell->time2();
          float ene1  = tCell->ene1(); //reco energy in MeV (OF2) in both chs
          float ene2  = tCell->ene2();

          // verify if it's a valid ch pmt 1
          if (!noch1 && (!badch1 || !pr_noBadCells ) ){
            // ...->adc_id(CellID, pmt 0 or 1, gain1() or gain2())
            HWIdentifier adchwid_pmt1 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,0,gain1));
            IdentifierHash hashpmt1 = m_tileHWID->get_channel_hash(adchwid_pmt1); //get pmt ch hash
            indexPmt1 = (size_t) (hashpmt1);

            int adc1     = m_tileHWID->adc(adchwid_pmt1);
            int channel1 = m_tileHWID->channel(adchwid_pmt1);
            int drawer1  = m_tileHWID->drawer(adchwid_pmt1);
            int ros1     = m_tileHWID->ros(adchwid_pmt1);

            if (pr_printCellsJet){ //optional to help debugging
              ATH_MSG_INFO (" in IsTile: Cell (layer) "<< cellNum << " (" << layerCell <<") ID (ros/draw/ch/adc) " << tileCellId << " ( "<< ros1 << "/" << drawer1 << "/" << channel1 << "/" << adc1 << "). HWID/indexPmt " << adchwid_pmt1 << "/" << indexPmt1 << ". ene1 (total)" << ene1 << "(" << eneCell << "). time(final) " << time1 << "(" << timeCell <<"). gain " << gain1 );
            }
            // test if there is conflict in the map
            if (tileJetRoiDigits.test(indexPmt1)) {
              ATH_MSG_ERROR (" ##### Error Tile: Conflict index of PMT1 position in PMT map. Position was already filled in this event. Skipping the cell of Index: " << indexPmt1 << " and hwid: " << adchwid_pmt1);
              continue;
              }
            else { //If there wasn't an error, 
              tileJetRoiDigits.set(indexPmt1); //fill the map.
              caloHashMap.push_back(indexPmt1);
              cellIndexMap.push_back(cellIndex);
              channelIndexMap.push_back(cellIndex + 0.1); // adds 0.1 to the cell index, to indicate this is the PMT 1
              channelHwidInJetRoiMap.push_back(adchwid_pmt1);
              channelTimeInJetRoiMap.push_back(time1);
              channelEnergyInJetRoiMap.push_back(ene1);
              channelCaloRegionMap.push_back(caloRegionIndex);
              if (!pr_noBadCells) badChannelMap.push_back(badch1);

              // readOutIndex++;
            }
          } //end-if its a valid pmt1
          // else if (noch1)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in jet ROI has not a valid pmt1 channel!");
          // else if (badch1)  ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in jet ROI is listed as a bad pmt1 channel!");
          // else              ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in jet ROI, had its pmt1 channel skipped for no known reason.");
          
          // verify if it's a valid ch pmt 2
          if ( (!noch2 && (!badch2 || !pr_noBadCells) ) ){
            HWIdentifier adchwid_pmt2 = m_tileCabling->s2h_adc_id(m_tileID->adc_id(tileCellId,1,gain2));
            IdentifierHash hashpmt2 = m_tileHWID->get_channel_hash(adchwid_pmt2);
            indexPmt2 = (size_t) (hashpmt2);

            int adc2     = m_tileHWID->adc(adchwid_pmt2);
            int channel2 = m_tileHWID->channel(adchwid_pmt2);
            int drawer2  = m_tileHWID->drawer(adchwid_pmt2);
            int ros2     = m_tileHWID->ros(adchwid_pmt2);

            if (pr_printCellsJet){ //optional to help debugging
              ATH_MSG_INFO (" in IsTile: Cell (layer) "<< cellNum << " (" << layerCell <<") ID (ros/draw/ch/adc) " << tileCellId << " ("<< ros2 << "/" << drawer2 << "/" << channel2 << "/" << adc2 <<"). HWID/indexPmt " << adchwid_pmt2 << "/" << indexPmt2 <<  ". ene2(total)" << ene2 << "(" << eneCell << "). time(final) "  << time2 << "(" << timeCell << "). gain " << gain2 );
            }              
            // test if there is conflict in the map (only for debugging reason)
            if (tileJetRoiDigits.test(indexPmt2)) {
              ATH_MSG_ERROR (" ##### Error Tile: Conflict index of PMT2 position in PMT map. Position was already filled in this event. Skipping the cell of Index: " << indexPmt2 << " and hwid: " << adchwid_pmt2);
              continue;
              }
            else {//If there wasn't an error,
              tileJetRoiDigits.set(indexPmt2); //fill the map.
              caloHashMap.push_back(indexPmt2);
              cellIndexMap.push_back(cellIndex);
              channelIndexMap.push_back(cellIndex + 0.2); // adds 0.2 to the cell index, to indicate this is the PMT2
              channelHwidInJetRoiMap.push_back(adchwid_pmt2);
              channelTimeInJetRoiMap.push_back(time2);
              channelEnergyInJetRoiMap.push_back(ene2);
              channelCaloRegionMap.push_back(caloRegionIndex);
              if (!pr_noBadCells) badChannelMap.push_back(badch2);
              // readOutIndex++;
            }
            
          }
          if (noch1)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in jet ROI has not a valid pmt1 channel!");
          if (noch2)   ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in jet ROI has not a valid pmt2 channel!");
          if ( (badch1 && badch2) && pr_noBadCells )  {
            ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in jet ROI is listed as a bad pmt2 channel! Skipping cell.");
            continue;
            }
          if ( (badch1 && noch2) && pr_noBadCells){
            ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in jet ROI is listed as a bad pm1 and has no valid pmt2 channel! Skipping cell.");
            continue;
            }
          // else        ATH_MSG_INFO (" (tile) Cell "<< cellNum <<" in jet ROI, had its pmt2 channel skipped for no known reason.");
          // } //end if id
        } // end if isTile

        // ========= LAr ==========
        else if ((isLAr) || (isLArFwd) || (isLArHEC)) { //LAr
          chhwid = larCabling->createSignalChannelID(cellId);
          // std::string hwid_lar = chid.getString();
          // ATH_MSG_INFO (typeid(chhwid).name());
          chidHash =  m_onlineLArID->channel_Hash(chhwid);
          index = (size_t) (chidHash);
          if (larJetRoiDigits.test(index)) {
            ATH_MSG_ERROR (" ##### Error LAr: Conflict index position in Channel map. Position was already filled in this event. Skipping the cell of Index: " << index << " and hwid: " << chhwid);
            continue;
            }
          else if (badCell && pr_noBadCells){
            ATH_MSG_INFO (" (LAr) Cell "<< cellNum <<" in jet ROI is a bad LAr channel! Skipping the cell.");
            continue;
          }
          else{
            larJetRoiDigits.set(index);
            caloHashMap.push_back(index);
            cellIndexMap.push_back(cellIndex);
            channelIndexMap.push_back(cellIndex + 0.0);
            channelHwidInJetRoiMap.push_back(chhwid);
            channelTimeInJetRoiMap.push_back(timeCell);
            channelEnergyInJetRoiMap.push_back(eneCell);
            channelCaloRegionMap.push_back(caloRegionIndex);
            if (!pr_noBadCells) badChannelMap.push_back(badCell);
          }
          // readOutIndex++;
          if (pr_printCellsJet){ //optional to help debugging
            ATH_MSG_INFO (" in IsLAr: Cell (layer) "<< cellNum << " (" << layerCell <<"). HwId (B_EC/P_N/FeedTr/slot/ch) " <<  chhwid << " (" << m_onlineLArID->barrel_ec(chhwid) << "/" << m_onlineLArID->pos_neg(chhwid) << "/"<< m_onlineLArID->feedthrough(chhwid) << "/" << m_onlineLArID->slot(chhwid) << "/" << m_onlineLArID->channel(chhwid) << ") . Index: " << index << ". ene " << eneCell << ". time " << timeCell << ". D_eta: " << detaCell << ". D_phi: " << dphiCell << " ):");
          }
        } //end else LAr

        else {
          ATH_MSG_ERROR (" ####### ERROR ! No CaloCell region was found!");
          continue;
        }

        // ##############################
        //  CELL INFO DUMP (JET ROI)
        // ##############################
        j_jetRoiIndex_cellLvl->push_back(jetRoiIndex);
        j_jetRoiCellIndex->push_back(cellIndex);
        j_cellGain->push_back(gainCell);
        j_cellLayer->push_back(layerCell);
        j_cellRegion->push_back(caloRegionIndex);
        //j_cellEnergy->push_back(eneCell)
        j_cellEta->push_back(etaCell);
        j_cellPhi->push_back(phiCell);
        j_cellDEta->push_back(detaCell);
        j_cellDPhi->push_back(dphiCell);
        j_cellToJetDEta->push_back(jetRoi_deta);
        j_cellToJetDPhi->push_back(jetRoi_dphi);


        // ##############################
        cellIndex++;
        cellNum++;          
        // } // end-if verification of phi region
      } //  end-if verification of jet ROI region
    } // end loop over cells 'AllCalo' container

    // ##############################
    //  JET INFO DUMP
    // ##############################
    j_jetIndex->push_back(jetRoiIndex);
    j_jetEta->push_back(jetEta);
    j_jetPhi->push_back(jetPhi);
    j_jetPt->push_back(jetPt);

    // ##############################
    //  DIGITS CHANNEL INFO DUMP (JET)
    // ##############################
    // Loop over ALL channels in LAr Digit Container.
    // Dump only the channels inside the previous clusters

    // ========= LAr ==========
    if (larJetRoiDigits.any()){
      ATH_MSG_INFO ("(Jet ROI) Dumping LAr Digits...");
      ATH_CHECK (evtStore()->retrieve (LarDigCnt, pr_larDigName));

      for (const LArDigit* dig : *LarDigCnt) {
        HWIdentifier channelID = dig->channelID();
        IdentifierHash idHash = m_onlineLArID->channel_Hash(channelID);
        size_t index = (size_t) (idHash);

        if (larJetRoiDigits.test(index)) {
          for (unsigned int k=0 ; k < channelHwidInJetRoiMap.size() ; k++){
            if (channelHwidInJetRoiMap[k]==channelID){
              // ATH_MSG_INFO ("Hey, the channelHwidInClusterMap == adc_id " << channelHwidInClusterMap[k] << "[" << k << "]" << "==" << channelID );
              // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );
              
            
              // digits
              std::vector<short> larDigitShort = dig->samples();
              std::vector<float> larDigit( larDigitShort.begin(), larDigitShort.end() ); // get vector conversion from 'short int' to 'float'
              // if (larDigitShort != larDigit) ATH_MSG_ERROR (" ###### LAr DIGITS: Digits conversion from short to float has changed the actual digits value !!!");
              // j_larSamples->push_back(dig->samples());
              j_channelDigits->push_back(larDigit);

              // Cell / readout data
              j_channelEnergy->push_back(channelEnergyInJetRoiMap[k]);
              j_channelTime->push_back(channelTimeInJetRoiMap[k]);
              if (!pr_noBadCells) j_channelBad->push_back(badChannelMap[k]);

              // DataDescriptorElements

              // Channel info
              int barrelEc = m_onlineLArID->barrel_ec(channelID);
              int posNeg = m_onlineLArID->pos_neg(channelID);
              int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
              int slot = m_onlineLArID->slot(channelID);
              int chn = m_onlineLArID->channel(channelID);      
              std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, slot, chn } ; //unite info
              j_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

              // important indexes
              j_jetRoiChannelIndex->push_back(channelIndexMap[k]);//each LAr cell is an individual read out.    
              j_jetRoiIndex_chLvl->push_back(jetRoiIndex);  // what roi this cell belongs at the actual event: 0, 1, 2 ... 
              j_channelHashMap->push_back(index); // add the jet roi channel hash 
              
              if (pr_printCellsJet){
                ATH_MSG_INFO ("In DumpLAr Digits: ReadOutIndex "<< channelIndexMap[k]  << ". HwId (B_EC/P_N/FeedTr/slot/ch) " <<  channelID << " (" << barrelEc << "/" << posNeg << "/"<< feedThr << "/" << slot << "/" << chn << ". Dig (float): " << larDigit);
              }
            } //end-if caloHashMap matches
          } //end for loop over caloHashMaps
        } // end-if clustBits Test
      } //end loop over LAr digits container
    } //end-if larClustBits is not empty


      // ========= TileCal ==========
    if (tileJetRoiDigits.any()) {
      ATH_MSG_INFO ("(Jet ROI) Dumping Tile PMT's Digits...");
      // if(!dumpTileDigits(TileDigCnt,"TileDigitsCnt",tileClusteredDigits, tileHashMap, cellIndex, readOutIndex, clusterIndex)) ATH_MSG_DEBUG("Tile Digits information cannot be collected!");
      ATH_CHECK (evtStore()->retrieve (TileDigCnt, pr_tileDigName));
  
      for (const TileDigitsCollection* TileDigColl : *TileDigCnt){
        if (TileDigColl->empty() ) continue;

        HWIdentifier adc_id_collection = TileDigColl->front()->adc_HWID();
        // uint32_t rodBCID = TileDigColl->getRODBCID();

        for (const TileDigits* dig : *TileDigColl){
          HWIdentifier adc_id = dig->adc_HWID();

          IdentifierHash adcIdHash = m_tileHWID->get_channel_hash(adc_id);
          // IdentifierHash adcIdHash = m_tileHWID->get_hash(adc_id);
          
          size_t index = (size_t) (adcIdHash);

          if (index <= m_tileHWID->adc_hash_max()){ //safety verification
            if (tileJetRoiDigits.test(index)){ //if this channel index is inside cluster map of cells
              // Im aware this may not be the most optimized way to dump the containers info, but the solution below
              // was done to ensure that the data order will be the same for different Branches. It's different from working just with the athena, 
              // because the order here is very important to keep the data links.
              for (unsigned int k=0 ; k < channelHwidInJetRoiMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInJetRoiMap[k] == adc_id){ //find the index from pmt index to proceed.
                  // ATH_MSG_INFO ("Hey, the channelHwidInJetRoiMap == adc_id " << channelHwidInJetRoiMap[k] << "[" << k << "]" << "==" << adc_id );
                  // if (caloHashMap[k] != index) ATH_MSG_ERROR ("Hey, the caloHashMap != index " << caloHashMap[k] << "[" << k << "]" << "!=" << index );

                  Identifier cellId = m_tileCabling->h2s_cell_id(adc_id); //gets cell ID from adc
                  
                  // Digits     
                  // j_tileSamples->push_back(dig->samples()); //temporarily maintained
                  j_channelDigits->push_back(dig->samples());

                  // Channel / readout data
                  j_channelEnergy->push_back(channelEnergyInJetRoiMap[k]);
                  j_channelTime->push_back(channelTimeInJetRoiMap[k]);
                  if (!pr_noBadCells) j_channelBad->push_back(badChannelMap[k]);

                  //channelInfo
                  int ros = m_tileHWID->ros(adc_id_collection); // index of ros (1-4)
                  int drawer = m_tileHWID->drawer(adc_id_collection); //drawer for each module (0-63)
                  int partition = ros - 1; //partition (0-3)
                  int tileChNumber = m_tileHWID->channel(adc_id); // channel from hw perspective (0-48)
                  int tileAdcGain = m_tileHWID->adc(adc_id); // gain from hw perspective (0 - low, 1 - high)
                  std::vector<int> chInfo{partition, drawer, tileChNumber, tileAdcGain}; //unite info

                  j_channelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  // important indexes
                  j_jetRoiChannelIndex->push_back(channelIndexMap[k]);
                  j_jetRoiIndex_chLvl->push_back(jetRoiIndex); // what roi this ch belongs
                  j_channelHashMap->push_back(index); // add the jet roi channel hash 
                  // c_clusterChannelIndex->push_back(readOutIndex); // each LAr cell is an individual read out.   
                  // c_clusterCellIndex->push_back(cellIndex); // MUST CORRECT THIS, NOT WORKING
                  
                  if (pr_printCellsJet){ //option for debugging
                    ATH_MSG_INFO ("In DumpTile Digits:" << " ReadOutIndex: " << channelIndexMap[k] << ". ID: "<< cellId << ". adc_id (ros/draw/ch/adc) " << adc_id << " (" << ros << "/" << drawer << "/" << tileChNumber << "/" << tileAdcGain << ")" <<  ". Dig: " << dig->samples());
                  }
                } // end if-pmt index comparison
              }// end loop over pmt-indexes
            } // end if digit index exist on clusteredDigits
          } //end if hash exists
        } // end loop TileDigitsCollection
      } //end loop TileDigitsContainer
    } // end DumpTileDigits
  
    // ##############################
    //  RAW CHANNEL INFO DUMP (JET)
    // ##############################

    // ========= LAr ==========
    if (larJetRoiDigits.any()) {
      ATH_MSG_INFO ("(Jet ROI) Dumping LAr Raw Channel...");
      ATH_CHECK (evtStore()->retrieve( LArRawCnt , pr_larRawName));

      for (const LArRawChannel& LArChannel : *LArRawCnt){
        // for (const TileRawChannel* TileChannel : *TileChannelColl){

          HWIdentifier channelID = LArChannel.channelID();
          IdentifierHash chHwidHash = m_onlineLArID->channel_Hash(channelID);
          size_t index = (size_t) (chHwidHash);

          if (larJetRoiDigits.test(index)){ //if this channel index is inside 
            for (unsigned int k=0 ; k < channelHwidInJetRoiMap.size() ; k++){  //loop over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInJetRoiMap[k] == channelID){
                  // ATH_MSG_INFO ("Hey, the channelHwidInJetRoiMap == channelID " << channelHwidInJetRoiMap[k] << "[" << k << "]" << "==" << channelID );
                  
                  //RawCh info
                  int rawEnergy        = LArChannel.energy(); // energy in MeV (rounded to integer)
                  int rawTime          = LArChannel.time();    // time in ps (rounded to integer)            
                  uint16_t rawQuality  = LArChannel.quality(); // quality from pulse reconstruction
                  int provenance  = (int) LArChannel.provenance(); // its uint16_t
                  float rawEnergyConv  = (float) (rawEnergy); 
                  float rawTimeConv    = (float) (rawTime);
                  float rawQualityConv = (float) (rawQuality);

                  // Channel info
                  int barrelEc = m_onlineLArID->barrel_ec(channelID);
                  int posNeg = m_onlineLArID->pos_neg(channelID);
                  int feedThr = m_onlineLArID->feedthrough(channelID); // feedthrough - cabling interface from cold and hot side. in barrel, there are 2 feedThr for each crate.
                  int slot = m_onlineLArID->slot(channelID);
                  int chn = m_onlineLArID->channel(channelID);      
                  std::vector<int> chInfo {barrelEc, posNeg, feedThr, slot, slot, chn } ; //unite info
                  j_rawChannelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  if ( (rawEnergy != rawEnergyConv) || (rawTime != rawTimeConv) || (rawQuality != rawQualityConv) ){
                    ATH_MSG_ERROR (" ###### (Jet ROI) LAR RAW CHANNEL: Value conversion from int to float of amplitude, time or quality (uint16_t) had changed its actual value !!!");
                  }
                  j_rawChannelAmplitude->push_back(rawEnergyConv);
                  j_rawChannelTime->push_back(rawTimeConv);
                  j_rawChannelQuality->push_back(rawQualityConv);
                  j_rawChannelPedProv->push_back(provenance);

                  // important indexes
                  j_jetRoiRawChannelIndex->push_back(channelIndexMap[k]);
                  j_jetRoiIndex_rawChLvl->push_back(jetRoiIndex); // what roi this ch belongs

                  if (pr_printCellsJet){ //optional to help debugging

                    HWIdentifier hardwareID = LArChannel.hardwareID();
                    HWIdentifier identifyID = LArChannel.identify();
                    uint16_t     provenance = LArChannel.provenance();

                    ATH_MSG_INFO ("In DumpLAr Raw ("<< channelIndexMap[k] <<"): hardwareID (B_EC/P_N/feedThr/slot/chn): " << hardwareID << "(" << barrelEc << "/" << posNeg << "/" << feedThr << "/" << slot << "/" << chn << ")" << ". Energy: " << rawEnergyConv << ". Time: " << rawTimeConv << ". Provenance: " << provenance << ". Quality: " << rawQualityConv);
                  } // end-if optional cell print
                } // end-if hwid match rawChID
              } // end loop over HWIDClusterMap
          } // end-if clust.test
        // } //end loop LArRawChannelCollection
      } //end loop LArRawChannelContainer
    } // end-if clustBits have Cells


    // ========= TileCal ==========
    if (tileJetRoiDigits.any()) {
      ATH_MSG_INFO ("(Jet ROI) Dumping tile PMT's Raw Channel...");
      ATH_CHECK (evtStore()->retrieve( TileRawCnt , pr_tileRawName));

      for (const TileRawChannelCollection* TileChannelColl : *TileRawCnt){
        for (const TileRawChannel* TileChannel : *TileChannelColl){

          HWIdentifier rawAdcHwid = TileChannel->adc_HWID();
          IdentifierHash chHwidHash = m_tileHWID->get_channel_hash(rawAdcHwid);
          size_t index = (size_t) (chHwidHash);


          if (tileJetRoiDigits.test(index)){ //if this channel index is inside 
            for (unsigned int k=0 ; k < channelHwidInJetRoiMap.size() ; k++){  //loopp over the vector of hashIndex, and verify if the cell index match.
                if (channelHwidInJetRoiMap[k] == rawAdcHwid){
                  // ATH_MSG_INFO ("Hey, the channelHwidInJetRoiMap == rawAdcHwid " << channelHwidInJetRoiMap[k] << "[" << k << "]" << "==" << rawAdcHwid );
                  
                  // Channel info
                  int tileAdc         = m_tileHWID->adc(rawAdcHwid);
                  int tileCh          = m_tileHWID->channel(rawAdcHwid);
                  int drawer          = m_tileHWID->drawer(rawAdcHwid);
                  int ros             = m_tileHWID->ros(rawAdcHwid);
                  int partition       = ros - 1;
                  std::vector<int> chInfo{partition, drawer, tileCh, tileAdc}; //unite info
                  j_rawChannelChInfo->push_back(chInfo); //add to the list of cell info (both calo)

                  //RawCh info
                  float rawAmplitude  = TileChannel->amplitude(); // amplitude in ADC counts (max=1023)
                  float rawTime       = TileChannel->time();    // time relative to triggering bunch
                  // quality is a number in [0,1] obtained by integrating the parent
                  // distribution for the "goodness" from the DSP.  In hypothesis testing
                  // terms, it is the significance of the hypothisis that the input
                  // to the DSP matches the signal-model used to tune the DSP.                  
                  float rawQuality    = TileChannel->quality();// quality of the sampling distribution
                  float rawPedestal   = TileChannel->pedestal();// reconstructed pedestal value
                  j_rawChannelAmplitude->push_back(rawAmplitude);
                  j_rawChannelTime->push_back(rawTime);
                  j_rawChannelPedProv->push_back(rawPedestal);
                  j_rawChannelQuality->push_back(rawQuality);

                  // important indexes
                  j_jetRoiRawChannelIndex->push_back(channelIndexMap[k]);
                  j_jetRoiIndex_rawChLvl->push_back(jetRoiIndex); // what roi this ch belongs

                  if (pr_printCellsJet){ //optional to help debugging
                    int tileIndex, tilePmt;

                    Identifier rawCellId = TileChannel->cell_ID();
                    Identifier rawPmtId = TileChannel->pmt_ID();
                    Identifier rawAdcId = TileChannel->adc_ID();

                    TileChannel->cell_ID_index(tileIndex,tilePmt);

                    ATH_MSG_INFO ("In DumpTile Raw "<< channelIndexMap[k] <<": rawAdcHwid (ros/drawer/ch/adc): " << rawAdcHwid << "(" << ros << "/" << drawer << "/" << tileCh << "/" << tileAdc << ")" ". Index/Pmt: " << tileIndex << "/" << tilePmt << ". Amplitude: " << rawAmplitude << ". Time: " << rawTime << ". Ped: " << rawPedestal << ". Quality: " << rawQuality);
                  } // end-if optional cell print
                } // end-if hwid match rawChID
              } // end loop over HWIDClusterMap
          } // end-if clust.test
        } //end loop TileRawChannelCollection
      } //end loop TileRawChannelContainer
    } // end-if clustBits have Cells

    jetRoiIndex++;

    larJetRoiDigits.reset();
    tileJetRoiDigits.reset();

    caloHashMap.clear();
  
    channelHwidInJetRoiMap.clear();
    cellIndexMap.clear();
    channelIndexMap.clear();
    channelEnergyInJetRoiMap.clear();
    channelTimeInJetRoiMap.clear();
    channelEtaInJetRoiMap.clear();
    channelPhiInJetRoiMap.clear();
    cellGranularityEtaInJetRoiMap.clear();
    cellGranularityPhiInJetRoiMap.clear();
    channelCaloRegionMap.clear();
    if (!pr_noBadCells) badChannelMap.clear();
    // cellClusDeltaEta.clear();
    // cellClusDeltaPhi.clear();
  } //end loop over jets in container
  return StatusCode::SUCCESS;
}

void EventReaderAlg::bookBranches(TTree *tree){
  // ## Event info
  tree->Branch ("RunNumber", &e_runNumber);
  tree->Branch ("EventNumber", &e_eventNumber);
  tree->Branch ("BCID", &e_bcid);
  tree->Branch ("avg_mu", &e_pileup);
  // #############

  // ## Cluster 
  tree->Branch ("cluster_index",&c_clusterIndex);
  tree->Branch ("cluster_et",&c_clusterEnergy);
  tree->Branch ("cluster_pt",&c_clusterPt);
  tree->Branch ("cluster_eta",&c_clusterEta);
  tree->Branch ("cluster_phi",&c_clusterPhi);
  tree->Branch ("cluster_eta_calc",&c_clusterEta_calc);
  tree->Branch ("cluster_phi_calc",&c_clusterPhi_calc);
  // Cluster cell
  tree->Branch ("cluster_index_cellLvl",&c_clusterIndex_cellLvl);
  tree->Branch ("cluster_cell_index",&c_clusterCellIndex);
  tree->Branch ("cluster_cell_caloGain",&c_cellGain);
  tree->Branch ("cluster_cell_layer",&c_cellLayer);
  tree->Branch ("cluster_cell_region",&c_cellRegion);
  // tree->Branch ("cluster_cell_energy",&c_cellEnergy);
  tree->Branch ("cluster_cell_eta",&c_cellEta);
  tree->Branch ("cluster_cell_phi",&c_cellPhi);
  tree->Branch ("cluster_cell_deta",&c_cellDEta);
  tree->Branch ("cluster_cell_dphi",&c_cellDPhi);
  tree->Branch ("cluster_cellsDist_dphi",&c_cellToClusterDPhi);
  tree->Branch ("cluster_cellsDist_deta",&c_cellToClusterDEta);

  // Cluster channel (digits and cell ch)
  tree->Branch ("cluster_index_chLvl",&c_clusterIndex_chLvl);
  tree->Branch ("cluster_channel_index",&c_clusterChannelIndex);
  tree->Branch ("cluster_channel_digits",&c_channelDigits);
  tree->Branch ("cluster_channel_energy",&c_channelEnergy);
  tree->Branch ("cluster_channel_time",&c_channelTime);
  if (!pr_noBadCells) tree->Branch ("cluster_channel_bad",&c_channelBad);
  tree->Branch ("cluster_channel_chInfo", &c_channelChInfo);
  tree->Branch ("cluster_channel_hash",&c_channelHashMap); 
  // Cluster raw channel
  tree->Branch ("cluster_index_rawChLvl",&c_clusterIndex_rawChLvl);
  tree->Branch ("cluster_rawChannel_index",&c_clusterRawChannelIndex);
  tree->Branch ("cluster_rawChannel_amplitude",&c_rawChannelAmplitude);
  tree->Branch ("cluster_rawChannel_time",&c_rawChannelTime);
  tree->Branch ("cluster_rawChannel_pedProv",&c_rawChannelPedProv);
  tree->Branch ("cluster_rawChannel_qual",&c_rawChannelQuality);  
  tree->Branch ("cluster_rawChannel_chInfo",&c_rawChannelChInfo); // tree->Branch ("cluster_cell_caloGain",&c_cellGain); //modify


  // to be removed....
  // tree->Branch ("cluster_cell_larSamples",&c_larSamples);
  // tree->Branch ("cluster_cell_tileSamples",&c_tileSamples);
  // #############

  // ## Jets
  tree->Branch ("jet_index",&j_jetIndex);
  tree->Branch ("jet_pt",&j_jetPt);
  tree->Branch ("jet_eta",&j_jetEta);
  tree->Branch ("jet_phi",&j_jetPhi);
  tree->Branch ("jet_roi_r",&j_roi_r);
  // Cells
  tree->Branch ("jet_index_cellLvl",&j_jetRoiIndex_cellLvl);
  tree->Branch ("jet_cell_index",&j_jetRoiCellIndex);
  tree->Branch ("jet_cell_caloGain",&j_cellGain);
  tree->Branch ("jet_cell_layer",&j_cellLayer);
  tree->Branch ("jet_cell_region",&j_cellRegion);
  // tree->Branch ("jet_cell_energy",&j_cellEnergy);
  tree->Branch ("jet_cell_eta",&j_cellEta);
  tree->Branch ("jet_cell_phi",&j_cellPhi);
  tree->Branch ("jet_cell_deta",&j_cellDEta);
  tree->Branch ("jet_cell_dphi",&j_cellDPhi);
  tree->Branch ("jet_cell_distJetDPhi",&j_cellToJetDPhi);
  tree->Branch ("jet_cell_distJetDEta",&j_cellToJetDEta);  
  // Channel
  tree->Branch ("jet_index_chLvl",&j_jetRoiIndex_chLvl);
  tree->Branch ("jet_channel_index",&j_jetRoiChannelIndex);
  tree->Branch ("jet_channel_chInfo",&j_channelChInfo);
  tree->Branch ("jet_channel_digits",&j_channelDigits);
  // tree->Branch ("jet_channel_larDigits",&j_larSamples); // xxxxxxxxxxxxxxxxx excluir
  // tree->Branch ("jet_channel_tileDigits",&j_tileSamples); //xxxxxxxxxxxxxxxxx excluir
  tree->Branch ("jet_channel_energy",&j_channelEnergy);
  tree->Branch ("jet_channel_time",&j_channelTime);
  if (!pr_noBadCells) tree->Branch ("jet_channel_bad",&j_channelBad);
  tree->Branch ("jet_channel_hash",&j_channelHashMap); 
  // Raw Channel
  tree->Branch ("jet_rawChannel_chInfo",&j_rawChannelChInfo);
  tree->Branch ("jet_rawChannel_amplitude",&j_rawChannelAmplitude);
  tree->Branch ("jet_rawChannel_time",&j_rawChannelTime);
  tree->Branch ("jet_rawChannel_pedProv",&j_rawChannelPedProv);
  tree->Branch ("jet_rawChannel_qual",&j_rawChannelQuality);
  tree->Branch ("jet_rawChannel_index",&j_jetRoiRawChannelIndex);
  tree->Branch ("jet_index_rawChLvl",&j_jetRoiIndex_rawChLvl);

  // ## Particle Truth ##
  if (pr_isMC){
    tree->Branch("mc_part_energy",&mc_part_energy);
    tree->Branch("mc_part_pt",&mc_part_pt);
    tree->Branch("mc_part_m",&mc_part_m);
    tree->Branch("mc_part_eta",&mc_part_eta);
    tree->Branch("mc_part_phi",&mc_part_phi);
    tree->Branch("mc_part_pdgId",&mc_part_pdgId);
    tree->Branch("mc_part_status", &mc_part_status);
    tree->Branch("mc_part_barcode", &mc_part_barcode);

    // ## Vertex Truth ##
    tree->Branch("mc_vert_x", &mc_vert_x);
    tree->Branch("mc_vert_y", &mc_vert_y);
    tree->Branch("mc_vert_z", &mc_vert_z);
    tree->Branch("mc_vert_time", &mc_vert_time);
    tree->Branch("mc_vert_perp", &mc_vert_perp);
    tree->Branch("mc_vert_eta", &mc_vert_eta);
    tree->Branch("mc_vert_phi", &mc_vert_phi);
    tree->Branch("mc_vert_barcode", &mc_vert_barcode);
    tree->Branch("mc_vert_id", &mc_vert_id);
  }
  
  // ## Photons ##
  tree->Branch("ph_energy",&ph_energy);
  tree->Branch("ph_pt",&ph_pt);  
  tree->Branch("ph_eta",&ph_eta);
  tree->Branch("ph_phi",&ph_phi);
  tree->Branch("ph_m",&ph_m);
  

  
}

void EventReaderAlg::clear(){
  // ## Event info
  e_runNumber   = 9999;
  e_eventNumber = 9999;
  e_bcid        = 9999;  
  e_pileup      = -999;
  // ############

  // ## Cluster 
  c_clusterIndex->clear();
  c_clusterEnergy->clear();
  c_clusterPt->clear();
  c_clusterEta->clear();
  c_clusterPhi->clear();
  c_clusterEta_calc->clear();
  c_clusterPhi_calc->clear();
  c_clusterIndex->clear();
  // Cluster cell
  c_clusterIndex_cellLvl->clear();
  c_clusterCellIndex->clear();
  c_cellGain->clear(); 
  c_cellLayer->clear();
  c_cellRegion->clear();
  //c_cellEnergy->clear();
  c_cellEta->clear();
  c_cellPhi->clear();
  c_cellDEta->clear();
  c_cellDPhi->clear();
  c_cellToClusterDPhi->clear();
  c_cellToClusterDEta->clear();  
   
  // Cluster channel
  c_clusterIndex_chLvl->clear();
  c_clusterChannelIndex->clear();
  c_channelDigits->clear();
  c_channelEnergy->clear();
  c_channelTime->clear();
  if (!pr_noBadCells) c_channelBad->clear();
  c_channelChInfo->clear();
  // Cluster raw channel
  c_rawChannelChInfo->clear();
  c_rawChannelAmplitude->clear();
  c_rawChannelTime->clear();
  c_rawChannelPedProv->clear();
  c_rawChannelQuality->clear();
  c_clusterRawChannelIndex->clear();
  c_clusterIndex_rawChLvl->clear();
  // c_cellGain->clear(); //modify
  c_channelHashMap->clear(); // 
  // to be removed....
  // c_larSamples->clear();
  // c_tileSamples->clear();
  // ############

  // ## Jet
  j_jetIndex->clear();
  j_jetPt->clear();
  j_jetEta->clear();
  j_jetPhi->clear();
  j_roi_r = pr_roiRadius;  
  // Cell
  j_jetRoiIndex_cellLvl->clear();
  j_jetRoiCellIndex->clear();
  j_cellGain->clear();
  j_cellLayer->clear();
  j_cellRegion->clear();
  //j_cellEnergy->clear();
  j_cellEta->clear();
  j_cellPhi->clear();
  j_cellDEta->clear();
  j_cellDPhi->clear();
  j_cellToJetDPhi->clear();
  j_cellToJetDEta->clear();
  // Channel
  j_jetRoiIndex_chLvl->clear(); // jet ROI index, for each channel index. (they have the same number of inputs)
  j_jetRoiChannelIndex->clear(); // index for a sequence of channels. It is a float for identify PMTs: +0.0 LAr, +0.1 Tile PMT1, +0.2 Tile PMT2
  j_channelChInfo->clear(); // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel) and Tile (ros, drawer, channel, adc)
  j_channelDigits->clear();  // samples from LAr and Tile cells/channels
  // j_larSamples->clear(); //xxxxxxxxxxx excluir
  // j_tileSamples->clear(); //xxxxxxxxxxx excluir
  j_channelEnergy->clear(); // energy of cell or readout channel inside jet ROI (MeV)
  j_channelTime->clear(); // time of channel inside cluster
  j_channelHashMap->clear(); // 
  if (!pr_noBadCells) j_channelBad->clear(); // channel linked to a cluster, whitch is tagged as bad. (1 - bad, 0 - not bad)
  // Raw channel
  j_rawChannelChInfo->clear(); // from both LAr (barrel_ec, pos_neg, feedthrough, slot, channel, provenance) and Tile (ros, drawer, channel, adc, pedestal)
  j_rawChannelAmplitude->clear(); // raw channel energy (adc)
  j_rawChannelTime->clear(); // raw channel time
  j_rawChannelPedProv->clear(); // raw channel estimated pedestal (Tile) or LAr provenance (??)
  j_rawChannelQuality->clear(); // raw channel quality
  j_jetRoiRawChannelIndex->clear(); // raw channel index
  j_jetRoiIndex_rawChLvl->clear(); // jet index at raw channel level

  // ## Particle Truth ##
  if (pr_isMC){
  // ATH_MSG_INFO("Particle Truth");
  mc_part_energy->clear();
  mc_part_pt->clear();
  mc_part_m->clear();
  mc_part_eta->clear();
  mc_part_phi->clear();
  mc_part_pdgId->clear();
  mc_part_status->clear();
  mc_part_barcode->clear();

  // ## Vertex Truth ##
  mc_vert_x->clear();
  mc_vert_y->clear();
  mc_vert_z->clear();
  mc_vert_time->clear();
  mc_vert_perp->clear();
  mc_vert_eta->clear();
  mc_vert_phi->clear();
  mc_vert_barcode->clear();
  mc_vert_id->clear();
}
  // ## Photons ##
  // ATH_MSG_INFO("Photons");
  ph_energy->clear();
  ph_eta->clear();
  ph_phi->clear();
  ph_pt->clear();
  ph_m->clear();
}

StatusCode EventReaderAlg::finalize() {
  ATH_MSG_INFO ("Finalizing " << name() << "...");
  return StatusCode::SUCCESS;
}

// athena.py EventReader_jobOptions.py > reader.log 2>&1; code reader.log
// athena.py EventReader_jobOptions.py > reader_test.log 2>&1; code reader_test.log
// cd ../build/;make -j;source x86*/setup*;cd ../run/
// cp ../source/EventReader/share/EventReader_jobOptions.py ../run/EventReader_jobOptions.py
// /usr/bin/python3 /eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/source/EventReader/share/validPlots.py
// /usr/bin/python3 /eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/source/EventReader/share/saveCaloDict.py