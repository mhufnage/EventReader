###############################################################
#
# Job options file
#
#==============================================================

#--------------------------------------------------------------
# ATLAS default Application Configuration options
#--------------------------------------------------------------
import AthenaCommon.AtlasUnixStandardJob

#--------------------------------------------------------------
# Private Application Configuration options
#--------------------------------------------------------------
isMC = False
# doMC = True
# dumpCells = False
# doPhoton = False
from AthenaCommon.AppMgr import ToolSvc
from AthenaCommon.AthenaCommonFlags import jobproperties as jps

# Full job is a list of algorithms
from AthenaCommon.AlgSequence import AlgSequence
job = AlgSequence()

from EventReader.EventReaderConf import EventReaderAlg

#### Geometry relation flags
from AthenaCommon.GlobalFlags import jobproperties
from AthenaCommon.DetFlags import DetFlags
from AthenaCommon.GlobalFlags import globalflags

Geometry = "ATLAS-R2-2016-01-00-01"
globalflags.DetGeo.set_Value_and_Lock('atlas')
#### ! If data is MC, comment the line below! #####
if not(isMC):
    globalflags.DataSource.set_Value_and_Lock('data') #
###################################################
DetFlags.detdescr.all_setOn()
DetFlags.Forward_setOff()
DetFlags.ID_setOff()

jobproperties.Global.DetDescrVersion = Geometry

    # We need the following two here to properly have
    # Geometry
from AtlasGeoModel import GeoModelInit
from AtlasGeoModel import SetGeometryVersion
include("CaloDetMgrDetDescrCnv/CaloDetMgrDetDescrCnv_joboptions.py")
include("LArDetDescr/LArDetDescr_joboptions.py")
#####

# Cabling map acess (LAr)
from LArCabling.LArCablingAccess import LArOnOffIdMapping
LArOnOffIdMapping()

job += EventReaderAlg( "EventReader" )

job.EventReader.clusterName     = "CaloCalTopoClusters"
job.EventReader.jetName         = "AntiKt4EMPFlowJets"
job.EventReader.roiRadius       = 0.2
job.EventReader.tileDigName     = "TileDigitsCnt"
if isMC:
    job.EventReader.larDigName      = "LArDigitContainer_MC"
else:
    job.EventReader.larDigName      = 'FREE'
job.EventReader.tileRawName     = "TileRawChannelCnt"
job.EventReader.larRawName      = "LArRawChannels"
job.EventReader.doTile          = True
job.EventReader.noBadCells      = True
job.EventReader.doLAr           = True
job.EventReader.printCellsClus  = True
job.EventReader.printCellsJet   = False
job.EventReader.testCode        = False
# job.EventReader.OuputLevel = INFO
job.EventReader.isMC            = True  # set to True in case of MC sample.

# if job.EventReader.isMC==False:
#     globalflags.DataSource.set_Value_and_Lock('data') 

from AthenaCommon.AppMgr import ServiceMgr
import AthenaPoolCnvSvc.ReadAthenaPool

testFile =  '/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_evenMoreEvents/ESD_pi0_000001.pool.root'
# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD/ESD_pi0.pool.root"
# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_moreEvents/ESD_pi0.pool.root"
# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_AMItags/ESD_pi0.pool.root" #AMI TAG reco
# testFile = "/eos/user/m/mhufnage/scripts_lxplus/Reco/ALP_reco/reco_HITStoESD_AMIdumpCells/ESD_pi0.pool.root"
ServiceMgr.EventSelector.InputCollections = [ testFile ]
ServiceMgr += CfgMgr.THistSvc()

# Create output file
hsvc = ServiceMgr.THistSvc
# jps.AthenaCommonFlags.HistOutputs = ["MYSTREAM:myfile.root"]
hsvc.Output += [ "rec DATAFILE='ntuple_test.root' OPT='RECREATE'" ]
# hsvc.Output += [ "recHist DATAFILE='hist.root' OPT='RECREATE'" ]
theApp.EvtMax = -1

MessageSvc.defaultLimit = 9999999  # all messages