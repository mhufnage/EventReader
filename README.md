# **EventReader Dumper package**

__

## **1. Reconstruction from HITS to ESD (MC)**
- 1.1. Simplest reconstruction:

    ```
    cd ../run
    mkdir recoLxplus
    cd recoLxplus
    setupATLAS
    asetup Athena,22.0.44
    Reco_tf.py --athenaopts=' --threads=1 --imf --perfmon' --AddCaloDigi True --maxEvents -1 --outputRDOFile=RDO_pi0_$i.pool.root --postInclude=jobHITStoRDOOptions.py --inputHitsFile=/eos/user/m/mhufnage/ALP_project/HITS/mc16_13TeV.428000.single_pi0/HITS.17102085._000002.pool.root.1 >Reco_tf_HITStoRDO.log 2>&1
    ```

    * And for multiple files, directly in LXPLUS:

        ```
        cd recoLxplus
        cp ../../source/EventReader/source/reco_HITStoESD_simplest.sh .
        # <edit it>
        bash reco_HITStoESD_simplest.sh
        ```

## **2. Bytestream data to ESD (Data)**

- 2.1. Download run data

    Dataset previous usage in [ATLAS e/gamma workshop 2017](https://indico.cern.ch/event/649891/timetable/), in [this presentation](https://indico.cern.ch/event/649891/contributions/2750915/attachments/1554469/2444022/2017_11_08-2.pdf). 

    * Runs 329542 and 331020: 
        * [Twiki link for 2017 reprocessing](https://twiki.cern.ch/twiki/bin/view/Atlas/Special2017Reprocessing)
        * [Good runs list with low pile-up 2017 (GRL)](https://atlas-tagservices.cern.ch/tagservices/RunBrowser/runBrowserReport/COMA_GRL.php?GRLAction=GRLReport&gpath=grlgen/Minbias_2017&gfile=data17_13TeV.periodAllYear_DetStatus-v96-pro21-11_Unknown_PHYS_StandardModel_MinimuBias2010.xml)

    * Basic Rucio commands:
        ```
        rucio list-dids data17_13TeV:data17_13TeV.00329542.physics_MinBias.*
        rucio list-dataset-replicas data17_13TeV:data17_13TeV.00329542.physics_MinBias.daq.RAW
        rucio download data17_13TeV:data17_13TeV.00329542.physics_MinBias.daq.RAW --nrandom=1
        rucio list-files data17_13TeV:data17_13TeV.00329542.physics_MinBias.daq.RAW
        ```
    * If the data is in tape, request via [Rucio-UI](https://rucio-ui.cern.ch/r2d2/request).
        ```
        rucio list-dataset-replicas user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW_der1655905167
        +-----------------------+---------+---------+
        | RSE                   |   FOUND |   TOTAL |
        |-----------------------+---------+---------|
        | CERN-PROD_SCRATCHDISK |       0 |      10 |
        +-----------------------+---------+---------+
        rucio list-files user.mhufnage.data17_13TeV.00329542.physics_MinBias.daq.RAW_der1655905167
        ```
    

<!-- - 2.1. Download DRAW_ZEE
   

    ```
    rucio list-dataset-replicas "data12_8TeV:data12_8TeV.00200913.physics_Egamma.merge.DRAW_ZEE.f437_m716"
        DATASET: data12_8TeV:data12_8TeV.00200913.physics_Egamma.merge.DRAW_ZEE.f437_m716
        +-----------------+---------+---------+
        | RSE             |   FOUND |   TOTAL |
        |-----------------+---------+---------|
        | CERN-PROD_TZERO |       2 |       2 |
        +-----------------+---------+---------+
    ```
    The dataset **data12_8TeV.00200913.physics_Egamma.merge.DRAW_ZEE.f431_m716** is stored on tape.
    ```    
    rucio list-dids "data12_8TeV:*200913*" --filter type=dataset,datatype=DRAW_ZEE
    ``` -->
## **3. Configuring the Dumper**
    ```
    cd ../build/
    setupATLAS
    asetup Athena,22.0.44
    make -j2
    source x86*/setup*
    cd ../run/
    athena.py EventReader_jobOptions.py > reader.log 2>&1; code reader.log
    ```


## **4. Python scripts (EventReader/share/)**
- 4.1. Validation plots (validPlots.py)
    ```
    python validPlots.py --help
        usage: validPlots.py [-h] [--fileAlias FILEALIAS] [--getNoise GETNOISE]
                            [--events EVENTS] [--outputFilesPath OUTPUTFILESPATH]
                            [--inputFilesPath INPUTFILESPATH]

        Script to read the dumped file from EventReader, and generate the validation
        plots as TH1F in *.root files.

        optional arguments:
        -h, --help            show this help message and exit
        --fileAlias FILEALIAS
                                name alias to generated files.
        --getNoise GETNOISE   get the histogram and dict of noisy cells around the
                                Jet ROI.
        --events EVENTS       amount of events to be processed. Default is all the
                                events pointed by the file directory.
        --outputFilesPath OUTPUTFILESPATH
                                path for the output *.root files. It may create
                                folders in this dir.
        --inputFilesPath INPUTFILESPATH
                                path for the file(s). It accepts the glob (*)
                                notation.
    ```
    - There are two main commands:
        * Get Noise parameters per layer.
            ```
            python validPlots.py \
            --getNoise=True \
            --inputFilesPath='/path/to/data/ESD_pi0_*.root' \ 
            --outputFilesPath='/path/to/output/run/singlePi0_50kEvts'
            ```
            * The _--getNoise_ parse will override the general plots generation, and will only loop over the cells most distant in the JetRoi of the dumped data, saving the _CaloCell Energy_ and the _RawChannel Energy_ in an output **'noiseCellJetROI.root'**, and their _mean_ and $\sigma$ in a dictionary **caloNoiseDict.json**.
            * All of these files will be saved in the output directory, parsed _outputFilesPath_.
            * _--events_ is equal to -1 by default (all events)

        * Generate the histograms for the dumped data with cuts (based on the noise dictionary generated earlier).
            * This script will load the generated file with the layer noise information, then produce all the plots with their own version with the cut at $e_{cut} = 2 *\sigma$ (default). A log file will be created for each script running, appending the outputs at the same file ****
            * The _--fileAlias_ parse is just an alias added at the beginning of the generated files and the LOG.
            * 
            ```
            python validPlots.py --inputFilesPath='/path/to/data/ESD_*.root' \ 
            --outputFilesPath = '/path/to/output/run/singlePi0_50kEvts' \
            --fileAlias='testSinglePi0_' \
            --events=-1

            
            python validPlots.py --getNoise=True --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root'  --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/testDigits'  --fileAlias='testSinglePi0_' --events=100

            python validPlots.py  --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root'  --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/testDigits'  --fileAlias='testSinglePi0_' --events=100
            ```
    - Examples for batch tests:
        ```
        python validPlots.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts' --events=10000 --fileAlias='3CellPerBin_' --nCellPerBin=3
       
        python validPlots.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts' --events=1 --fileAlias='test_'
        ```

- 4.2. Reading histograms (readHisto.py)
    ```
    python readHisto.py --help
    usage: readHisto.py [-h] [--fileAlias FILEALIAS]
                        [--outputFilesPath OUTPUTFILESPATH]
                        [--inputFilesPath INPUTFILESPATH]

    Script to read the histograms generated by validPlots.py, in *.root files.

    optional arguments:
    -h, --help            show this help message and exit
    --fileAlias FILEALIAS
                            name alias to generated *.root files. (
                            <fileAlias>jetAnalysis.root )
    --outputFilesPath OUTPUTFILESPATH
                            path for the automatic generated output folders with
                            histograms. If the folder doesnt exist, it will create
                            it.
    --inputFilesPath INPUTFILESPATH
                            path for the histogram file(s).
    ```
    * Exemple:
    ```
    python readHisto.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts/histograms/' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts/plots/1CellPerBin/' --fileAlias='1CellPerBin_'
    ```

- 4.3. Batch validation plots and histograms (validPlotsBatch.sh)
     ```
    cd EventReader/share/
    # <update it with the desired commands>
    bash validPlotsBatch.sh
    ```


## **5. Sparse checkout Athena packages**
```
cd
cd private
git atlas init-workdir https://gitlab.cern.ch/atlas/athena.git
cd athena
git branch
# * master
git fetch upstream
git rebase upstream/master # atualiza Master local com ATHENA MASTER
git push origin master # atualiza o fork com o ATHENA MASTER

git checkout -b jetAddPtThresMin upstream/22.0 --no-track # cria nova branch no ATHENA 22.0 (sem atualizacao automatica)
git atlas addpkg JetRec

cmake ../athena/Projects/WorkDir/
source x86_64-centos7-gcc11-opt/setup.sh

```

<!-- TileCal readout system
element     range       meaning
-------     -----       -------

ros         1 to 4      ReadOutSystem number ( 1,2 = pos/neg Barrel (side A/C)
                                                3,4 = pos/neg Ext.Barrel (side A/C) )
drawer      0 to 63     64 drawers (modules) in one cylinder (phi-slices)
channel     0 to 47     channel number in the drawer
adc         0 to 1      ADC number for the channel (0 = low gain, 1 = high gain)


Possible Gain values
    enum CaloGain {
        TILELOWLOW =-16 ,
        TILELOWHIGH =-15 ,
        TILEHIGHLOW  = -12,
        TILEHIGHHIGH = -11,
        TILEONELOW   =-4,
        TILEONEHIGH  =-3,
        INVALIDGAIN = -1, 
        LARHIGHGAIN = 0, 
        LARMEDIUMGAIN = 1,  
        LARLOWGAIN = 2,
        LARNGAIN =3,
        UNKNOWNGAIN=4}; -->