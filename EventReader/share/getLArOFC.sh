#!/bin/sh
# Get the LAr OFC to NTuple 
# https://acode-browser1.usatlas.bnl.gov/lxr/source/athena/LArCalorimeter/LArCalibTools/share/LArCommConditions2Ntuple.py

# setupATLAS
# asetup Athena,22.0.70

# athena.py -c 'RootFile="LArOFC.root";Objects=["OFC"];SuperCells=False;IsMC=False;IsFlat=True' LArCalibTools/LArCommConditions2Ntuple.py ## OK
athena.py -c 'RootFile="LArOFC.root";Objects=["OFC", "PEDESTAL"];SuperCells=False;IsMC=False;IsFlat=True' LArCalibTools/LArCommConditions2Ntuple.py ## OK
# athena.py -c 'RootFile="LArOFC_noFlat.root";Objects=["PHYSAUTOCORR"];SuperCells=False;IsMC=False;IsFlat=False' LArCalibTools/LArCommConditions2Ntuple.py ## ERROR

