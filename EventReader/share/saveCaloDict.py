#   Mateus Hufnagel 19/04/2022
#   This scripts adds the Delta_Eta and Delta_Phi (granularity) of the cells selected by caloSampling layer, as unique lists for each layer.
#   The values are exact, so may differ a little from the helpers commented in Athena.

import ROOT
import numpy as np
from ROOT import TH1F
import json
from glob import glob
from functionsHelper import listToNumpyArray, isInsideLArCrackRegion, loadJsonFile, saveAsJsonFile, getCentralCellIndexJetROI_deltaR, save_pickle, load_pickle #custom
# import pandas as pd

ROOT.gROOT.SetBatch(True)

bProduce    = False
bProcess    = True

# cernbox = '/home/mhufnagel/cernbox/' # local
workdir     = '/eos/user/m/mhufnage/'
recoFolder  = workdir + 'scripts_lxplus/Reco/'
workFolder  = workdir + 'scripts_lxplus/Grid/workAthena/ALPPackages/'
inputFileName  = glob(workFolder+'run/ntuples/ESD_pi0_*.pool.root') #lxplus
outputDir = workdir+'scripts_lxplus/Grid/workAthena/ALPPackages/run/' #lxplus
fDictName = 'dictCalo.json'
# inputFileName = '/home/mhufnagel/cernbox/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuple.root' #local


sTree = ROOT.TChain("data",'')
for file in inputFileName:  
    sTree.Add(file+"/data")

# sFile = ROOT.TFile(inputFileName,'READ')
# sTree = sFile.Get('data')
nEvts = sTree.GetEntries()

dictCalo = {'Layer':['PreSamplerB', #ok
                    'EMB1', #ok
                    'EMB2', #ok
                    'EMB3', #ok
                    'PreSamplerE', #ok
                    'EME1', #ok
                    'EME2', #ok
                    'EME3', #ok
                    'HEC0',
                    'HEC1',
                    'HEC2',
                    'HEC3',
                    'Tile_LB_A', #ok
                    'Tile_LB_BC', #ok
                    'Tile_LB_D', #ok
                    'Tile_Gap1_C10', #ok
                    'Tile_Gap2_D4', #ok
                    'Tile_Gap3_E', #ok
                    'Tile_EB_A', #ok
                    'Tile_EB_BC', #ok
                    'Tile_EB_D', #ok
                    'FCAL0',
                    'FCAL1',
                    'FCAL2'],
        'granularityEtaLayerPrecise': [[0.02500000037252903]   ,   [0.0031250000465661287 , 0.02500000037252903]     , [0.02500000037252903, 0.07500000298023224],   [0.05000000074505806], # EMB (PSB, EMB1, EMB2, EMB3)
                        [0.02500000037252903], [ 0.0031250000465661287, 0.004166666883975267, 0.0062500000931322575, 0.02500000037252903, 0.02500000037252903, 0.05000000074505806, 0.10000000149011612], [0.02500000037252903, 0.05000000074505806, 0.10000000149011612], [0.05000000074505806], # EMEC (PSE, EME1, EME2, EME3)
                        [0.10000000149011612, 0.20000000298023224], [0.10000000149011612, 0.20000000298023224], [0.10000000149011612, 0.20000000298023224], [0.10000000149011612, 0.20000000298023224], # HEC0, HEC1, HEC2, HEC3 
                        [0.10000000149011612], [0.10000000149011612], [0.20000000298023224], [0.10000000149011612], [0.10000000149011612], [0.20000000298023224], [0.10000000149011612], [0.10000000149011612], [0.20000000298023224], # Tile (LBA, LBBC, LBD, GAP1, GAP2, GAP3*, EBA, EBBC, EBD ) *Gap3 has 0.1 and 0.2 granularity
                        [0.2], [0.2], [0.2] ], # FCAL0, FCAL1, FCAL2 ** ( approximately 0.2) ** not considering the precise values of the FCAL
        'granularityPhiLayerPrecise':  [[0.09817477315664291]   ,   [0.09817477315664291 , 0.02454369328916073]     , [0.02454369328916073, 0.02454369328916073],   [0.02454369328916073],  # EMB (PSB, EMB1, EMB2, EMB3)
                        [0.09817477315664291], [ 0.09817477315664291, 0.09817477315664291,0.09817477315664291,0.09817477315664291,0.09817477315664291,0.09817477315664291,0.09817477315664291,], [0.02454369328916073, 0.02454369328916073, 0.09817477315664291], [0.02454369328916073],  # EMEC (PSE, EME1, EME2, EME3)
                        [0.09817477315664291, 0.19634954631328583], [0.09817477315664291, 0.19634954631328583], [0.09817477315664291, 0.19634954631328583], [0.09817477315664291, 0.19634954631328583], # HEC0, HEC1, HEC2, HEC3 
                        [0.09817477315664291], [0.09817477315664291], [0.09817477315664291], [0.09817477315664291], [0.09817477315664291], [0.09817477315664291], [0.09817477315664291], [0.09817477315664291], [0.09817477315664291], # Tile (LBA, LBBC, LBD, GAP1, GAP2, GAP3*, EBA, EBBC, EBD ) *Gap3 has 0.1 and 0.2 granularity
                        [0.2], [0.2], [0.2] ], # FCAL0, FCAL1, FCAL2 ** ( pi/16) ** not considering the precise values of the FCAL
        'granularityEtaLayer':     [[0.025]   ,   [0.025/8 , 0.025]     , [0.025, 0.075],   [0.050],            # EMB (PSB, EMB1, EMB2, EMB3)
                        [0.025], [ 0.025/8, 0.025/6,  0.025/4, 0.025, 0.025, 0.05, 0.1], [0.025, 0.05, 0.1], [0.05], # EMEC (PSE, EME1, EME2, EME3)
                        [0.1, 0.2], [0.1, 0.2], [0.1, 0.2], [0.1, 0.2],                             # HEC0, HEC1, HEC2, HEC3 
                        [0.1], [0.1], [0.2], [0.1], [0.1], [0.2], [0.1], [0.1], [0.2],              # Tile (LBA, LBBC, LBD, GAP1, GAP2, GAP3*, EBA, EBBC, EBD ) *Gap3 has 0.1 and 0.2 granularity
                        [0.2], [0.2], [0.2] ],                                                      # FCAL0, FCAL1, FCAL2 ** ( approximately 0.2)
        'granularityPhiLayer':     [[0.1]   ,   [0.1 , 0.025]     , [0.025, 0.025],   [0.025],            # EMB (PSB, EMB1, EMB2, EMB3)
                        [0.1], [ 0.1, 0.1, 0.1, 0.1, 0.1, 0.1, 0.1], [0.025, 0.025, 0.1], [0.025], # EMEC (PSE, EME1, EME2, EME3)
                        [0.1, 0.2], [0.1, 0.2], [0.1, 0.2], [0.1, 0.2],                             # HEC0, HEC1, HEC2, HEC3 
                        [0.1], [0.1], [0.1], [0.1], [0.1], [0.1], [0.1], [0.1], [0.1],              # Tile (LBA, LBBC, LBD, GAP1, GAP2, GAP3*, EBA, EBBC, EBD ) *Gap3 has 0.1 and 0.2 granularity
                        [0.2], [0.2], [0.2] ],                                                      # FCAL0, FCAL1, FCAL2 ** ( pi/16)
        # store only the positive eta values.
        'etaLowLim':    [[0]   ,   [0, 1.4]     , [0, 1.4],   [0],                                   # EMB (PSB, EMB1, EMB2, EMB3)
                        [1.5], [ 1.5, 1.8, 2.0, 1.425, 2.4, 1.375, 2.5], [1.425, 1.375, 2.5], [1.5], # EMEC (PSE, EME1, EME2, EME3)
                        [1.5, 2.5], [1.5, 2.5], [1.5, 2.5], [1.5, 2.5],                             # HEC0, HEC1, HEC2, HEC3 
                        [0], [0], [0], [0.8], [0.8], [0.8], [0.8], [0.8], [0.8],                    # Tile (LBA, LBBC, LBD, GAP1, GAP2, GAP3, EBA, EBBC, EBD )
                        [3.1], [3.1], [3.1] ],                                                      # FCAL0, FCAL1, FCAL2
        'etaHighLim':   [[1.52],   [1.4, 1.475], [1.4, 1.475], [1.35],                                  # EMB (PSB, EMB1, EMB2, EMB3)
                        [1.8], [1.8, 2.0, 2.4, 1.5, 2.5, 1.425, 3.2], [2.5, 1.425, 3.2], [2.5],    # EMEC (PSE, EME1, EME2, EME3)
                        [2.5, 3.2], [2.5, 3.2], [2.5, 3.2], [2.5, 3.2],                            # HEC0, HEC1, HEC2, HEC3 
                        [1.0], [1.0], [1.0], [1.6], [1.6], [1.6], [1.7], [1.7], [1.7],             # Tile (LBA, LBBC, LBD, GAP1, GAP2, GAP3, EBA, EBBC, EBD )
                        [4.9], [4.9], [4.9] ],                                                     # FCAL0, FCAL1, FCAL2
        'transitionRegionEta': 
                                 [0.1,   #['PreSamplerB',
                                  0.15,  #'EMB1',
                                  0.2,   #'EMB2',
                                  0.2,   #'EMB3',
                                  0.1,   #'PreSamplerE',
                                  0.1,   #'EME1',
                                  0.2,   #'EME2',
                                  0.3,   #'EME3',
                                  0.2,   #'HEC0',
                                  0.2,   #'HEC1',
                                  0.2,   #'HEC2',
                                  0.3,   #'HEC3',
                                  0.15,  #'Tile_LB_A',
                                  0.15,  #'Tile_LB_BC',
                                  0.15,  #'Tile_LB_D',
                                  0.0,   #'Tile_Gap1_C10',
                                  0.0,   #'Tile_Gap2_D4',
                                  0.15,  #'Tile_Gap3_E',
                                  0.15,  #'Tile_EB_A',
                                  0.15,  #'Tile_EB_BC',
                                  0.15,  #'Tile_EB_D',
                                  0.2,   #'FCAL0',
                                  0.2,   #'FCAL1',
                                  0.2  ]#'FCAL2'],
}


####### Configuration ######
maxOfLayers     = len(dictCalo['Layer'])
etaGr = [ [] for _ in range(maxOfLayers) ]
phiGr = [ [] for _ in range(maxOfLayers) ]

regionsPerLayer = []
for lay in dictCalo['granularityEtaLayer']:
    regionsPerLayer.append(len(lay))

cellCollectionDict  = {}
for key in dictCalo.keys():
    if key=='Layer' or key=='transitionRegionEta': continue
    for lay in range(0, maxOfLayers):
        for region in range(0, regionsPerLayer[lay]):
            # print(key)
            dictCalo[key][lay][region] = round( dictCalo[key][lay][region], 6) #round the dict values to n=6 precision.
    # etaGr[lay] = regionsPerLayer[lay] * [None] # slots of DEta and DPhi for all regions, for each layer.
    # phiGr[lay] = regionsPerLayer[lay] * [None]
    # cellCollectionDict[dictCalo["Layer"][lay]]  = []

# print(cellCollectionDict)

############################
# evLim       = 1#nEvts
############################

# dictCalo['granularityEtaLayerPrecise'] = etaGr # granularity values based on CaloCell->deta() method.
# dictCalo['granularityPhiLayerPrecise'] = phiGr

# print(dictCalo['granularityPhiLayerPrecise'])

# for evN in range(0,evLim):
#     sTree.GetEntry(evN)
#     # if evN < evLim:
#     if (evN % 100) == 0:
#         print("Event %d"%(evN))

#     # jet
#     jetIndex                     = getattr(sTree,'jet_index')
#     jetPt                        = getattr(sTree,'jet_pt')
#     jetEta                       = getattr(sTree,'jet_eta')
#     jetPhi                       = getattr(sTree,'jet_phi')
#     # jet Cells 
#     jetCellIndex                 = getattr(sTree,'jet_cell_index') 
#     jetIndexCellLvl              = getattr(sTree,'jet_index_cellLvl')
#     jetCellCaloGain              = getattr(sTree,'jet_cell_caloGain')
#     jetCellLayer                 = getattr(sTree,'jet_cell_layer')
#     jetCellRegion                = getattr(sTree,'jet_cell_region')
#     jetCellEta                   = getattr(sTree,'jet_cell_eta')
#     jetCellPhi                   = getattr(sTree,'jet_cell_phi') 
#     jetCellDEta                  = getattr(sTree,'jet_cell_deta')
#     jetCellDPhi                  = getattr(sTree,'jet_cell_dphi')
#     jetCellsDistDEta             = getattr(sTree,'jet_cell_distJetDEta')
#     jetCellsDistDPhi             = getattr(sTree,'jet_cell_distJetDPhi')
#     ###
#     # jet
#     jetIndexArray               = listToNumpyArray(jetIndex)
#     jetPtArray                  = listToNumpyArray(jetPt)
#     jetEtaArray                 = listToNumpyArray(jetEta)
#     jetPhiArray                 = listToNumpyArray(jetPhi)
#     # jet Cells
#     jetCellIndexArray           = listToNumpyArray(jetCellIndex)
#     jetIndexCellLvlArray        = listToNumpyArray(jetIndexCellLvl)
#     jetCellLayerArray           = listToNumpyArray(jetCellLayer)
#     jetCellsDistDPhiArray       = listToNumpyArray(jetCellsDistDPhi)
#     jetCellsDistDEtaArray       = listToNumpyArray(jetCellsDistDEta)
#     jetCellDEtaArray            = listToNumpyArray(jetCellDEta)
#     jetCellDPhiArray            = listToNumpyArray(jetCellDPhi)
#     jetCellEtaArray             = listToNumpyArray(jetCellEta)
#     jetCellPhiArray             = listToNumpyArray(jetCellPhi)
#     jetCellRegionArray          = listToNumpyArray(jetCellRegion)

# ###
#     for jet in range(0,len(jetIndexArray)):
#         jIndex  = np.where(jetIndexCellLvlArray == jet)[0] #for each jet in the event, get cells indexes for that jet

#         for lay in range(0, maxOfLayers):         
#             layerIndex      = np.where(jetCellLayerArray[jIndex] == lay)[0]
#             # layRegionsEtaAux = regionsPerLayer[lay] * [None] # slots of DEta and DPhi for all regions, for each layer.
#             # layRegionsPhiAux = regionsPerLayer[lay] * [None]

#             if len(layerIndex) > 0:
#                 # print(farCellIndex, jetCellsDistDEtaArray[layerIndex][farCellIndex])

#                 for cellEtaIndex in range(0, len(jetCellEtaArray[layerIndex])): # for the cells within the calo sampling layer...
#                     cellCaloRegion      = jetCellRegionArray[layerIndex][cellEtaIndex]
#                     cellEta             = jetCellEtaArray[layerIndex][cellEtaIndex]
#                     cellPhi             = jetCellPhiArray[layerIndex][cellEtaIndex]
#                     cellDeltaEta        = jetCellsDistDEtaArray[layerIndex][cellEtaIndex]
#                     cellDeltaPhi        = jetCellsDistDPhiArray[layerIndex][cellEtaIndex]
#                     cellDEta            = jetCellDEtaArray[layerIndex][cellEtaIndex]
#                     cellDPhi            = jetCellDPhiArray[layerIndex][cellEtaIndex]
                    # jcIndex             = jetCellIndexArray[layerIndex][cellEtaIndex]

                    # cellIdentity        = [cellEta, cellPhi, cellDEta, cellDPhi]
                    # if cellIdentity not in cellCollectionDict[dictCalo["Layer"][lay]]: ## Add to dict only if that cell wasnt added before.
                    #     cellCollectionDict[dictCalo["Layer"][lay]].append(cellIdentity)

                    # for layerRegionIndex, etaMin, etaMax in zip(range(0,regionsPerLayer[lay]), dictCalo['etaLowLim'][lay], dictCalo['etaHighLim'][lay]): # for each region inside a calo sampling layer...
                    # # if dictCalo["Layer"][lay] == 'EME1':
                    # #     print ( " Layer: {}, {} <= {} < {}, cellDeta: {}".format(dictCalo["Layer"][lay], etaMin, (cellEta), etaMax, cellDEta ))
                    #     if np.fabs(cellEta) >= etaMin and np.fabs(cellEta) < etaMax: #verify layer eta limits
                    #         # layRegionsEtaAux[layerRegionIndex]  = cellDEta
                    #         # layRegionsPhiAux[layerRegionIndex]  = cellDPhi
                    #         # if dictCalo["Layer"][lay] == 'EME1':
                    #         #     print ( " Layer: {}, {} <= {} < {}, cellDeta: {}".format(dictCalo["Layer"][lay], etaMin, (cellEta), etaMax, cellDEta ))
                            
                    #         # bInsideLArCrack = (isInsideLArCrackRegion(cellEta) and (cellCaloRegion == 1)) # verify LAr crack region # 1.37 <= eta <= 1.52 and >= 2.37
                    #         dictCalo['granularityEtaLayerPrecise'][lay][layerRegionIndex]   = cellDEta
                    #         # dictCalo['granularityEtaLayerPrecise'][lay] = np.unique( dictCalo['granularityEtaLayerPrecise'][lay] ).tolist()
                    #         dictCalo['granularityPhiLayerPrecise'][lay][layerRegionIndex]   = cellDPhi
                    #         # dictCalo['granularityPhiLayerPrecise'][lay] = np.unique( dictCalo['granularityPhiLayerPrecise'][lay] ).tolist()


saveAsJsonFile(dictCalo , "dictCaloByLayer.json" )
# saveAsJsonFile( cellCollectionDict, )
# save_pickle("cellCollectionDict.pickle", cellCollectionDict)

##
# if bProcess:
#     #
#     # Load the generated 'cellCollectionDict.pickle', extract the limits of each region,
#     # and get its associated cell granularity in eta/phi.
#     # At the end, add those values to the main 'dictCaloByLayer.json'
#     #
#     cellCollectionDict  = load_pickle("cellCollectionDict.pickle")
#     dictCalo            = loadJsonFile("dictCaloByLayer.json")

#     maxOfLayers         = len(dictCalo['Layer'])

#     # New keys for limits in eta:
#     keyEtaLowPrecise    = 'etaLowLim'
#     keyEtaHighPrecise   = 'etaHighLim'
    
#     dictCalo[keyEtaHighPrecise] = []
#     dictCalo[keyEtaLowPrecise]  = []

#     print(cellCollectionDict['Tile_LB_A'][1:10])

#     for lay in range(0, maxOfLayers):
#         layerKey        = dictCalo["Layer"][lay]
#         uniqueDEta      = np.unique( np.array(cellCollectionDict[layerKey])[:,2] ).tolist()
#         uniqueDPhi      = np.unique( np.array(cellCollectionDict[layerKey])[:,3] ).tolist()
#         # print(uniqueDEta)
#         # print("Layer {}:\t\t Unique DPhi: {}.".format(layerKey, [float(x) for x in uniqueDPhi ] ) )
#         print("Layer {}:\t\t Unique DEta: {}.".format(layerKey, [float(x) for x in uniqueDEta ] ) )

        # print(lay,np.shape(uniqueGranEta))
        # if lay==23:
        #     print(uniqueGranEta)

        # for cell in cellCollectionDict[layerKey]:
        #     cellEta     = cell[0]
        #     cellPhi     = cell[1]
        #     cellDEta    = cell[2]
        #     cellDPhi    = cell[3]

            


