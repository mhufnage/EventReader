#!/bin/sh
shopt -s expand_aliases
source $HOME/.bashrc
############## Batch Mode ###############
# setupATLAS;
# asetup Athena,22.0.44;

echo "Get noise from jet distant cells in each Layer within the ROI..."
echo "python validPlots.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts' --events=-1 --getNoise=True"
python validPlots.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts' --events=-1 --getNoise=True

for i in {1..5}
    do
        # Validation Plots + Histograms
        echo "$i bins..."
        echo "python validPlots.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts' --events=-1 --fileAlias='${i}CellPerBin_' --nCellPerBin=$i"        
        python validPlots.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts' --events=-1 --fileAlias=${i}'CellPerBin_' --nCellPerBin=$i

        echo "python readHisto.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts/histograms/' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts/plots/${i}CellPerBin/' --fileAlias='${i}CellPerBin_' "
        python readHisto.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts/histograms/' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts/plots/'${i}'CellPerBin/' --fileAlias=${i}'CellPerBin_'

    done

