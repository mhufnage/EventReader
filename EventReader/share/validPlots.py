import ROOT
import numpy as np
# from ROOT import TH1F, TTree, TChain, TFile, gROOT
from glob import glob
from time import time
from histHelper import histLayerRegionFixBin, histLayerDynBin, histTH1F, histTH1D, histTH2F, plotTH1F, plotTH2F  #custom
from functionsHelper import correctedDeltaPhi, calcDeltaEta, getCellEneTimeFromChannel, listToNumpyArray, digitsToList, isInsideLArCrackRegion, loadJsonFile, saveAsJsonFile, getCentralCellIndexJetROI_deltaR #custom
import math
import argparse
import logging
import os

ROOT.gROOT.SetBatch(True)
# ROOT.gROOT.SetStyle("ATLAS")
################################

############################
#### PARSE ARGUMENTS   #####
############################
# Usage example: 
#   python validPlots.py  --getNoise=True
#   python validPlots.py --inputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root' --outputFilesPath='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts'


# parse arguments good tutorial found at https://levelup.gitconnected.com/the-easy-guide-to-python-command-line-arguments-96b4607baea1
parser = argparse.ArgumentParser(description='Script to read the dumped file from EventReader, and generate the validation plots as TH1F in *.root files.')
parser.add_argument("--fileAlias", type=str, default='test_', help='name alias to generated *.root files. ( <fileAlias>jetAnalysis.root )')
parser.add_argument("--getNoise", type=bool, default=False, help='get the histogram and dict of noisy cells around the Jet ROI.')
parser.add_argument("--events", type=int, default=-1, help='amount of events to be processed. Default is all the events pointed by the file directory.')
parser.add_argument("--nCellPerBin", type=int, default=1, help='number of cells for each bin, in each calorimeter region. Default is 1. Valid only for histograms that contains eta and phi coordinates.')
# parser.add_argument("--n_process", type=int, default=4, help='This determines the amount of processors to be used: 1 = single proccess, 2+ = multiprocessing.')
parser.add_argument("--nSigmaCut", type=int, default=2, help='This determines the n times the relative to the sigma noise, the cuts will be performed in the JetROI. Default is 2.')
parser.add_argument("--outputFilesPath", type=str, default='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_50kEvts', help='path for the output *.root files. It may create folders in this dir.')
parser.add_argument("--inputFilesPath", type=str, default='/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root', help='path for the file(s). It accepts the glob (*) notation.')
# parser.add_argument("--outputFolder", type=str, default='singlePi0', help='name alias the output histograms, related to dumped data.')

args = parser.parse_args()

# n_process = args.n_process
parse_aliasName         = args.fileAlias
parse_events            = args.events
parse_getNoise          = args.getNoise
outputFilesPath         = args.outputFilesPath
inputFilesPath          = args.inputFilesPath
cutNoiseSigma           = args.nSigmaCut
nCellPerBin             = args.nCellPerBin
# outputFolder            = args.outputFolder

########################################
#######  PATHS AND FILENAMES    ########
########################################
# outputFilesPath = '/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run' #path for the 'run' dir, where the package will be executed
# inputFilesPath  = '/eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ESD_pi0_*.root' #dumped files absolute path
# outputFolder    = 'singlePi0' #name of output folder for dumped data pointed in 'inputFilesPath'

# inputFileName  = glob(cernbox+'scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuples/ntuple_singlePi0_Test.root') #lxplus
# outputDir = cernbox+'scripts_lxplus/Grid/workAthena/ALPPackages/run/singlePi0_test/' #lxplus
inputFileName   = glob('{}'.format(inputFilesPath)) # ntuple_singlePi0_Truth000001
outputDir       = '{}/'.format(outputFilesPath) #lxplus
outputDirHist   = outputDir+'histograms/'
fDictLayerName  = 'dictCaloByLayer.json'
# inputFileName = '/home/mhufnagel/cernbox/scripts_lxplus/Grid/workAthena/ALPPackages/run/ntuple.root' #local

if not(os.path.isdir(outputDir)):
    os.mkdir(outputDir)
    print("Path {} created.".format(outputDir))
if not(os.path.isdir(outputDirHist)):
    os.mkdir(outputDirHist)
    print("Path {} created.".format(outputDirHist))

clusFileName        = parse_aliasName+'clusterAnalysis.root'
jetFileName         = parse_aliasName+'jetAnalysis.root'
partFileName        = parse_aliasName+'particleAnalysis.root'
histConfigDictName  = parse_aliasName+"histConfigDict.json"
noiseFileName       = 'noiseCellJetROI.root'
caloNoiseDictName   = "caloNoiseDict.json"

############################
#######    LOGGING    ######
############################
if parse_getNoise: parse_aliasName = 'getNoise_'
logging.basicConfig(
        format='%(asctime)s %(levelname)-8s %(message)s',
        filename=outputDirHist+parse_aliasName+'configLog.log',
        filemode='w', # for appending the logs at the same file, comment this line
        # filename='logFile_{:%Y-%m-%d}.log'.format(datetime.now()
        level=logging.INFO,
        datefmt='%Y-%m-%d %H:%M:%S')
logging.info("+++++Starting Execution of Script.")

############################
##   LOAD DUMPED DATA     ##
############################

sTree = ROOT.TChain("data",'')
logging.info("input files: \n")
for file in inputFileName:
    fileNameInputLog = file+" added to the TChain..."
    print(fileNameInputLog)
    logging.info(fileNameInputLog)    
    sTree.Add(file+"/data")

if parse_events == -1:
    nEvts = sTree.GetEntries()
else:
    nEvts = parse_events

    
sTree.GetEntry(0) #get first entry to load constants

##################################################################################################################
####### Configuration ######
############################
jetROI              = np.float32(getattr(sTree, 'jet_roi_r'))
# maxOfLayers         = len(dictCaloLayer['Layer']) 
# clusFileName        = parse_aliasName+'clusterAnalysis.root'
# jetFileName         = parse_aliasName+'jetAnalysis.root'
# partFileName        = parse_aliasName+'particleAnalysis.root'
# histConfigDictName  = parse_aliasName+"histConfigDict.json"
# noiseFileName       = 'noiseCellJetROI.root'
# caloNoiseDictName   = "caloNoiseDict.json"
evLim               = nEvts
N_farCells          = 2 # number of cells in the border of jetROI, to add in the noise histograms.
# transitionEtaWidthL = dictCaloLayer["transitionRegionEta"] #0.2 # eta value around the transition regions to consider (or discard) cells which could be tagged as belonging to another region in the same layer.
# transitionEtaWidthH = dictCaloLayer["transitionRegionEta"] #0.2 # (eta + transitionEtaWidthL) < Eta < (eta - transitionEtaWidthH). A value for each layer.
# cutNoiseSigma       = 2 # cut = cutNoiseSigma * sigma
eneGevRange         = 400 #GeV
binsEneGevRange     = 200
cutPtDeltaR         = 0 #MeV <unused yet>
divBinEtaReg        = nCellPerBin #division of binning of each calo region, based on multiple width of the cell granularity, in that region. (bins per cell)
divBinPhiReg        = nCellPerBin
#### Main Switches ####
dataHasBadCh        = False # if the data dumped has the flag 'noBadCells=True', this flag has to be False. Otherwise, its True.
isMC                = False
# performCut          = True
loadClusters        = True
loadJets            = False # <--
test                = False
getNoiseCell        = False
getDigits           = True
loadParticles       = False
saveHistograms      = False
################################################################################################################

if parse_getNoise: # if set flag to get noise, override the others.
    # performCut      = False
    loadClusters    = False
    loadJets        = False
    getNoiseCell    = parse_getNoise
    loadParticles   = False
    saveHistograms  = True

# ****** DICTIONARIES ******
dictCaloLayer       = loadJsonFile(fDictLayerName) # Load Calo Geometry Dictionary
transitionEtaWidthL = dictCaloLayer["transitionRegionEta"] #0.2 # eta value around the transition regions to consider (or discard) cells which could be tagged as belonging to another region in the same layer.
transitionEtaWidthH = dictCaloLayer["transitionRegionEta"] #0.2 # (eta + transitionEtaWidthL) < Eta < (eta - transitionEtaWidthH). A value for each layer.
maxOfLayers         = len(dictCaloLayer['Layer']) 
if not(getNoiseCell): 
    noiseInfoLayer  = loadJsonFile(outputDirHist+caloNoiseDictName) # Load Noise dictionary
    # cutNoiseSigma   = noiseInfoLayer["cutNoiseSigma"]

# ****** LOGGING ******
logMsg1 = ' ** Configuration of validPlots.py script **\nConfiguration:\n\tisMC \'{}\'\n\tloadClusters \'{}\'\n\tloadJets \'{}\'\n\tgetNoiseCell \'{}\'\n\tloadParticles \'{}\'\n\tsaveHistograms \'{}\'\n\tdivBinEtaReg {}\n\tdivBinPhiReg {}\n\tevLim {}\n\tcutNoiseSigma {}'.format(
        isMC, loadClusters, loadJets, getNoiseCell, loadParticles, saveHistograms, divBinEtaReg, divBinPhiReg, evLim, cutNoiseSigma)
print(logMsg1)
logging.info(logMsg1)
logging.info("Output histogram directory: {}".format(outputDirHist))
logging.info("Output files: \n{}\n{}\n{}".format(clusFileName,jetFileName, partFileName))
if os.path.isfile(outputDirHist+noiseFileName) and not(getNoiseCell): logging.info("Generated noise dictionary file for jet ROI: {}".format(noiseFileName))
else: logging.info("Generated noise dictionary file for jet ROI: <not available>")

# Histograms (Cluster - Cell)
hClusterCellEta = histTH1D('clus_eta_cell', 'clus_eta_cell',100,-6,6)
hClusterCellPhi = histTH1D('clus_phi_cell', 'clus_phi_cell',64,-math.pi,math.pi)
hClusterEta     = histTH1D('clus_eta','clus_eta',100,-7,7)
hClusterPhi     = histTH1D('clus_phi','clus_phi',64,-math.pi,math.pi)
hClusterEtaCalc     = histTH1D('clus_eta_calc','clus_eta_calc',100,-6,6)
hClusterPhiCalc     = histTH1D('clus_phi_calc','clus_phi_calc',64,-math.pi,math.pi)
hClusterEtaDiff     = histTH1D('clus_eta_diff','clus_eta_diff',100,-6,6)
hClusterPhiDiff     = histTH1D('clus_phi_diff','clus_phi_diff',64,-math.pi,math.pi)

# Histograms (Jet - Cell)
histTypeErrorBar            = 'hpe0'
histTypeSimple              = ''
hDeltaEtaJetList            = histLayerDynBin(dictCaloLayer,dim='eta', bothSides=True, hLim=jetROI, divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg,histNameAlias='jetDelta_')
hDeltaPhiJetList            = histLayerDynBin(dictCaloLayer,dim='phi', bothSides=True, hLim=jetROI, divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg,histNameAlias='jetDelta_')
hDeltaEtaJetList_cut        = histLayerDynBin(dictCaloLayer,dim='eta', bothSides=True, hLim=jetROI, divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg,histNameAlias='jetDeltaCut_')
hDeltaPhiJetList_cut        = histLayerDynBin(dictCaloLayer,dim='phi', bothSides=True, hLim=jetROI, divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg,histNameAlias='jetDeltaCut_')
hDeltaCentralEtaJetList     = histLayerDynBin(dictCaloLayer,dim='eta', bothSides=True, hLim=jetROI, divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg,histNameAlias='jetDeltaCentral_')
hDeltaCentralPhiJetList     = histLayerDynBin(dictCaloLayer,dim='phi', bothSides=True, hLim=jetROI, divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg,histNameAlias='jetDeltaCentral_')
hDeltaCentralEtaJetList_cut = histLayerDynBin(dictCaloLayer,dim='eta', bothSides=True, hLim=jetROI, divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg,histNameAlias='jetDeltaCentralCut_')
hDeltaCentralPhiJetList_cut = histLayerDynBin(dictCaloLayer,dim='phi', bothSides=True, hLim=jetROI, divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg,histNameAlias='jetDeltaCentralCut_')
hEtaList_pos                = histLayerDynBin(dictCaloLayer,dim='eta', useRegionLimits=True, negSide=False, divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg, histNameAlias='jetEtaPos_' ) #histograms of cell eta inside jet ROI
hPhiList_pos                = histLayerDynBin(dictCaloLayer,dim='phi', useRegionLimits=True, negSide=False, divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg, histNameAlias='jetPhiPos_' )
hEtaList_neg                = histLayerDynBin(dictCaloLayer,dim='eta', useRegionLimits=True, negSide=True , divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg, histNameAlias='jetEtaNeg_' )
hPhiList_neg                = histLayerDynBin(dictCaloLayer,dim='phi', useRegionLimits=True, negSide=True , divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg, histNameAlias='jetPhiNeg_' )
hEtaList_pos_cut            = histLayerDynBin(dictCaloLayer,dim='eta', useRegionLimits=True, negSide=False, divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg, histNameAlias='jetEtaPosCut_' ) #histograms of cell eta inside jet ROI
hPhiList_pos_cut            = histLayerDynBin(dictCaloLayer,dim='phi', useRegionLimits=True, negSide=False, divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg, histNameAlias='jetPhiPosCut_' )
hEtaList_neg_cut            = histLayerDynBin(dictCaloLayer,dim='eta', useRegionLimits=True, negSide=True , divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg, histNameAlias='jetEtaNegCut_' )
hPhiList_neg_cut            = histLayerDynBin(dictCaloLayer,dim='phi', useRegionLimits=True, negSide=True , divBinEta=divBinEtaReg, divBinPhi=divBinPhiReg, histNameAlias='jetPhiNegCut_' )
hNoiseCalcEneList           = histLayerRegionFixBin(dictCaloLayer,dim='eta',name='jet_NoiseCalcEneMeV', hBins=200, hLim1=-400, hLim2=400) #fill with the farthest away cell from jet (only cell noise)
hNoiseCalcRawEneList        = histLayerRegionFixBin(dictCaloLayer,dim='eta',name='jet_NoiseCalcRawEneMeV', hBins=200, hLim1=-400, hLim2=400) #fill with the farthest away cell from jet (only raw ch noise)
hRawChTimeList              = histLayerRegionFixBin(dictCaloLayer,dim='eta',name='jet_cellRawChTime', hBins=40, hLim1=-100, hLim2=100)
hRawChEneList               = histLayerRegionFixBin(dictCaloLayer,dim='eta',name='jet_cellRawChEneGeV', hBins=binsEneGevRange, hLim1=-20, hLim2=eneGevRange)
hRawChTimeList_cut          = histLayerRegionFixBin(dictCaloLayer,dim='eta',name='jet_cellRawChTimeCut', hBins=40, hLim1=-100, hLim2=100)
hRawChEneList_cut           = histLayerRegionFixBin(dictCaloLayer,dim='eta',name='jet_cellRawChEneGeVCut', hBins=binsEneGevRange, hLim1=-20, hLim2=eneGevRange)
hChTimeList                 = histLayerRegionFixBin(dictCaloLayer,dim='eta',name='jet_cellChTime', hBins=40, hLim1=-100, hLim2=100)
hChEneList                  = histLayerRegionFixBin(dictCaloLayer,dim='eta',name='jet_cellChEneGeV', hBins=binsEneGevRange, hLim1=-20, hLim2=eneGevRange)
hChTimeList_cut             = histLayerRegionFixBin(dictCaloLayer,dim='eta',name='jet_cellChTimeCut', hBins=40, hLim1=-100, hLim2=100)
hChEneList_cut              = histLayerRegionFixBin(dictCaloLayer,dim='eta',name='jet_cellChEneGeVCut', hBins=binsEneGevRange, hLim1=-20, hLim2=eneGevRange)
hJetCellEta                 = histTH1F('jet_eta_cell', 'jet_eta_cell',100,-7,7)
hJetCellPhi                 = histTH1F('jet_phi_cell', 'jet_phi_cell',64,-math.pi,math.pi)
hJetEta                     = histTH1F('jet_eta', 'jet_eta',100,-7,7)
hJetPhi                     = histTH1F('jet_phi', 'jet_phi',64,-math.pi,math.pi)
hJetPt                      = histTH1F('jet_pt', 'jet_pt',binsEneGevRange,0,eneGevRange)
hJetCellEta_cut             = histTH1F('jet_eta_cell_cut', 'jet_eta_cell_cut',100,-7,7)
hJetCellPhi_cut             = histTH1F('jet_phi_cell_cut', 'jet_phi_cell_cut',64,-math.pi,math.pi)
h2JetCellEDeltaEta          = histTH2F('jet_cell_ene_deltaEta','jet_cell_ene_deltaEta',50,-jetROI,jetROI,100,-20,100)
h2JetCellEDeltaPhi          = histTH2F('jet_cell_ene_deltaPhi','jet_cell_ene_deltaPhi',50,-jetROI,jetROI,100,-20,100)
h2JetCellEDeltaEta_cut      = histTH2F('jet_cell_ene_deltaEta_cut','jet_cell_ene_deltaEta_cut',50,-jetROI,jetROI,100,-20,100)
h2JetCellEDeltaPhi_cut      = histTH2F('jet_cell_ene_deltaPhi_cut','jet_cell_ene_deltaPhi_cut',50,-jetROI,jetROI,100,-20,100)

# Histograms (Particles)
binsDeltaR            = 100
hPhotonEnergy         = histTH1F('Photon_energy','Photon_energy',binsEneGevRange,0,eneGevRange)
hPhotonTruthEnergy    = histTH1F('Photon_TruthEnergy','Photon_TruthEnergy',binsEneGevRange,0,eneGevRange)
hParticlePdgId        = histTH1F('MC_pdgId', 'MC_pdgId',70,0,200)
hPhotonRecoDeltaR     = histTH1F('PhotonReco_deltaR','PhotonReco_deltaR',binsDeltaR,0,1)
hPhotonTruthDeltaR    = histTH1F('PhotonTruth_deltaR','PhotonTruth_deltaR',binsDeltaR,0,1)
hPhotonRecoDeltaEta   = histTH1F('PhotonReco_deltaEta','PhotonReco_deltaEta',200,-5,5)
hPhotonTruthDeltaEta  = histTH1F('PhotonTruth_deltaEta','PhotonTruth_deltaEta',200,-5,5)
hPhotonRecoDeltaPhi   = histTH1F('PhotonReco_deltaPhi','PhotonReco_deltaPhi',64,-math.pi,math.pi)
hPhotonTruthDeltaPhi  = histTH1F('PhotonTruth_deltaPhi','PhotonTruth_deltaPhi',64,-math.pi,math.pi)
# hPhotonTruthDeltaR_lorentz = ROOT.TH1F('PhotonTruth_deltaR_lorentz','PhotonTruth_deltaR_lorentz',50,0,1)
# hPhotonTruthDeltaR.SetDirectory(0)

# hDEtaList = histLayer(dictCaloLayer['Layer'],dim='eta',hBins=20,hLim=0.5)
# hDPhiList = histLayer(dictCaloLayer['Layer'],dim='phi',hBins=20,hLim=0.5)
# hEtaList = histLayer(dictCaloLayer['Layer'],dim='eta',hBins=20,hLim=6.3)
# hPhiList = histLayer(dictCaloLayer['Layer'],dim='phi',hBins=20,hLim=6.3)

# Number of regions in each sampling layer (eta x phi region)
regionsPerLayer = []
for reg in dictCaloLayer['granularityEtaLayer']:
    regionsPerLayer.append(len(reg))

evN = 0

# Histogram files
# layIndex = 1
start = time()
for evN in range(0,evLim):
    sTree.GetEntry(evN)
    if (evN % 500) == 0:
        evtLogInfo = "Event %d / %d"%(evN, evLim)
        print(evtLogInfo)
        logging.info(evtLogInfo)

    if loadClusters:
        clusterIndex                 = getattr(sTree,'cluster_index')
        clusterPt                    = getattr(sTree,'cluster_pt')
        clusterEta                   = getattr(sTree,'cluster_eta')
        clusterPhi                   = getattr(sTree,'cluster_phi')
        clusterEtaCalc               = getattr(sTree,'cluster_eta_calc')
        clusterPhiCalc               = getattr(sTree,'cluster_phi_calc')
        # Cluster Cells 
        clusterCellIndex             = getattr(sTree,'cluster_cell_index') 
        clusterIndexCellLvl          = getattr(sTree,'cluster_index_cellLvl')
        clusterCellCaloGain          = getattr(sTree,'cluster_cell_caloGain')
        clusterCellLayer             = getattr(sTree,'cluster_cell_layer')
        clusterCellRegion            = getattr(sTree,'cluster_cell_region')
        clusterCellEta               = getattr(sTree,'cluster_cell_eta')
        clusterCellPhi               = getattr(sTree,'cluster_cell_phi') 
        clusterCellDEta              = getattr(sTree,'cluster_cell_deta')
        clusterCellDPhi              = getattr(sTree,'cluster_cell_dphi')
        clusterCellsDistDEta         = getattr(sTree,'cluster_cellsDist_deta')
        clusterCellsDistDPhi         = getattr(sTree,'cluster_cellsDist_dphi')
        # Cluster Channel 
        clusterIndexChLvl            = getattr(sTree,'cluster_index_chLvl')
        clusterChannelIndex          = getattr(sTree,'cluster_channel_index')
        clusterChannelDigits         = getattr(sTree,'cluster_channel_digits')
        clusterChannelEnergy         = getattr(sTree,'cluster_channel_energy')
        clusterChannelTime           = getattr(sTree,'cluster_channel_time')
        if dataHasBadCh: clusterChannelBad            = getattr(sTree,'cluster_channel_bad')
        clusterChannelChInfo         = getattr(sTree,'cluster_channel_chInfo')
        # Cluster raw channel 
        clusterIndexRawChLvl         = getattr(sTree,'cluster_index_rawChLvl')
        clusterRawChannelIndex       = getattr(sTree,'cluster_rawChannel_index')
        clusterRawChannelAmplitude   = getattr(sTree,'cluster_rawChannel_amplitude')
        clusterRawChannelTime        = getattr(sTree,'cluster_rawChannel_time')
        clusterRawChannelPedProv     = getattr(sTree,'cluster_rawChannel_pedProv')
        clusterRawChannelQual        = getattr(sTree,'cluster_rawChannel_qual')
        clusterRawChannelChInfo      = getattr(sTree,'cluster_rawChannel_chInfo')
    if loadJets or getNoiseCell or test:
        # jet
        jetIndex                     = getattr(sTree,'jet_index')
        jetPt                        = getattr(sTree,'jet_pt')
        jetEta                       = getattr(sTree,'jet_eta')
        jetPhi                       = getattr(sTree,'jet_phi')
        # jet Cells 
        jetCellIndex                 = getattr(sTree,'jet_cell_index') 
        jetIndexCellLvl              = getattr(sTree,'jet_index_cellLvl')
        jetCellCaloGain              = getattr(sTree,'jet_cell_caloGain')
        jetCellLayer                 = getattr(sTree,'jet_cell_layer')
        jetCellRegion                = getattr(sTree,'jet_cell_region')
        jetCellEta                   = getattr(sTree,'jet_cell_eta')
        jetCellPhi                   = getattr(sTree,'jet_cell_phi') 
        jetCellDEta                  = getattr(sTree,'jet_cell_deta')
        jetCellDPhi                  = getattr(sTree,'jet_cell_dphi')
        jetCellsDistDEta             = getattr(sTree,'jet_cell_distJetDEta')
        jetCellsDistDPhi             = getattr(sTree,'jet_cell_distJetDPhi')
        # Jet Channel       
        jetIndexChLvl                = getattr(sTree,'jet_index_chLvl')
        jetChannelIndex              = getattr(sTree,'jet_channel_index')
        jetChannelDigits             = getattr(sTree,'jet_channel_digits')
        jetChannelEnergy             = getattr(sTree,'jet_channel_energy')
        jetChannelTime               = getattr(sTree,'jet_channel_time')
        if dataHasBadCh: jetChannelBad                = getattr(sTree,'jet_channel_bad')
        jetChannelChInfo             = getattr(sTree,'jet_channel_chInfo')
        # # jet raw channel
        # jetIndexRawChLvl             = getattr(sTree,'jet_index_rawChLvl')
        jetRawChannelIndex           = getattr(sTree,'jet_rawChannel_index')
        jetRawChannelAmplitude       = getattr(sTree,'jet_rawChannel_amplitude')
        jetRawChannelTime            = getattr(sTree,'jet_rawChannel_time')
        # jetRawChannelPedProv         = getattr(sTree,'jet_rawChannel_pedProv')
        # jetRawChannelQual            = getattr(sTree,'jet_rawChannel_qual')        
        # jetRawChannelChInfo          = getattr(sTree,'jet_rawChannel_chInfo')

    if loadParticles:
        if isMC:
            mcTruthEnergy  = getattr(sTree,'mc_energy')
            mcTruthPt      = getattr(sTree,'mc_pt')
            mcTruthM       = getattr(sTree,'mc_m')
            mcTruthEta     = getattr(sTree,'mc_eta')
            mcTruthPhi     = getattr(sTree,'mc_phi')
            mcTruthPdgId   = getattr(sTree,'mc_pdgId')

        photonEnergy  = getattr(sTree,'ph_energy')
        photonPt      = getattr(sTree,'ph_pt')
        photonEta     = getattr(sTree,'ph_eta')
        photonPhi     = getattr(sTree,'ph_phi')
        photonM       = getattr(sTree,'ph_m')

    #**********************************************
    # Format into python list/np array
    #**********************************************
    if loadClusters:
        clusterIndexArray               = listToNumpyArray(clusterIndex)
        clusterPtArray                  = listToNumpyArray(clusterPt)
        clusterEtaArray                 = listToNumpyArray(clusterEta)
        clusterPhiArray                 = listToNumpyArray(clusterPhi)
        clusterEtaCalcArray             = listToNumpyArray(clusterEtaCalc)
        clusterPhiCalcArray             = listToNumpyArray(clusterPhiCalc)

        clusterCellIndexArray           = listToNumpyArray(clusterCellIndex)
        clusterIndexCellLvlArray        = listToNumpyArray(clusterIndexCellLvl)
        clusterCellCaloGainArray        = listToNumpyArray(clusterCellCaloGain)
        clusterCellLayerArray           = listToNumpyArray(clusterCellLayer)
        clusterCellRegionArray          = listToNumpyArray(clusterCellRegion)
        clusterCellEtaArray             = listToNumpyArray(clusterCellEta)
        clusterCellPhiArray             = listToNumpyArray(clusterCellPhi)
        clusterCellDEtaArray            = listToNumpyArray(clusterCellDEta)
        clusterCellDPhiArray            = listToNumpyArray(clusterCellDPhi)
        clusterCellsDistDEtaArray       = listToNumpyArray(clusterCellsDistDEta)
        clusterCellsDistDPhiArray       = listToNumpyArray(clusterCellsDistDPhi)

        clusterChannelIndexArray        = listToNumpyArray(clusterChannelIndex)
        clusterChannelDigitsArray       = digitsToList(clusterChannelDigits)
        clusterChannelEnergyArray       = listToNumpyArray(clusterChannelEnergy)
        clusterChannelTimeArray         = listToNumpyArray(clusterChannelTime)
        if dataHasBadCh: clusterChannelBadArray      = listToNumpyArray(clusterChannelBad)

        clusterRawChannelIndexArray     = listToNumpyArray(clusterRawChannelIndex)
        clusterRawChannelAmplitudeArray = listToNumpyArray(clusterRawChannelAmplitude)
        clusterRawChannelTimeArray      = listToNumpyArray(clusterRawChannelTime)

    if loadJets or getNoiseCell or test:
        jetIndexArray               = listToNumpyArray(jetIndex)
        jetPtArray                  = listToNumpyArray(jetPt)
        jetEtaArray                 = listToNumpyArray(jetEta)
        jetPhiArray                 = listToNumpyArray(jetPhi)

        jetCellIndexArray           = listToNumpyArray(jetCellIndex)
        jetIndexCellLvlArray        = listToNumpyArray(jetIndexCellLvl)
        jetCellLayerArray           = listToNumpyArray(jetCellLayer)
        jetCellsDistDPhiArray       = listToNumpyArray(jetCellsDistDPhi)
        jetCellsDistDEtaArray       = listToNumpyArray(jetCellsDistDEta)
        jetCellDEtaArray            = listToNumpyArray(jetCellDEta)
        jetCellDPhiArray            = listToNumpyArray(jetCellDPhi)
        jetCellEtaArray             = listToNumpyArray(jetCellEta)
        jetCellPhiArray             = listToNumpyArray(jetCellPhi)
        jetCellRegionArray          = listToNumpyArray(jetCellRegion)

        jetIndexChLvlArray          = listToNumpyArray(jetIndexChLvl)
        jetChannelIndexArray        = listToNumpyArray(jetChannelIndex)
        jetChannelDigitsArray       = digitsToList(jetChannelDigits)
        jetChannelEnergyArray       = listToNumpyArray(jetChannelEnergy)
        jetChannelTimeArray         = listToNumpyArray(jetChannelTime)
        if dataHasBadCh: jetChannelBadArray          = listToNumpyArray(jetChannelBad)
        #jetChannelChInfo

        jetRawChannelIndexArray     = listToNumpyArray(jetRawChannelIndex)
        jetRawChannelAmplitudeArray = listToNumpyArray(jetRawChannelAmplitude)
        jetRawChannelTimeArray      = listToNumpyArray(jetRawChannelTime)
    
    if loadParticles:
        mcTruthEnergyArray      = listToNumpyArray(mcTruthEnergy)
        mcTruthPtArray          = listToNumpyArray(mcTruthPt)
        mcTruthEtaArray         = listToNumpyArray(mcTruthEta)
        mcTruthPhiArray         = listToNumpyArray(mcTruthPhi)
        mcTruthPdgIdArray       = listToNumpyArray(mcTruthPdgId)
        mcTruthMArray           = listToNumpyArray(mcTruthM)
    
        photonEnergyArray       = listToNumpyArray(photonEnergy)
        photonPtArray           = listToNumpyArray(photonPt)
        photonEtaArray          = listToNumpyArray(photonEta)
        photonPhiArray          = listToNumpyArray(photonPhi)
        photonMArray            = listToNumpyArray(photonM)

#**********************************************
#  CLUSTERS PROCESSING
#**********************************************
    if loadClusters:
        for lay in range(0, maxOfLayers):            
            layerIndex = np.where(clusterCellLayerArray == lay)[0]
            if len(layerIndex) > 0:
                for layerRegionIndex, etaMin, etaMax in zip(range(0,regionsPerLayer[lay]), dictCaloLayer['etaLowLim'][lay], dictCaloLayer['etaHighLim'][lay]): # for each region inside a calo sampling layer...
                    for cellEtaIndex in range(0, len(clusterCellEtaArray[layerIndex])): # for the cells within the calo sampling layer...
                        cellCaloRegion  = clusterCellRegionArray[layerIndex][cellEtaIndex]
                        cellEta         = clusterCellEtaArray[layerIndex][cellEtaIndex]
                        cellPhi         = clusterCellPhiArray[layerIndex][cellEtaIndex]
                        cellDeltaEta    = clusterCellDEtaArray[layerIndex][cellEtaIndex]
                        cellDeltaPhi    = clusterCellDPhiArray[layerIndex][cellEtaIndex]
                        ccIndex         = clusterCellIndexArray[layerIndex][cellEtaIndex]
                                                
                        # cellEneRaw, cellTimeRaw  = getCellEneTimeFromChannel(clusterRawChannelAmplitudeArray, clusterRawChannelTimeArray, clusterRawChannelIndexArray, ccIndex)# Raw Ch                        
                        # cellEne, cellTime        = getCellEneTimeFromChannel(clusterChannelEnergyArray, clusterChannelTimeArray, clusterChannelIndexArray, ccIndex)# Cell Ch (there is ch only in Tile, otherwise, just cells)

                if getDigits:
                    print(np.shape(layerIndex), max(layerIndex), np.shape(clusterChannelDigitsArray))
                    for idx in layerIndex:
                        print(idx,clusterChannelDigitsArray[idx])
                        # print(idx)
                    # a = [clusterChannelDigitsArray[i] for i in layerIndex]
                    # print(a)
        
        for cIndex in range(len(clusterIndexArray)): # Clusters
            hClusterEta.Fill(clusterEtaArray[cIndex])
            hClusterPhi.Fill(clusterPhiArray[cIndex])
            hClusterEtaCalc.Fill(clusterEtaCalcArray[cIndex])
            hClusterPhiCalc.Fill(clusterPhiCalcArray[cIndex])

            etaDiff = np.fabs(clusterEtaCalcArray[cIndex]) - np.fabs(clusterEtaArray[cIndex])
            phiDiff = np.fabs(clusterPhiCalcArray[cIndex]) - np.fabs(clusterPhiArray[cIndex])
            hClusterEtaDiff.Fill(etaDiff)
            hClusterPhiDiff.Fill(phiDiff)
#**********************************************
#  NOISE IN JETS ROI
#**********************************************
# For each Calo region, select the 'N_farCells' CaloCells that is (are) closer to the border of the Jet ROI,
# and store their CaloCell Energy and RawEnergy in root histograms.
    if getNoiseCell:
        for jet in range(0,len(jetIndexArray)):
            jIndex  = np.where(jetIndexCellLvlArray == jet)[0] #for each jet in the event, get cells indexes for that jet
            jetEta  = jetEtaArray[jet]
            jetPhi  = jetPhiArray[jet]
            # jetPt   = jetPtArray[jet]

            for lay in range(0, maxOfLayers):
                layerIndex = np.where(jetCellLayerArray[jIndex] == lay)[0]
                if len(layerIndex) > 0:

                    # farCellIndex = np.argmax(jetCellsDistDEtaArray[layerIndex][:]) # farthest away cell from jet (only noise)
                    cellLayerDeltaR = getCentralCellIndexJetROI_deltaR(jetEta, jetPhi, jetCellEtaArray[layerIndex], jetCellPhiArray[layerIndex], jetCellRegionArray[layerIndex], getDeltaRList=True)
                    # now, get the N_farCells distant cell indexes, based on deltaR.
                    if len(cellLayerDeltaR) < int(N_farCells/2): # arbitrary rule for small cell collection
                        N_farCellsAux   = 1
                    else: N_farCellsAux = N_farCells
                    # farCellIndex    = np.argpartition(cellLayerDeltaR, -N_farCellsAux)[-N_farCellsAux:] # requires numpy > 1.8
                    farCellIndex    = np.argsort(cellLayerDeltaR)[-N_farCellsAux:]

                    for fcindex in farCellIndex: # for the cells within the calo sampling layer... loop over the farCells
                        cellCaloRegion      = jetCellRegionArray[layerIndex][fcindex]
                        cellEta             = jetCellEtaArray[layerIndex][fcindex]
                        cellPhi             = jetCellPhiArray[layerIndex][fcindex]
                        jcIndex             = jetCellIndexArray[layerIndex][fcindex]

                        # if cellEtaIndex == farCellIndex: #for noise measuring (MeV)

                        cellEneRaw, cellTimeRaw  = getCellEneTimeFromChannel(jetRawChannelAmplitudeArray, jetRawChannelTimeArray, jetRawChannelIndexArray, jcIndex)# Raw Ch                        
                        cellEne, cellTime        = getCellEneTimeFromChannel(jetChannelEnergyArray, jetChannelTimeArray, jetChannelIndexArray, jcIndex)# Cell Ch (there is ch only in Tile, otherwise, just cells)

                        for layerRegionIndex, etaMin, etaMax in zip(range(0,regionsPerLayer[lay]), dictCaloLayer['etaLowLim'][lay], dictCaloLayer['etaHighLim'][lay]): # for each region inside a calo sampling layer...
                            if np.fabs(cellEta) > etaMin and np.fabs(cellEta) < etaMax: #verify layer eta limits
                                
                                bInsideLArCrack = (isInsideLArCrackRegion(cellEta) and (cellCaloRegion == 1)) # verify LAr crack region # 1.37 <= eta <= 1.52 and >= 2.37
                                
                                if not(bInsideLArCrack): #if cell is not in the crack:
                                    # histograms without eCut
                                    # if cellEtaIndex == farCellIndex: #for noise measuring (MeV)
                                        # print("Far Cell saved !",farCellIndex)
                                        hNoiseCalcEneList[lay][layerRegionIndex].Fill(cellEne)
                                        hNoiseCalcRawEneList[lay][layerRegionIndex].Fill(cellEneRaw)

#**********************************************
#  JETS ROI PROCESSING
#********************************************** 
# Main processing chain for cells in a Jet ROI. Select the regions, calculate DeltaEta and Phi, and many validation histograms.
    if loadJets:        
        for jet in range(0,len(jetIndexArray)):
            jIndex  = np.where(jetIndexCellLvlArray == jet)[0] #for each jet in the event, get cells indexes for that jet

            jetEta  = jetEtaArray[jet]
            jetPhi  = jetPhiArray[jet]
            jetPt   = jetPtArray[jet]

            hJetEta.Fill(jetEta)
            hJetPhi.Fill(jetPhi)
            hJetPt.Fill(jetPt/1000) #GeV conversion
            
            # print(np.shape(jetCellEtaArray))
            # centralCellIndex    = getCentralCellIndexJetROI_deltaR(jetEta, 
                                                        # jetPhi, 
                                                        # jetCellEtaArray, 
                                                        # jetCellPhiArray)
            # centralCellIndex    = getCentralCellIndexJetROI_insideCell(jetEta, 
            #                                             jetPhi, 
            #                                             jetCellEtaArray, 
            #                                             jetCellPhiArray, 
            #                                             jetCellDEtaArray, 
            #                                             jetCellDPhiArray)

            for lay in range(0, maxOfLayers):            
                layerIndex = np.where(jetCellLayerArray[jIndex] == lay)[0]
                if len(layerIndex) > 0:
                    farCellIndex        = np.argmax(jetCellsDistDEtaArray[layerIndex][:]) # farthest away cell from jet (only noise)
                    centralCellIndex    = getCentralCellIndexJetROI_deltaR(jetEta, 
                                                                            jetPhi, 
                                                                            jetCellEtaArray[layerIndex],
                                                                            jetCellPhiArray[layerIndex],
                                                                            jetCellRegionArray[layerIndex])
                    # print(farCellIndex, jetCellsDistDEtaArray[layerIndex][farCellIndex])

                    for cellEtaIndex in range(0, len(jetCellEtaArray[layerIndex])): # for the cells within the calo sampling layer...
                        cellCaloRegion      = jetCellRegionArray[layerIndex][cellEtaIndex]
                        cellEta             = jetCellEtaArray[layerIndex][cellEtaIndex]
                        cellPhi             = jetCellPhiArray[layerIndex][cellEtaIndex]
                        cellDeltaEta        = jetCellsDistDEtaArray[layerIndex][cellEtaIndex]
                        cellDeltaPhi        = jetCellsDistDPhiArray[layerIndex][cellEtaIndex]
                        cellDeltaCentralEta = calcDeltaEta(jetCellEtaArray[layerIndex][centralCellIndex], jetCellEtaArray[layerIndex][cellEtaIndex]) # applied precision limit
                        cellDeltaCentralPhi = correctedDeltaPhi(jetCellPhiArray[layerIndex][centralCellIndex], jetCellPhiArray[layerIndex][cellEtaIndex]) # applied precision limit
                        cellDEta            = jetCellDEtaArray[layerIndex][cellEtaIndex]
                        cellDPhi            = jetCellDPhiArray[layerIndex][cellEtaIndex]
                        jcIndex             = jetCellIndexArray[layerIndex][cellEtaIndex]

                        cellEneRaw, cellTimeRaw  = getCellEneTimeFromChannel(jetRawChannelAmplitudeArray, jetRawChannelTimeArray, jetRawChannelIndexArray, jcIndex)# Raw Ch                        
                        cellEne, cellTime        = getCellEneTimeFromChannel(jetChannelEnergyArray, jetChannelTimeArray, jetChannelIndexArray, jcIndex)# Cell Ch (there is ch only in Tile, otherwise, just cells)

                        for layerRegionIndex, etaMin, etaMax in zip(range(0,regionsPerLayer[lay]), dictCaloLayer['etaLowLim'][lay], dictCaloLayer['etaHighLim'][lay]): # for each region inside a calo sampling layer...
                            # if np.fabs(cellEta) >= etaMin and np.fabs(cellEta) < etaMax: #verify layer eta limits (old criteria)
                            # if np.fabs(cellEta) >= (etaMin - transitionEtaWidthL[lay]) and np.fabs(cellEta) < (etaMax + transitionEtaWidthH[lay]): # criteria with expanded eta limits
                            if np.fabs(cellEta) >= (etaMin + transitionEtaWidthL[lay]) and np.fabs(cellEta) < (etaMax - transitionEtaWidthH[lay]): # criteria with Reduced eta limits

                                 ### DEBUG ####
                                # if cellDEta != dictCaloLayer['granularityEtaLayerPrecise'][lay][layerRegionIndex]:# or cellDPhi != dictCaloLayer['granularityPhiLayerPrecise'][lay][layerRegionIndex]:                                   
                                #     logging.info("Event: {}, {}, region: {} < eta < {}, eta: {} (DEta: truth={}/actual={}), phi: {} (truth={}/actual={})".format(evN, dictCaloLayer["Layer"][lay], etaMin, etaMax, cellEta, dictCaloLayer['granularityEtaLayerPrecise'][lay][layerRegionIndex], cellDEta, cellPhi, dictCaloLayer['granularityPhiLayerPrecise'][lay][layerRegionIndex], cellDPhi))
                            
                                # if cellDPhi != dictCaloLayer['granularityPhiLayerPrecise'][lay][layerRegionIndex]:# or cellDPhi != dictCaloLayer['granularityPhiLayerPrecise'][lay][layerRegionIndex]:                                   
                                #     logging.info("Event: {}, {}, region: {} < eta < {}, eta: {} (DEta: truth={}/actual={}), phi: {} (truth={}/actual={})".format(evN, dictCaloLayer["Layer"][lay], etaMin, etaMax, cellPhi, dictCaloLayer['granularityEtaLayerPrecise'][lay][layerRegionIndex], cellDEta, cellPhi, dictCaloLayer['granularityPhiLayerPrecise'][lay][layerRegionIndex], cellDPhi))
                                # if lay==2:
                                #     logging.info("Event: {}, CaloRegion: {}, Layer: {}. \ncentralEta/Phi: {}/{}, centralDEta/DPhi: {}/{}, \ncellEta/phi: {}/{}, DEta/DPhi: {}/{}, deltaEta/Phi: {}/{}".format(evN, cellCaloRegion, dictCaloLayer['Layer'][lay], jetCellEtaArray[layerIndex][centralCellIndex], jetCellPhiArray[layerIndex][centralCellIndex], jetCellDEtaArray[layerIndex][centralCellIndex], jetCellDPhiArray[layerIndex][centralCellIndex], cellEta, cellPhi, cellDEta, cellDPhi, cellDeltaCentralEta, cellDeltaCentralPhi))

                                bInsideLArCrack = (isInsideLArCrackRegion(cellEta) and (cellCaloRegion == 1)) # verify LAr crack region # 1.37 <= eta <= 1.52 and >= 2.37
                                
                                if not(bInsideLArCrack): #if cell is not in the crack:
                                    # bLArCellBelongsToECutRegion     = (cellCaloRegion > 0)  and (cellCaloRegion < 5) and (cellEne > eCutLArCells)
                                    # bTileCellBelongsToECutRegion    = (cellCaloRegion == 0) and (cellEne > eCutTileCells)
                                    # bFwdBelongsToECutRegion         = (cellCaloRegion == 5) and (cellEne > eCutFwdCells)

                                    cutCellEne    = cutNoiseSigma * noiseInfoLayer["eStd"][lay][layerRegionIndex] #MeV
                                    # cutCellEneRaw = cutNoiseSigma * noiseInfoLayer["eRawStd"][lay][layerRegionIndex] #MeV + ADC (Tile)

                                    # histograms without eCut
                                    hRawChTimeList[lay][layerRegionIndex].Fill(cellTimeRaw)
                                    hRawChEneList[lay][layerRegionIndex].Fill(cellEneRaw/1000)
                                    hChTimeList[lay][layerRegionIndex].Fill(cellTime)
                                    hChEneList[lay][layerRegionIndex].Fill(cellEne/1000)
                                    if cellEtaIndex == farCellIndex: #for noise measuring (MeV)
                                        # print("Far Cell saved !",farCellIndex)
                                        hNoiseCalcEneList[lay][layerRegionIndex].Fill(cellEne)
                                        hNoiseCalcRawEneList[lay][layerRegionIndex].Fill(cellEneRaw)

                                    hJetCellEta.Fill(cellEta)
                                    hJetCellPhi.Fill(cellPhi)

                                    if (cellEta >= 0):
                                        hEtaList_pos[lay][layerRegionIndex].Fill(cellEta) # fill hist by cell eta region criteria
                                        hPhiList_pos[lay][layerRegionIndex].Fill(cellPhi)
                                    else:
                                        hEtaList_neg[lay][layerRegionIndex].Fill(cellEta)
                                        hPhiList_neg[lay][layerRegionIndex].Fill(cellPhi)

                                    hDeltaEtaJetList[lay][layerRegionIndex].Fill(cellDeltaEta)
                                    hDeltaPhiJetList[lay][layerRegionIndex].Fill(cellDeltaPhi)
                                    hDeltaCentralEtaJetList[lay][layerRegionIndex].Fill(cellDeltaCentralEta)
                                    hDeltaCentralPhiJetList[lay][layerRegionIndex].Fill(cellDeltaCentralPhi)

                                    h2JetCellEDeltaEta.Fill(cellDeltaEta, cellEne/1000)
                                    h2JetCellEDeltaPhi.Fill(cellDeltaPhi, cellEne/1000)

                                    # histograms WITH eCut
                                    # if (bLArCellBelongsToECutRegion or bTileCellBelongsToECutRegion or bFwdBelongsToECutRegion):
                                    if cellEne > cutCellEne:
                                        hRawChTimeList_cut[lay][layerRegionIndex].Fill(cellTimeRaw)
                                        hRawChEneList_cut[lay][layerRegionIndex].Fill(cellEneRaw/1000)
                                        hChTimeList_cut[lay][layerRegionIndex].Fill(cellTime)
                                        hChEneList_cut[lay][layerRegionIndex].Fill(cellEne/1000)

                                        if (cellEta >= 0):
                                            hEtaList_pos_cut[lay][layerRegionIndex].Fill(cellEta) # fill hist by cell eta region criteria
                                            hPhiList_pos_cut[lay][layerRegionIndex].Fill(cellPhi)
                                        else:
                                            hEtaList_neg_cut[lay][layerRegionIndex].Fill(cellEta) # fill hist by cell eta region criteria
                                            hPhiList_neg_cut[lay][layerRegionIndex].Fill(cellPhi)

                                        hDeltaEtaJetList_cut[lay][layerRegionIndex].Fill(cellDeltaEta)
                                        hDeltaPhiJetList_cut[lay][layerRegionIndex].Fill(cellDeltaPhi)
                                        hDeltaCentralEtaJetList_cut[lay][layerRegionIndex].Fill(cellDeltaCentralEta)
                                        hDeltaCentralPhiJetList_cut[lay][layerRegionIndex].Fill(cellDeltaCentralPhi)

                                        h2JetCellEDeltaEta_cut.Fill(cellDeltaEta, cellEne/1000)
                                        h2JetCellEDeltaPhi_cut.Fill(cellDeltaPhi, cellEne/1000)

    if test:        
        for jet in range(0,len(jetIndexArray)):
            jIndex  = np.where(jetIndexCellLvlArray == jet)[0] #for each jet in the event, get cells indexes for that jet

            jetEta  = jetEtaArray[jet]
            jetPhi  = jetPhiArray[jet]
            jetPt   = jetPtArray[jet]

            hJetEta.Fill(jetEta)
            hJetPhi.Fill(jetPhi)
            hJetPt.Fill(jetPt/1000) #GeV conversion
            
            # for lay in range(0, maxOfLayers): 
            
            lay = 1
            layerIndex = np.where(jetCellLayerArray[jIndex] == lay)[0]
            # print("Event {}: ".format(evN),jetCellLayerArray[layerIndex])

            if len(layerIndex) > 0:
                farCellIndex        = np.argmax(jetCellsDistDEtaArray[layerIndex][:]) # farthest away cell from jet (only noise)
                centralCellIndex    = getCentralCellIndexJetROI_deltaR(jetEta, 
                                                                        jetPhi, 
                                                                        jetCellEtaArray[layerIndex],
                                                                        jetCellPhiArray[layerIndex])
                # print(farCellIndex, jetCellsDistDEtaArray[layerIndex][farCellIndex])

                for cellEtaIndex in range(0, len(jetCellEtaArray[layerIndex])): # for the cells within the calo sampling layer...
                    cellCaloRegion      = jetCellRegionArray[layerIndex][cellEtaIndex]
                    cellEta             = jetCellEtaArray[layerIndex][cellEtaIndex]
                    cellPhi             = jetCellPhiArray[layerIndex][cellEtaIndex]
                    cellDeltaEta        = jetCellsDistDEtaArray[layerIndex][cellEtaIndex]
                    cellDeltaPhi        = jetCellsDistDPhiArray[layerIndex][cellEtaIndex]
                    cellDeltaCentralEta = jetCellEtaArray[layerIndex][centralCellIndex] - jetCellEtaArray[layerIndex][cellEtaIndex]
                    cellDeltaCentralPhi = correctedDeltaPhi(jetCellPhiArray[layerIndex][centralCellIndex], jetCellPhiArray[layerIndex][cellEtaIndex])
                    cellDEta            = jetCellDEtaArray[layerIndex][cellEtaIndex]
                    cellDPhi            = jetCellDPhiArray[layerIndex][cellEtaIndex]
                    jcIndex             = jetCellIndexArray[layerIndex][cellEtaIndex]

                    cellEneRaw, cellTimeRaw  = getCellEneTimeFromChannel(jetRawChannelAmplitudeArray, jetRawChannelTimeArray, jetRawChannelIndexArray, jcIndex)# Raw Ch                        
                    cellEne, cellTime        = getCellEneTimeFromChannel(jetChannelEnergyArray, jetChannelTimeArray, jetChannelIndexArray, jcIndex)# Cell Ch (there is ch only in Tile, otherwise, just cells)

                    for layerRegionIndex, etaMin, etaMax in zip(range(0,regionsPerLayer[lay]), dictCaloLayer['etaLowLim'][lay], dictCaloLayer['etaHighLim'][lay]): # for each region inside a calo sampling layer...
                        # if np.fabs(cellEta) >= etaMin and np.fabs(cellEta) < etaMax: #verify layer eta limits (old criteria)
                        # if np.fabs(cellEta) >= (etaMin - transitionEtaWidth) and np.fabs(cellEta) < (etaMax + transitionEtaWidth): # criteria with expanded eta limits
                        if np.fabs(cellEta) >= (etaMin + transitionEtaWidth) and np.fabs(cellEta) < (etaMax - transitionEtaWidth): # criteria with Reduced eta limits

                                ### DEBUG ####
                            if cellDEta != dictCaloLayer['granularityEtaLayerPrecise'][lay][layerRegionIndex]:# or cellDPhi != dictCaloLayer['granularityPhiLayerPrecise'][lay][layerRegionIndex]:                                   
                                print("Event: {}, {}, region: {} < eta < {}, eta: {} (DEta: truth={}/actual={}), phi: {} (truth={}/actual={})".format(evN, dictCaloLayer["Layer"][lay], etaMin, etaMax, cellEta, dictCaloLayer['granularityEtaLayerPrecise'][lay][layerRegionIndex], cellDEta, cellPhi, dictCaloLayer['granularityPhiLayerPrecise'][lay][layerRegionIndex], cellDPhi))
                            
                            if cellDPhi != dictCaloLayer['granularityPhiLayerPrecise'][lay][layerRegionIndex]:# or cellDPhi != dictCaloLayer['granularityPhiLayerPrecise'][lay][layerRegionIndex]:                                   
                                print("Event: {}, {}, region: {} < eta < {}, eta: {} (DEta: truth={}/actual={}), phi: {} (truth={}/actual={})".format(evN, dictCaloLayer["Layer"][lay], etaMin, etaMax, cellEta, dictCaloLayer['granularityEtaLayerPrecise'][lay][layerRegionIndex], cellDEta, cellPhi, dictCaloLayer['granularityPhiLayerPrecise'][lay][layerRegionIndex], cellDPhi))
                                
                            bInsideLArCrack = (isInsideLArCrackRegion(cellEta) and (cellCaloRegion == 1)) # verify LAr crack region # 1.37 <= eta <= 1.52 and >= 2.37
                            
                            if not(bInsideLArCrack): #if cell is not in the crack:
                                # bLArCellBelongsToECutRegion     = (cellCaloRegion > 0)  and (cellCaloRegion < 5) and (cellEne > eCutLArCells)
                                # bTileCellBelongsToECutRegion    = (cellCaloRegion == 0) and (cellEne > eCutTileCells)
                                # bFwdBelongsToECutRegion         = (cellCaloRegion == 5) and (cellEne > eCutFwdCells)

                                cutCellEne    = cutNoiseSigma * noiseInfoLayer["eStd"][lay][layerRegionIndex] #MeV
                                # cutCellEneRaw = cutNoiseSigma * noiseInfoLayer["eRawStd"][lay][layerRegionIndex] #MeV + ADC (Tile)

                                # histograms without eCut
                                hRawChTimeList[lay][layerRegionIndex].Fill(cellTimeRaw)
                                hRawChEneList[lay][layerRegionIndex].Fill(cellEneRaw/1000)
                                hChTimeList[lay][layerRegionIndex].Fill(cellTime)
                                hChEneList[lay][layerRegionIndex].Fill(cellEne/1000)
                                if cellEtaIndex == farCellIndex: #for noise measuring (MeV)
                                    # print("Far Cell saved !",farCellIndex)
                                    hNoiseCalcEneList[lay][layerRegionIndex].Fill(cellEne)
                                    hNoiseCalcRawEneList[lay][layerRegionIndex].Fill(cellEneRaw)

                                hJetCellEta.Fill(cellEta)
                                hJetCellPhi.Fill(cellPhi)

                                if (cellEta >= 0):
                                    hEtaList_pos[lay][layerRegionIndex].Fill(cellEta) # fill hist by cell eta region criteria
                                    hPhiList_pos[lay][layerRegionIndex].Fill(cellPhi)
                                else:
                                    hEtaList_neg[lay][layerRegionIndex].Fill(cellEta)
                                    hPhiList_neg[lay][layerRegionIndex].Fill(cellPhi)

                                hDeltaEtaJetList[lay][layerRegionIndex].Fill(cellDeltaEta)
                                hDeltaPhiJetList[lay][layerRegionIndex].Fill(cellDeltaPhi)
                                hDeltaCentralEtaJetList[lay][layerRegionIndex].Fill(cellDeltaCentralEta)
                                hDeltaCentralPhiJetList[lay][layerRegionIndex].Fill(cellDeltaCentralPhi)

                                h2JetCellEDeltaEta.Fill(cellDeltaEta, cellEne/1000)
                                h2JetCellEDeltaPhi.Fill(cellDeltaPhi, cellEne/1000)

                                # histograms WITH eCut
                                # if (bLArCellBelongsToECutRegion or bTileCellBelongsToECutRegion or bFwdBelongsToECutRegion):
                                if cellEne > cutCellEne:
                                    hRawChTimeList_cut[lay][layerRegionIndex].Fill(cellTimeRaw)
                                    hRawChEneList_cut[lay][layerRegionIndex].Fill(cellEneRaw/1000)
                                    hChTimeList_cut[lay][layerRegionIndex].Fill(cellTime)
                                    hChEneList_cut[lay][layerRegionIndex].Fill(cellEne/1000)

                                    if (cellEta >= 0):
                                        hEtaList_pos_cut[lay][layerRegionIndex].Fill(cellEta) # fill hist by cell eta region criteria
                                        hPhiList_pos_cut[lay][layerRegionIndex].Fill(cellPhi)
                                    else:
                                        hEtaList_neg_cut[lay][layerRegionIndex].Fill(cellEta) # fill hist by cell eta region criteria
                                        hPhiList_neg_cut[lay][layerRegionIndex].Fill(cellPhi)

                                    hDeltaEtaJetList_cut[lay][layerRegionIndex].Fill(cellDeltaEta)
                                    hDeltaPhiJetList_cut[lay][layerRegionIndex].Fill(cellDeltaPhi)
                                    hDeltaCentralEtaJetList_cut[lay][layerRegionIndex].Fill(cellDeltaCentralEta)
                                    hDeltaCentralPhiJetList_cut[lay][layerRegionIndex].Fill(cellDeltaCentralPhi)

                                    h2JetCellEDeltaEta_cut.Fill(cellDeltaEta, cellEne/1000)
                                    h2JetCellEDeltaPhi_cut.Fill(cellDeltaPhi, cellEne/1000)
#**********************************************
#  PARTICLE PROCESSING
#**********************************************
    if loadParticles:
        # Delta R for the pair with highest Pt (reco and MC_truth)
        recoLead4Vec    = ROOT.TLorentzVector()
        recoSubLead4Vec = ROOT.TLorentzVector()
        if len(photonPtArray) > 1: # there was reconstructed more than 1 photon in this event? Then get Delta_r     
            ptPhotonRecoOrdered     = np.argsort(photonPtArray) #get the indexes in ascending order of PT
            leadPhIndex      = ptPhotonRecoOrdered[0]
            subLeadPhIndex   = ptPhotonRecoOrdered[1]
            if (photonPtArray[leadPhIndex] > cutPtDeltaR) and (photonPtArray[subLeadPhIndex] > cutPtDeltaR): # apply a cut in photon pt                
                recoLead4Vec.SetPtEtaPhiM( photonPtArray[leadPhIndex], photonEtaArray[leadPhIndex], photonPhiArray[leadPhIndex], photonMArray[leadPhIndex])
                recoSubLead4Vec.SetPtEtaPhiM( photonPtArray[subLeadPhIndex], photonEtaArray[subLeadPhIndex], photonPhiArray[subLeadPhIndex], photonMArray[subLeadPhIndex] )
                photonRecoDeltaPhi      = recoLead4Vec.DeltaPhi(recoSubLead4Vec)
                photonRecoDeltaEta      = photonEtaArray[leadPhIndex] - photonEtaArray[subLeadPhIndex] #deltaEta
                photonRecoDeltaR        = recoLead4Vec.DeltaR(recoSubLead4Vec)
                # photonRecoDeltaPhi      = correctedDeltaPhi( photonPhiArray[ptPhotonRecoOrdered[0]], photonPhiArray[ptPhotonRecoOrdered[1]] ) #deltaPhi
                # photonRecoDeltaEta      = photonEtaArray[ptPhotonRecoOrdered[0]] - photonEtaArray[ptPhotonRecoOrdered[1]] #deltaEta
                # photonRecoDeltaR        = calcDeltaR( photonRecoDeltaEta , photonRecoDeltaPhi ) # get delta_R
                hPhotonRecoDeltaR.Fill(photonRecoDeltaR) #fill the histogram
                hPhotonRecoDeltaEta.Fill(photonRecoDeltaEta)
                hPhotonRecoDeltaPhi.Fill(photonRecoDeltaPhi)

        photonTruthIndex    = np.where( mcTruthPdgIdArray == 22 )[0] #get only the indexes of photons in the truth              
        if len(photonTruthIndex) > 1:
            ptPhotonTruthOrdered    = np.argsort(mcTruthPtArray[photonTruthIndex]) #get the indexes in ascending order of PT (only photons)
            leadPhIndex             = ptPhotonTruthOrdered[0]
            subLeadPhIndex          = ptPhotonTruthOrdered[1]
            if (mcTruthPtArray[photonTruthIndex][leadPhIndex] > cutPtDeltaR) and (mcTruthPtArray[photonTruthIndex][subLeadPhIndex] > cutPtDeltaR): # apply a cut in photon pt
                recoLead4Vec.SetPtEtaPhiM( mcTruthPtArray[photonTruthIndex][leadPhIndex], mcTruthEtaArray[photonTruthIndex][leadPhIndex], mcTruthPhiArray[photonTruthIndex][leadPhIndex], mcTruthMArray[photonTruthIndex][leadPhIndex])
                recoSubLead4Vec.SetPtEtaPhiM( mcTruthPtArray[photonTruthIndex][subLeadPhIndex], mcTruthEtaArray[photonTruthIndex][subLeadPhIndex], mcTruthPhiArray[photonTruthIndex][subLeadPhIndex], mcTruthMArray[photonTruthIndex][subLeadPhIndex] )
                photonTruthDeltaPhi      = recoLead4Vec.DeltaPhi(recoSubLead4Vec)
                photonTruthDeltaEta      = mcTruthEtaArray[photonTruthIndex][leadPhIndex] - mcTruthEtaArray[photonTruthIndex][subLeadPhIndex] #deltaPhi only between truthPhotons pair, of highest PT
                photonTruthDeltaR        = recoLead4Vec.DeltaR(recoSubLead4Vec)
                
                # photonTruthDeltaPhi     = correctedDeltaPhi( mcTruthPhiArray[photonTruthIndex][ptPhotonTruthOrdered[0]] , mcTruthPhiArray[photonTruthIndex][ptPhotonTruthOrdered[1]] ) #deltaPhi only between truthPhotons pair, of highest PT
                # photonTruthDeltaEta     = mcTruthEtaArray[photonTruthIndex][ptPhotonTruthOrdered[0]] - mcTruthEtaArray[photonTruthIndex][ptPhotonTruthOrdered[1]] #deltaPhi only between truthPhotons pair, of highest PT
                # photonTruthDeltaR       = calcDeltaR( photonTruthDeltaEta , photonTruthDeltaPhi )
                hPhotonTruthDeltaR.Fill(photonTruthDeltaR) #fill the histogram
                hPhotonTruthDeltaEta.Fill(photonTruthDeltaEta)
                hPhotonTruthDeltaPhi.Fill(photonTruthDeltaPhi)
                
        for photon in photonEnergyArray:
            hPhotonEnergy.Fill(photon/1000)
        for particle in range(0,len(mcTruthEnergyArray)):
            # Get only the photons
            if mcTruthPdgId[particle] == 22: # photon pdgID
                hPhotonTruthEnergy.Fill(mcTruthEnergyArray[particle]/1000)
                hParticlePdgId.Fill(mcTruthPdgIdArray[particle])

        

###################################################################        
# ***************
#   HISTOGRAMS
# ***************
if saveHistograms:
    histConfigDict  = {} # dictionary with many histogram configuration parameters for help in further plots

    # ** Clusters **
    if loadClusters:
        histFile = ROOT.TFile(outputDirHist+clusFileName, "RECREATE")
        hClusterCellEta.Write()
        hClusterCellPhi.Write()
        hClusterEta.Write()
        hClusterPhi.Write()
        hClusterEtaCalc.Write()
        hClusterPhiCalc.Write()
        hClusterEtaDiff.Write()
        hClusterPhiDiff.Write()
        histFile.Close()
        logging.info(outputDirHist+clusFileName + " saved.")


    # ** Particles **
    if loadParticles:
        histFile = ROOT.TFile(outputDirHist+partFileName, "RECREATE")
        hPhotonEnergy.Write()
        hPhotonTruthEnergy.Write()
        hParticlePdgId.Write()
        hPhotonRecoDeltaR.Write()
        hPhotonTruthDeltaR.Write()
        hPhotonRecoDeltaEta.Write()
        hPhotonTruthDeltaEta.Write()
        hPhotonRecoDeltaPhi.Write()
        hPhotonTruthDeltaPhi.Write()
        histFile.Close()
        logging.info(outputDirHist+partFileName + " saved.")

    # ** Noise in Jets ROI **
    if getNoiseCell:
        noiseCutDict = {"eRawStd":[],
                        "eRawMean": [],
                        "eStd":[],
                        "eMean": []}

        histFile = ROOT.TFile(outputDirHist+noiseFileName, "RECREATE")
        for lay in range(0, maxOfLayers):
            # nReg    = len(regionsPerLayer[lay]) # get the number of regions per layer
            eRawStd  = []
            eRawMean = []
            eStd     = []
            eMean    = []
            for region in range(0, regionsPerLayer[lay]):
                hNoiseCalcEneList[lay][region].Write()
                hNoiseCalcRawEneList[lay][region].Write()

                eRawStd.append(hNoiseCalcRawEneList[lay][region].GetStdDev())
                eRawMean.append(hNoiseCalcRawEneList[lay][region].GetMean())
                eStd.append(hNoiseCalcEneList[lay][region].GetStdDev())
                eMean.append(hNoiseCalcEneList[lay][region].GetMean())

            noiseCutDict['eRawStd'].append(eRawStd)
            noiseCutDict['eRawMean'].append(eRawMean)
            noiseCutDict['eStd'].append(eStd)
            noiseCutDict['eMean'].append(eMean)
    
        histFile.Close()
        saveAsJsonFile(noiseCutDict, outputDirHist+caloNoiseDictName)
        logging.info(outputDirHist+caloNoiseDictName + " saved.")

    # ** Jets **
    if loadJets or test:
        histFile = ROOT.TFile(outputDirHist+jetFileName, "RECREATE")
        for lay in range(0, maxOfLayers):
            for region in range(0, regionsPerLayer[lay]):
                hDeltaEtaJetList[lay][region].Write()
                hDeltaPhiJetList[lay][region].Write()
                hDeltaEtaJetList_cut[lay][region].Write()
                hDeltaPhiJetList_cut[lay][region].Write()
                hDeltaCentralEtaJetList[lay][region].Write()
                hDeltaCentralPhiJetList[lay][region].Write()
                hDeltaCentralEtaJetList_cut[lay][region].Write()
                hDeltaCentralPhiJetList_cut[lay][region].Write()
                hEtaList_pos[lay][region].Write()
                hPhiList_pos[lay][region].Write()
                hEtaList_neg[lay][region].Write()
                hPhiList_neg[lay][region].Write()
                hEtaList_pos_cut[lay][region].Write()
                hPhiList_pos_cut[lay][region].Write()
                hEtaList_neg_cut[lay][region].Write()
                hPhiList_neg_cut[lay][region].Write()
                hRawChTimeList[lay][region].Write()
                hRawChEneList[lay][region].Write()
                hRawChTimeList_cut[lay][region].Write()
                hRawChEneList_cut[lay][region].Write()
                # hNoiseCalcEneList[lay][region].Write()
                # hNoiseCalcRawEneList[lay][region].Write()
                hChTimeList[lay][region].Write()
                hChEneList[lay][region].Write()
                hChTimeList_cut[lay][region].Write()
                hChEneList_cut[lay][region].Write()

        hJetEta.Write()
        hJetPhi.Write()
        hJetPt.Write()
        hJetCellEta.Write()
        hJetCellPhi.Write()
        hJetCellEta_cut.Write()
        hJetCellPhi_cut.Write()
        h2JetCellEDeltaEta.Write()
        h2JetCellEDeltaPhi.Write()
        h2JetCellEDeltaEta_cut.Write()
        h2JetCellEDeltaPhi_cut.Write()        
        histFile.Close()
        logging.info(outputDirHist+jetFileName + " saved.")

        histConfigDict["divBinEtaReg"]          = divBinEtaReg
        histConfigDict["divBinPhiReg"]          = divBinPhiReg
        histConfigDict["cutNoiseSigma"]         = cutNoiseSigma
        histConfigDict["transitionEtaWidthL"]   = transitionEtaWidthL
        histConfigDict["transitionEtaWidthH"]   = transitionEtaWidthH


if not(getNoiseCell) and saveHistograms: saveAsJsonFile(histConfigDict, outputDirHist+histConfigDictName )

end = time()
timeLog = "Total time for %d events: %.3f minutes."%(evLim,(end-start)/60)
print(timeLog)
logging.info(timeLog)

ROOT.gROOT.SetBatch(False)

# /usr/bin/python3 /eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/source/EventReader/share/validPlots.py
# /usr/bin/python3 /eos/user/m/mhufnage/scripts_lxplus/Grid/workAthena/ALPPackages/source/EventReader/share/saveCaloDict.py
